\section[Intro.]{Introduction}
\begin{frame}{Recap}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, text width=1cm, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, text width=3cm, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]

\begin{figure}[htb]
    \centering
    \resizebox{\textwidth}{!}{%
        \begin{tikzpicture}[node distance=6cm]
        \node (gm) [process] {Generate Mesh};
        \node (gd) [process, right of=gm] {Generate State Space Model};
        \node (material) [io, below of=gm] {Material Prop.};
        \node (excitation) [process, right of=gd] {$\dot{x} = \mathbf{A} x + \mathbf{B} u$ \\$y = \mathbf{C} x + \mathbf{D} u$};
        \node (loading) [io, below of=excitation] {Step};
        \node (vib) [process, right of=excitation] {Vibration Localizer};
        \node (visual) [process, below of=vib] {Visual Localizer};
        \node (noise) [io, below of=visual] {Noise Model};
        \node (sf) [process, right of=vib] {Sensor Fusion};
        % \draw [arrow] (start) -- (gm);
        \draw [arrow] (gm) -- node[anchor=south] {$\mathbf{K}, \mathbf{M}$} (gd);
        \draw [arrow] (material) -| (gd);
        \draw [arrow] (material) -- (gm);
        \draw [arrow] (gd) -- node[anchor=south] {$\mathbf{A, B, C, D}$}(excitation);
        \draw [arrow] (vib) -- (sf);
        \draw [arrow] (loading) -- node[anchor=east] {$u(t)$}(excitation);
        \draw [arrow] (loading) -- node[anchor=south] {Step Location}(visual);
        \draw [arrow] (loading) |- node[anchor=south] {Step Location}(noise);
        \draw [arrow] (noise) -- node[anchor=east] {$\mathbf{\Sigma_{noise}}$}(visual);
        \draw [arrow] (excitation) -- node[anchor=south] {$y(t)$} (vib);
        \draw [arrow] (visual) -| (sf);
        \end{tikzpicture}
    }%
    % \caption{This figure has a width which is a factor of text width}
\end{figure}
\end{frame}
\begingroup
\footnotesize
\begin{frame}{Heuristic Vibrolocalizer (HV)}
    \begin{columns}[T]
        \column{0.45\textwidth}
        \justify
        Energy of a discrete-time vibration signal observed by accelerometer $i$:
        \begin{equation*}
            E_i = \sum \left|z_k\right|^2
        \end{equation*}
        Location of the target:
        \begin{equation*}
        \begin{bmatrix}
            x_t \\
            y_t
        \end{bmatrix}  = \dfrac{1}{\sum^K_{i=1} E_i} 
        \begin{bmatrix}x_1 & \cdots & x_K \\ y_1 & \cdots & y_K \end{bmatrix}
        \begin{bmatrix}E_1 \\ \vdots \\ E_K \end{bmatrix}
        \end{equation*}
        \column{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figs-12102020/goodwin.png}
            \caption{HV employs the energy reading of the same impact by a number of sensors. The linear combination of the sensor locations and energy rates yields the location of the target.}
        \end{figure}
    \end{columns}
    \pause
    \begin{block}{HV Limitations:}
        HV is limited to localize occupants within the convex-hull that is made up of the sensor locations.
    \end{block}
\end{frame}
\endgroup

\begin{frame}{Introduction - II}
    Wave~\cref{eq:wave} is studied since 1746 for different structures, geometries, etc.
    \begin{equation}
        \label{eq:wave}
        \frac{\partial^2 \vect{u}}{\partial t^2} = c^2 \nabla^2 \vect{u}
    \end{equation}
    \pause 
    It is very intricate to model the vibration of the modern structures:
    \begin{itemize}
        \pause \item Non-isotropic material properties
        \pause \item Complex boundary conditions
        \pause \item Sophisticated geometries and imperfections
        \pause \item Other nonlinearities
    \end{itemize}
    \pause
    \begin{block}{}
        Under these observations, any localization system, that uses the \textit{vibration} of a solid body as \textit{localization feature}, should model the inaccuracies, uncertainties of the overall system. 
    \end{block}
    
\end{frame}

\begin{frame}{Summary}
    I will discuss three concepts:
    \vfill
    \begin{figure}
    \centering
    \begin{tikzpicture}[scale=1]
        \draw[thick] (0, 0) circle (1.5cm); 
        \draw[thick] (4, 0) circle (1.5cm); 
        \draw[thick] (8, 0) circle (1.5cm); 
        \draw (0, -2.5) node {${}^{i}$ Energy};
        \draw (4,-2.5) node {${}^{ii}$ Localization};
        \draw (8,-2.5) node {${}^{iii}$ Self-calibration};
        \draw (0, 0) node[inner sep=0pt] {\includegraphics[width=.24\textwidth]{figs-12102020/energy_circle.eps}};
        \draw (4, 0) node[inner sep=0pt] {\includegraphics[width=.21\textwidth]{figs-12102020/localization_circle.eps}};
        \draw (8, 0) node[inner sep=0pt] {\includegraphics[width=.21\textwidth]{figs-12102020/gradient_descent.png}};
    \end{tikzpicture}
    \end{figure}
\end{frame}

\section[Sensor Model]{Energy of A Structural Vibration Signal -- Probabilistic Perspective}
\begin{frame}{From the Perspective of the Sensor}
Following the previous literature~\cite{kok2017using}, the measurements of a single degree-of-freedom accelerometer can be modeled as below:

\begin{block}{1-DOF Accelerometer Model}
    \begin{equation}
        \label{eq:acc}
        z_k = \underbrace{z^a_k}_{\text{Actual vibration}} + \underbrace{\delta_k}_{\text{Drift model}} + \underbrace{\nu_k}_{\text{Noise model}}
    \end{equation}
\end{block}

where $z_k$ is the measurement of the accelerometer at time step $k$ \\
$\delta_k \sim \mathcal{N}\left(a_\delta k + b_\delta, \sigma_\delta^2 \right)$, and \\
$\nu_k \sim \mathcal{N}\left(0, \sigma_\nu^2 \right)$ represents the actual acceleration, the slowly varying sensor bias, and the noise,
respectively.\footnote[frame]{$X \sim \mathcal{N}\left(\mu, \sigma^2 \right)$ and $Y \sim \texttt{Normal}(\mu, \sigma^2)$ represent normally distributed random variables with a mean of $\mu$ and standard deviation of $\sigma$.}
\end{frame}

\begin{frame}{Accelerometer -- Probabilistic Perspective}
    \begin{columns}[T]
        \column{0.55\textwidth}
        \begin{block}{Measurement Distribution}
            \justify
            By using the properties of the normal distribution, we can consider each time sample of the measurement as drawn from the distribution stated below:
            \begin{equation*}
                \label{eq:acc_rv}
                Z_k \sim \mathcal{N}\left(\underbrace{z^a_k + a_\delta k + b_\delta}_{\mu_z}, \underbrace{\sigma_\delta^2 + \sigma_\nu^2}_{\sigma_z^2} \right)
            \end{equation*}
        \end{block}
        \column{0.4\textwidth}
        \begin{figure}
            \includegraphics[width=\linewidth]{figs-12102020/1a.eps}
            \caption{\footnotesize Vibration response of a $2^{nd}$ order system with its noise envelope.}
        \end{figure}
    \end{columns}
\end{frame}

% \begin{frame}{Energy of a Discrete-time Signal}
% \begingroup
% \small
% Conventionally, the energy of a discrete time signal $z_k$ between time steps $k=\{k_1 \ldots k_n \}$ is calculated as shown in \Cref{eq:energy}.
% \begin{equation}
%     \label{eq:energy}
%     e = \sum_{k_1}^{k_n} \lvert z_k \rvert^2 = \sum_{k_1}^{k_n} \left\lvert \mathcal{N}\left(\mu_z, \sigma_z^2 \right)\right\rvert^2 = \sigma_z^2 \sum_{k_1}^{k_n} \left\lvert \mathcal{N}\left(\frac{\mu_z}{\sigma_z}, 1 \right)\right\rvert^2
% \end{equation}

% \begin{columns}[T]
%     \column{0.65\linewidth}
%     \begin{block}{Energy distribution}
%     \begingroup
%     \footnotesize
%     \justify
%     By definition, squared some of $n$ number of normally distributed random variable with unit variance follows \texttt{Non-central Chi-Squared} distribution, denoted as $\chi^{\prime 2}_n\left(\lambda \right)$ with non-centrality parameter $\lambda = \sum^n_1 \left(\frac{\mu_z}{\sigma_z}\right)^2$, and $n$ degrees of freedom.
%     \begin{equation}
%         \label{eq:energy_rv}
%             E \sim \sigma_z^2 \chi^{\prime 2}_n\left(\lambda \right)
%     \end{equation}
%     \endgroup
%     \end{block}
%     \column{0.35\textwidth}
%     \begin{figure}
%         \includegraphics[width=\linewidth]{figs-12102020/chi_squared.eps}
%         % \caption{\footnotesize Energy of a time signal under uncertainties.}
%     \end{figure}
% \end{columns}
% \endgroup
% 
% \end{frame}


\begin{frame}{True Energy Distribution from Noisy Signal -- I}
    Let's assume the samples of the vibration (time) signal follow the model $Z_k = z_k + \nu_k$ where $z_k$ is the actual vibration, and $\nu_k \sim \mathcal{N}\left(0, \sigma_z^2\right)$ random errors in sensing it.
    This representation is equivalent with:
    \pause
    \begin{equation}
        Z_k \sim \mathcal{N}\left(z_k, \sigma_z^2\right) \sim \sigma_z \mathcal{N}\left(\frac{z_k}{\sigma_z}, 1\right)
    \end{equation}
    \pause
     Then, the energy of the noisy signal:
    \begin{subequations}
        \begin{align}
            E_n &= \sum_{k=1}^{n} \left\lvert Z_k \right\rvert^{2} \sim \sigma_z^2 \chi^{\prime 2}_n\left(\frac{E_t}{\sigma_z^2} \right) \\
            &\simeq \mathcal{N}\left(n + \frac{E_t}{\sigma_z^2}, \frac{4 E_t}{\sigma_z^2} + 2n \right) \\
            E_n &= \sigma_z^2 n + E_t + 2 \sqrt{E_t} + \kappa
        \end{align}
    \end{subequations}
    where $\kappa \sim \mathcal{N}\left(0, 1 + 2 n \sigma_z^2 \right)$.
\end{frame}


\begin{frame}{True Energy Distribution from Noisy Signal -- I}
\begin{block}{True Energy Distribution from Noisy Signal}
    \begin{equation}
        E_t \sim \underbrace{\mathcal{N}\left(2 + n \sigma_z^2 - E_n, 1 + 2 n \sigma_z^2 \right) - 2 \sqrt{\mathcal{N}\left(1- n \sigma_z^2 - E_n, 1 + 2 n \sigma_z^2 \right)}}_{~\Pi(n, \sigma, E)}
    \end{equation}
\end{block}
\begin{equation}
    \Pi(x~|~n, \sigma, E) = 
    % \frac{\sqrt{2}\,{\mathrm{e}}^{-\frac{{\left(-n\,{\sigma _{z}}^2+E_{n}+x-2\right)}^2}{2\,{\left(2\,n\,{\sigma _{z}}^2+1\right)}^2}}}{2\,\sqrt{\pi }\,\left(2\,n\,{\sigma _{z}}^2+1\right)}-2\,\sqrt{\frac{\sqrt{2}\,{\mathrm{e}}^{-\frac{{\left(n\,{\sigma _{z}}^2+E_{n}+x-1\right)}^2}{2\,{\left(2\,n\,{\sigma _{z}}^2+1\right)}^2}}}{2\,\sqrt{\pi }\,\left(2\,n\,{\sigma _{z}}^2+1\right)}}
     \frac{\sqrt{2}\,{\mathrm{e}}^{-\frac{{\left(-n\,{\sigma }^2+E_{n}+x-2\right)}^2}{2\,{\left(2\,n\,{\sigma }^2+1\right)}^2}}}{2\,\sqrt{\pi }\,\left(2\,n\,{\sigma }^2+1\right)}-2\,\sqrt{\frac{\sqrt{2}\,{\mathrm{e}}^{-\frac{{\left(n\,{\sigma }^2+E_{n}+x-1\right)}^2}{2\,{\left(2\,n\,{\sigma }^2+1\right)}^2}}}{2\,\sqrt{\pi }\,\left(2\,n\,{\sigma }^2+1\right)}}
\end{equation}
\end{frame}

\begin{frame}{True Energy Distribution from Noisy Signal -- II}
\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{figs-12102020/timesignal.eps}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{figs-12102020/timesignal_30secs.eps}
\end{figure}
\end{frame}

% \begin{frame}{Energy Distribution -- Approximation to Normal}
%     \begin{columns}[T]
%     \column{0.55\textwidth}
%         \justify
%         A random variable with \texttt{Non-central Chi-squared} distribution can be approximated to that of a normally distributed as $n \to \infty$ or $\lambda \to \infty$.
%                 $E \to \mathcal{N} \left(\mu, \sigma^2 \right)$, where $\mu \approx (n + \lambda)$, and $\sigma^2 \approx 2\left(n+2 \lambda \right)$~\cite{muirhead2009aspects}.
%     \column{0.45\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[width=\textwidth]{figs-12102020/1b.eps}
%             % \caption{A random variable with \texttt{Non-central Chi-squared} distribution and its approximation to Normal.}
%             \label{fig:my_label}
%         \end{figure}
%     \end{columns}
%     \pause
%     \begin{block}{Energy $E$ represented with Normal Distribution}
%         \begin{equation}
%             E \sim \mathcal{N}\left(\sigma_z^2 \left(\lambda + n\right), 2 \sigma_z^4 \left(2\lambda + n\right) \right) = \mathcal{N}\left(\mu_E, \sigma_E^2 \right)
%             \label{eq:energy_normal_approximation}
%         \end{equation}
%     \end{block}
% \end{frame}

\section[PEBLE]{Probabilistic Energy-based Location Estimator}
\begin{frame}{PEBLE -- \underline{P}robabilistic \underline{E}nergy-\underline{b}ased \underline{L}ocation \underline{E}stimator}
Let a floor-mounted accelerometer $a$ that resides the localization space $\mathcal{S}$ is located $\vect{x}_a = \begin{bmatrix} x_a & y_a \end{bmatrix}^\top \in \mathbb{R}^2$.
% Accelerometer $a$ measures the acceleration of the floor due to the footfall pattern of an occupant in the localization space $\mathcal{S}$.
% In this work, we consider a localization problem where each occupant is first localized in accelerometer's local coordinate system where the target location is characterized with the distance $d$ between the sensor and the target, and the direction $\theta$ of the target wrt. to the sensor.
% This representation forms a polar coordinate frame where the origin is the sensor location as shown in \Cref{fig:polar_coords}.

\begin{figure}
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[step=0.5cm,gray,very thin, fill opacity=0.1] (-5,-1) grid (10,5); % grid
        \draw (-5, 4.5) node[anchor=west, rotate=0] {Localization space $\mathcal{S}$};
        \draw[red,thick,->] (0, 0) -- (2, 0) node[anchor=north east] {$x_a$}; % x-axis
        \draw[green,thick,->] (0, 0) -- (0, 2) node[anchor=north east] {$y_a$}; % y-axis
        \draw[fill=black, fill opacity=0.2] (0, 0) circle (0.07cm);  %% sensor
        \draw[gray,thick,.->] (0, 0) -- (3, 4) node[anchor=north east]{}; %
        \draw[fill=black] (3, 4) circle (0.1cm);  %% target
        \draw (3,4) node[anchor=west, rotate=0] {Target location $d \angle \theta$};
        \draw (1.5, 2.5) node[anchor=east, rotate=45] {$d$};
        \draw [<->] (0:1.1)  arc (0:53.1:1.1) node [right,pos=0.5] {$\theta$};
    \end{tikzpicture}
    \caption{The occupants location is characterized in a polar coordinate system defined about sensor's location. The origin of this coordinate frame is the location of the sensor in localization space $\mathcal{S}$. The occupant's location is finally described with the vector formed with distance $d$ and the direction $\theta$.}
    \label{fig:polar_coords}
\end{figure}
\end{frame}

\begingroup
\small
\begin{frame}{Distance Model}
In order to determine the distance $d$ between the accelerometer and the occupant, we assume a simple propagation model of the floor vibration such that resulting energy of the signal $E$ decays exponentially with the distance $d$ between the accelerometer $a$ and the impact location.

\begin{equation}
    \label{eq:propagation}
    E = E_0 \exp{\left(\gamma d\right)}
\end{equation}
where $E_0$ is the energy at the impact location, $\gamma$ decay rate.

\begin{block}{Distance Distribution}
    \begin{equation}
        D = \frac{1}{\gamma}\log{\frac{E}{E_0}}
        \label{eq:distance}
    \end{equation}
    with a pdf of
    \begin{equation}
        f_D(d) = \frac{1}{\gamma}\log{\frac{\Pi(d~|~n, \sigma, E)}{E_0}}
    \end{equation}
\end{block}
\end{frame}
\endgroup

% \begin{frame}{Distance Model - II}
% Since the energy of the time signal follows a normal distribution, we can finally conclude that the distance $d$ between the accelerometer and the impact can be modeled with a \texttt{Log-normal} distribution as shown below.

% \begin{block}{Distance Distribution}
%     \begin{equation}
%         \label{eq:distance}
%           D \sim \mathcal{LN}\left(\frac{\mu_E}{\gamma}, \frac{\sigma_E^2}{\gamma^2} \right)
%     \end{equation}
%     where $\mathcal{LN}\left(\mu_D=\frac{\mu_E}{\gamma}, \sigma_D^2=\frac{\sigma_E^2}{\gamma^2} \right)$ represents a \texttt{Log-normal} distribution with a mean $\mu_D$ and a variance $\sigma_D^2$.
% \end{block}
% \end{frame}

% \begin{frame}{Distance Model - III}
%     \begin{columns}[T]
%         \column{0.6\textwidth}
%             \justify
%             PDF of a \texttt{Log-normal} distribution is shown below.
%             By using this model, the probability of target's location for any given distance $d$ can be easily calculated.
%         \column{0.4\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[width=\textwidth]{figs-12102020/2a.eps}
%         \end{figure}
%     \end{columns}
%     \begin{block}{Distance Distribution}
%     \begin{equation}
%         \label{eq:pdf_d_acc}
%         p_{D}(d~\lvert~\mu_D, \sigma_D) = \frac{1}{d \sigma \sqrt{2 \pi}} \exp{\left( -\frac{\log{\left( d-\mu\right)}^2}{2 \sigma^2} \right)} 
%     \end{equation}
% \end{block}
% \end{frame}

\begin{frame}{Direction Model}
From the derivation in \Cref{eq:distance}, an accelerometer can said to be a `ranging' sensor since it is only able to infer the distance between the occupant and the sensor itself.
As for the directional information, it will not be able obtained from energy-based localization.
Therefore, the direction of the occupant wrt to the sensor can be modeled as $\Theta \sim \texttt{Uniform}\left(0, 2\pi \right)$.

\begin{columns}[T]
    \column{0.5\textwidth}
    \begin{block}{Direction Distribution}
        \begin{equation}
            \label{eq:pdf_theta_acc}
            f_{\Theta}(\theta) = \frac{1}{2 \pi}
        \end{equation}
    \end{block}
    \column{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{figs-12102020/2b_linear_uniform.eps}
    \end{figure}
\end{columns}
\end{frame}

\begin{frame}{Polar Coordinates to Global (Cartesian) Coordinates}
    If local estimation of each accelerometer is transformed onto the localization space $\mathcal{S}$:
    \begin{equation}
        \label{eq:polar_cartesian}
                f_{X,Y}(x, y) = \frac{f_{D, \Theta}(d, \theta)}{\begin{vmatrix}\vect{J}\end{vmatrix}} = \frac{f_{D}(d) f_{\Theta}(\theta)}{\begin{vmatrix}\vect{J}\end{vmatrix}},
    \end{equation}
    \begin{equation}
        \begin{vmatrix} \vect{J} \end{vmatrix} = 
        \begin{vmatrix}
            \frac{\partial x}{\partial r} & \frac{\partial x}{\partial \theta} \\
            \frac{\partial y}{\partial r} & \frac{\partial y}{\partial \theta}
        \end{vmatrix} =
        \begin{vmatrix}
            \cos{\theta} & -d \sin{\theta} \\
            \sin{\theta} & -d \cos{\theta} \\
            \end{vmatrix} = d \left(\sin^2{\theta} + \cos^2{\theta} \right) = d
    \end{equation}

    \begin{block}{Location Distribution}
        \begin{equation}
            f_{X,Y}(x, y) = \frac{f_{D}(d=\norm{\vect{x_t} - \vect{x_a}}) f_{\Theta}(\theta)}{\norm{\vect{x_t} - \vect{x_a}}}
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}{Location Distribution}
    Explicitly, the location distribution can be stated as:
    
    \begin{equation}
    \label{eq:pdf_acc_final}
        f_{X,Y}(x, y) = \frac{1}{2 \gamma \pi d}\log{\frac{\Pi(d~|~n, \sigma, E_n)}{E_0}}
    \end{equation}

    where $d=\norm{\vect{x} - \vect{x_a}}$ is the sensor-source distance.
    \begin{block}{Note:}
        In order words, the mean and the variance of the distance $D$ is correlated with the sum of the energy of the noisy, bias-drifted vibration signal, and the number of samples. 
    \end{block}
\end{frame}

\section[Self-Calibration]{Self-calibrating Localization}
\begin{frame}{Self-calibration, is it even possible?}
    \begin{itemize}
        \item Question arises, can we spare ourselves from an arduous calibration step? 
        \pause \item Can PEBLE exploit some facts about the system design?
        \pause \item Can calibration problem and localization problem inform each other?
    \end{itemize}
\end{frame}

\begin{frame}{Calibration -- I}
Assuming $N$ number sensors that are index with $k$ are planted in the localization space $\mathcal{S}$.
We can formulate the calibration as a minimization problem for unknowns $E_0$, $\gamma_k$ with measured energy $E_k$.

\begin{block}<only@1>{Calibration Problem with known distance $\hat{d}_k$}
    \begin{equation}
        E^*_0, \gamma^*_k = \argmin{E_0 \gamma_k}E_k - E_0 \exp{\left(d_k \gamma_k\right)}
    \end{equation}
\end{block}
\begin{block}<only@2-3>{Calibration Problem in matrix terms}
    \begin{equation}
        \begin{bmatrix}
            \vect{1}_{N\times 1} & \vect{I}_{N\times N} \\
        \end{bmatrix}
        \begin{bmatrix}
            \log{E_0} \\
            \gamma_1 \\\
            \vdots \\
            \gamma_N
        \end{bmatrix} 
        = \begin{bmatrix}
            \log{E_1} \\
            \vdots \\
            \log{E_N} \\
        \end{bmatrix}
    \end{equation}
\end{block}
\begin{block}<only@3>{}
The system is under-determined. There exists some techniques to handle it but the solution is not guaranteed!
\end{block}

% \pause
% \begin{block}{Calibration Problem with distance $d_k$ as unknown}
%     \begin{equation}
%         \left\{d_k, \gamma, \sigma_z \right\}^*_{1..N} = \argmin{d, \gamma, \sigma_z}\left[\sum^N_1\left\{\mathbb{E}(E_k) - \gamma_k  \log(\norm{\vect{x_t} - \vect{x_k}}\right\}\right]
%     \end{equation}
% \end{block}
% \begin{block}{Localization Problem}
%     \begin{equation}
%         \vect{x_t}^* = \argmin{\vect{x_t}}\left[-\sum^K_1\log{\frac{f_{D}(\norm{\vect{x_t} - \vect{x_k}}) f_{\Theta}(\theta)}{\norm{\vect{x_t} - \vect{x_k}}}}\right]
%     \end{equation}
% \end{block}
\end{frame}

\begingroup
\small
\begin{frame}{Calibration -- II}
\begin{block}{Calibration w/o knowing distance $d_k$}
    Calibration Problem for sensor $k$:
    \begin{equation}
        E^*_0, \gamma^*_k, d^*_k = \argmin{E_0, d_k, \gamma_k}E_k - E_0 \exp{\left(d_k \gamma_k\right)}
    \end{equation}
    Localization Problem with all sensors:
    \begin{equation}
        \vect{x_t}^* = \argmin{\vect{x_t}} -\sum^N_{k=1}\log{\frac{f_{D}(d_k) f_{\Theta}(\theta)}{d_k}}
    \end{equation}
    where $d_k = \norm{\vect{x_t} - \vect{x_k}}$.
\end{block}

\pause \begin{block}{Note}
There exists infinitely many solutions for the calibration problem, but one of them should jointly solve the localization problem, too.
\end{block}
\end{frame}
\endgroup

\begingroup
\small
\begin{frame}{New Cost Function}
    Let us define two new cost functions $\vect{G}(\vect{p})$ and $\vect{H}(\vect{p})$ that are:
    \begin{itemize}
        \pause \item multivariate: $\vect{p} =
        \left[
            \underbrace{x_t, y_t,}_{\text{Target Location}} \underbrace{E_0,}_{\text{Forcing}} \underbrace{\gamma_1, \ldots, \gamma_N}_{\text{Propagation params.}}, \underbrace{\sigma_{z_1}, \ldots, \sigma_{z_N}}_{\text{Sensor params.}}
        \right]^\top$ 
        \pause \item vector-valued:
        \begin{equation}
            \begin{split}
            \vect{G}(\vect{p}) &=
            \begin{bmatrix}
                E_1 - E_0 \exp{\left(d_1 \gamma_1\right)} \\
                \vdots \\
                E_N - E_0 \exp{\left(d_N \gamma_N\right)} \\
            \end{bmatrix}, \\
            \vect{H}(\vect{p}) &= \begin{bmatrix}
                -\log{\frac{1}{2 \gamma \pi d_1}\log{\frac{\Pi(d_1~|~n_1, \sigma_{z_1}, E_1)}{E_0}}} \\
                \vdots \\
                -\log{\frac{1}{2 \gamma \pi d_K}\log{\frac{\Pi(d_K~|~n_K, \sigma_{z_K}, E_K)}{E_0}}} \\
            \end{bmatrix}
            \end{split}
        \end{equation}
    \end{itemize}
\end{frame}
\endgroup

\begin{frame}{}
    Instead of minimizing $\vect{G}(\vect{p})$ or $\vect{H}(\vect{p})$, we can define another function based on the original:
    \begin{equation} 
        F(\vect{p})= \frac{1}{2} \vect{G}^\top \vect{G}(\vect{p}) + \frac{1}{2} \vect{H}^\top \vect{H}(\vect{p})
        \label{eq:cost}
    \end{equation}
    
    Can we minimize \Cref{eq:cost} for some 
    \begin{equation*}
        \vect{p} =
                \left[
                    \underbrace{x_t, y_t,}_{\text{Target Location}} \underbrace{E_0,}_{\text{Forcing}} \underbrace{\gamma_1, \ldots, \gamma_N}_{\text{Propagation params.}}, \underbrace{\sigma_{z_1}, \ldots, \sigma_{z_N}}_{\text{Sensor params.}}
                \right]^\top
    \end{equation*}
        given the energies calculated by the accelerometers $\{E_1, \cdots, E_k\}$.
\end{frame}
\begingroup
\small
\begin{frame}{Iterative Solver}
    Since the cost function $F(\vect{p})$ is under-determined, and nonlinear, it is difficult to solve for the exact parameters. However, we can use numeric methods to minimize the errors due to the parameter vector $\vect{p}$.

    \pause
    \begin{block}{Gradient Descent}
    \begin{columns}[T]
        \column{0.6\textwidth}
        \justify
        This algorithm solves the function $\vect{G}(\vect{p})$ in an iterative manner.
        In each iteration $i$, the algorithm takes a step in the opposite direction of the gradient of the function at the current point, i.e. $\nabla F$ evaluated at $\vect{p}^{i}$.
        %
        \column{0.3\textwidth}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=1]
                \draw[red,thick,->] (-1.2, -1) -- (0.8, -1) node[anchor=north east] {$x_t$}; % x-axis
                \draw[green,thick,->] (-1.2, -1) -- (-1.2, 1) node[anchor=north east] {$y_t$}; % x-axis
                \draw[thick, fill=black] (0, 0) circle(0.03);
                \draw[thick] (0, 0) circle [x radius=1cm, y radius=0.8cm]; 
                % \draw[thick] (0.015, 0.015) circle [x radius=0.7cm, y radius=0.65cm]; 
                \draw[thick] (0.03, 0.025) circle [x radius=0.55cm, y radius=0.5cm];
                \draw[thick] (0, 0) circle [x radius=0.25cm, y radius=0.15cm];
                \draw[black,thin,->] (-1, 0) -- (-.3, -0.4) node[anchor=north] {\small$\vect{p}^1$};
                \draw[black,thin,->] (-.3, -0.4) -- (0.2, -0.1) node[anchor=north] {\small$\vect{p}^2$};
                \draw[black,thin,->] (0.2, -0.1) -- (0.0, 0.0);
            \end{tikzpicture}
        \end{figure}
        \end{columns}
        \begin{equation}
            \vect{p}^{i+1} = \vect{p}^{i} - \eta_0 \nabla F(\vect{p}^{i}) = \vect{p}^{i} - \eta_0 \left( \vect{J}^{\top}_{\vect{G}}(\vect{p}^{i}) \vect{G}(\vect{p}^{i})+\vect{J}^{\top}_{\vect{H}}(\vect{p}^{i}) \vect{H}(\vect{p}^{i})\right)
        \end{equation}
    \end{block}
\end{frame}
\endgroup

\section[Validation]{Validation: Simulations \& Results}
\begingroup
\scriptsize
\begin{frame}{Simulations -- Overview}
    \begin{columns}[T]
    \column{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{figs-12102020/environment.eps}
        \caption{Sensor Layout}
    \end{figure}
    \begin{table}[H]
        \centering
        \begin{tabular}{@{}p{2.5cm}c{1cm}@{}}
            Accelerometers & Property\\ \toprule
            Sampling Rate &  4096 Hz \\ 
            \# of Sensors & 4 \\
            Process Noise $\vect{w}_k$ & No \\
            Measurement Noise $\vect{v}_k$ & Yes \\ \bottomrule
            \bottomrule
        \end{tabular}
        \caption{Properties of the simulated accelerometers}
        \label{tab:sensors}
    \end{table}
    \column{0.5\textwidth}
    \begin{table}[H]
        \centering
        \begin{tabular}{@{}p{2.5cm}c{1cm}@{}}
            Material & Concrete (GWC) \\ \toprule
            Young’s Modulus & 31.490 $GPa$\\ 
            Dimensions & 2 $\times$ 2 $\times$ 0.025 $m$\\
            Density & 2443 $kg/m^3$ \\
            \# of Elements & 13 $\times$ 13 \\
            BC & Simply supported \\
            \bottomrule
        \end{tabular}
        \caption{Material properties used in the simulations.}
        \label{tab:material}
    \end{table}
    \end{columns}
\end{frame}
\endgroup

\begin{frame}{Simulations -- Overview}
    \begin{columns}[T]
    \column{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{figs-12102020/forcing.eps}
        \caption{Ground Reaction Force and its PSD.}
    \end{figure}
    \column{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{figs-12102020/case2_vibration_response.eps}
        \caption{Vibration Response measured with 4 sensors.}
    \end{figure}
    \end{columns}
\end{frame}

\begin{frame}{Simulations -- Two Cases}
    \begin{columns}[T]
    \column{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{figs-12102020/case1_sensors.eps}
        \caption{Case 1: Impact is in the convex hull.}
    \end{figure}
    \column{0.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{figs-12102020/case2_sensors.eps}
        \caption{Case 2: Impact is not in the convex hull.}
    \end{figure}
    \end{columns}
\end{frame}

\begin{frame}{Results -- Case 1 w/ Perfect calibration}
    Set all the sensor and propagation parameters to the exact (simulated) values.
    \begin{block}<only@1>{Energy and Distance Distributions}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figs-12102020/case1_pepd.eps}
        \end{figure}
    \end{block}
    \begin{block}<only@2>{Location Distribution}
        \begin{columns}[T]
            \column{0.45\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=0.9\textwidth]{figs-12102020/case1_estimations.eps}
            \end{figure}
            \column{0.45\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=0.94\textwidth]{figs-12102020/case1_joint_estimation.eps}
            \end{figure}    
        \end{columns}
        
    \end{block}
\end{frame}

\begin{frame}{Results -- Case 2 w/ Perfect calibration}
    Set all the sensor and propagation parameters to the exact (simulated) values.
    \begin{block}<only@1>{Energy and Distance Distributions}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figs-12102020/case2_pepd.eps}
        \end{figure}
    \end{block}
    \begin{block}<only@2>{Location Distribution}
        \begin{columns}[T]
            \column{0.45\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=0.9\textwidth]{figs-12102020/case2_estimations.eps}
            \end{figure}
            \column{0.45\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=0.94\textwidth]{figs-12102020/case2_joint_estimation.eps}
            \end{figure}    
        \end{columns}
        
    \end{block}
\end{frame}

% \begin{frame}{Results -- Case 1 w/ Self-calibration}
%     Estimate all the sensor and propagation parameters, as well as the target location.
%     \begin{block}<only@1>{Energy and Distance Distributions}
%         \begin{figure}
%             \centering
%             \includegraphics[width=\textwidth]{figs-12102020/case1_pepd.eps}
%         \end{figure}
%     \end{block}
%     \begin{block}<only@2>{Location Distribution}
%         \begin{columns}[T]
%             \column{0.45\textwidth}
%             \begin{figure}
%                 \centering
%                 \includegraphics[width=0.9\textwidth]{figs-12102020/case1_estimations.eps}
%             \end{figure}
%             \column{0.45\textwidth}
%             \begin{figure}
%                 \centering
%                 \includegraphics[width=0.94\textwidth]{figs-12102020/case1_joint_estimation.eps}
%             \end{figure}    
%         \end{columns}
        
%     \end{block}
% \end{frame}

% \begin{frame}{Results -- Case 2 w/ Self-calibration}
%     Estimate all the sensor and propagation parameters, as well as the target location.
%     \begin{block}<only@1>{Energy and Distance Distributions}
%         \begin{figure}
%             \centering
%             \includegraphics[width=\textwidth]{figs-12102020/case2_pepd.eps}
%         \end{figure}
%     \end{block}
%     \begin{block}<only@2>{Location Distribution}
%         \begin{columns}[T]
%             \column{0.45\textwidth}
%             \begin{figure}
%                 \centering
%                 \includegraphics[width=0.9\textwidth]{figs-12102020/case2_estimations.eps}
%             \end{figure}
%             \column{0.45\textwidth}
%             \begin{figure}
%                 \centering
%                 \includegraphics[width=0.94\textwidth]{figs-12102020/case2_joint_estimation.eps}
%             \end{figure}    
%         \end{columns}
%     \end{block}
% \end{frame}

\begingroup
\footnotesize
\begin{frame}{Analysis}
    \begin{table}[H]
        \centering
        \begin{tabular}{@{}p{2cm}c{0.6cm}c{0.6cm}c{0.6cm}c{0.6cm}@{}}
            Case-1 & $\norm{\epsilon}_2 [cm]\footnote[frame]{Euclidean distance between the mean and the actual location.} $ & $\sigma_x [cm]$ & $\sigma_y [cm]$ & $\epsilon_{M}\footnote[frame]{Mahalanobis distance between the joint estimation and the actual location.}$ \\ \toprule
            Heuristic & 19.14 & & & \\
            Perfect calib. & 7.6964e-02 & .46 & .69 & 0.1111 \\
            Self-calib. & TODO & TODO & TODO & TODO \\ \bottomrule
        \end{tabular}
        % \caption{Properties of the simulated accelerometers}
        \label{tab:case1}
    \end{table}
    
    \begin{table}[H]
        \centering
        \begin{tabular}{@{}p{2cm}c{0.6cm}c{0.6cm}c{0.6cm}c{0.6cm}@{}}
            Case-2 & $\norm{\epsilon}_2 [cm]$ & $\sigma_x [cm]$ & $\sigma_y [cm]$ & $\epsilon_{M}$ \\ \toprule
            Heuristic & 85.31 & & & \\
            Perfect calib. & 0.5425 & 5.13 & 5.11 & 0.1057 \\
            Self-calib. & TODO & TODO & TODO & TODO \\ \bottomrule
        \end{tabular}
        % \caption{Properties of the simulated accelerometers}
        \label{tab:case1}
    \end{table}
\end{frame}
\endgroup

\section[Conclusions]{Conclusions \& Future Work}
\begin{frame}{Summary \& Conclusions}
    In this presentation:
    \begin{columns}[T]
        \column{0.5\textwidth}
        \justify
        \begin{itemize}
            \pause \item derived a model which estimates the distribution of \underline{true energy from noisy signal}
            \pause \item formulated \underline{a localization technique} based on the estimated energy distribution
            \pause \item validated probabilistic perspective of the \underline{localization under uncertainties}
        \end{itemize}
        \column{0.45\textwidth}
        \begin{figure}[ht]
            \begin{overlayarea}{5cm}{4cm} % your height
             \only<2>{\includegraphics[width=\textwidth]{figs-12102020/timesignal.eps}}
             \only<3>{\includegraphics[width=\textwidth]{figs-12102020/case2_estimations.eps}}
             \only<4>{\includegraphics[width=\textwidth]{figs-12102020/case2_joint_estimation.eps}}
             \end{overlayarea}
        \end{figure}
    \end{columns}
    
\end{frame}

\begin{frame}{Future Work}
    \begin{itemize}
        \pause \item Validation on more locations
        \pause \item Validation on different sensor layout
        \pause \item Calculation of the error bounds of the system as a function of location
        \pause \item Investigate other solvers/techniques, e.g. genetic algorithms, fuzzy-logic, etc., for better performance \& accuracy
        \pause \item Drift to be incorporated in the model
    \end{itemize}
\end{frame}

\begin{frame}{Questions?}
    \begin{figure}[ht]
        \includegraphics[width=0.45\textwidth]{figs-12102020/case1_joint_estimation.eps}
        \includegraphics[width=0.45\textwidth]{figs-12102020/case2_joint_estimation.eps}
        \caption{\textbf{Left:} PDF of the target location who is in the convex hull. \textbf{Right:} PDF of the target location who is outside of the convex hull.}
    \end{figure}
\end{frame}