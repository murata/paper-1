\begin{frame}[t]{Introduction}
\begingroup
\small
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
        Consider the problem of estimating a footstep location under measurement uncertainty and possibly with Byzantine sensors.
    \end{column}
    \begin{column}{0.5\textwidth}
        $\vect{x}_{true} \in \mathcal{R}^2$: True occupant location\\
        $\mathcal{M} \triangleq \{1, \ldots, m\}$: Index set for $m$ sensors placed under a dispersive floor \\ 
        $\vect{t}_i$: Sensor location in localization space $i \in \mathcal{M}$ \\
        $\mathcal{S} = \{ (x, y): \left(x_{min} \leq x \leq x_{max}\right) \wedge \left(y_{min} \leq y \leq y_{max}\right)\}$: 
        The vibro-measurements of sensor $i$ associated with a heel strike of the footstep is $z_i[k]$ when the true measurement should be $z_{true, i}[k]$ at time samples $k \in \{1, \ldots, n\}$.
        In this study, we define a measurement error due to characteristics of the sensor as $\zeta_i[k]$.
        The relationship between the true vibro-measurements and the sensor's output can be defined as,
    
        \begin{equation}
            z_i[k] = z_{true, i}[k] + \zeta_i[k]~.
            \label{eq:zi}
        \end{equation}
    
    \end{column}
\end{columns}
\endgroup

\end{frame}
\subsection{Method}
\begin{frame}[t]{Signal Model of a Single Axis Accelerometer}
\begingroup
\small
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/signal_model.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
        \vspace*{-\baselineskip}
        \begin{alertblock}{Signal Model of a Single Axis Accelerometer}
            Accelerometer measurements can be stated as,
            \begin{equation}
                \label{eq:signal}
                \boxed{
                    z[k] = z_t[k] +\zeta[k]
                }
            \end{equation}
            where, \\
            $z_t[k] \in \mathbb{R}$: True vibration signal \\
            $z[k] \in \mathbb{R}$: Sensor measurements \\
            $\zeta[k] \sim \N{0}{\sigma_\zeta} \in \mathbb{R}$: Measurement error \\
            $k = 1, \ldots, n$ time steps.
        \end{alertblock}
    \end{column}
\end{columns}
\endgroup
\end{frame}


\begin{frame}{Signal Energy under Uncertainty}
\begin{columns}[t]
    \begin{column}{0.5\textwidth}
        \begingroup
        \small
        In the light of the previous equations, the signal energy can be derived as,

        \begin{subequations}
            \begin{align}
                e &= e_t + \varepsilon \\
                &= \sum\limits_{k=1}^n z[k]^2 = \sum\limits_{k=1}^n \left(z_t[k] + \zeta[k] \right)^2
            \end{align}
        \end{subequations}
        where $e_t$ and $\varepsilon$ represent the true signal energy and its uncertainty. 
        For large $n$:
        \begin{equation}
            \boxed{\Rightarrow e \sim f_{E}(e; e_t) \approx \N{\mu_{E}(e_t)}{\sigma_{E}(e_t)}}
        \end{equation}
        where $\mu_E(e_t) = e_t + n \sigma_\zeta^2$ and
        $\sigma_{E}^2(e_t) = 2 n \sigma_\zeta^4 + 4 e_t  \sigma_\zeta^2$.
        \endgroup
    \end{column}

    \begin{column}{0.5\textwidth}
        \vspace*{-\baselineskip}
        \begingroup
        \footnotesize
        \begin{alertblock}{Recall}
            \textbf{Rayleigh's Energy Equation}: 
            Energy of a discrete time domain signal $z[k] \in \mathbb{R}$ is shown in \Cref{eq:rayleigh} 
            \begin{equation}
                \label{eq:rayleigh}
                \boxed{e = \sum_{k=1}^{n} z[k]^2.}
            \end{equation}

            \textbf{Squared sum of $n$ number of normally distributed random variables with unit variance}: $X_i \sim \NSTD{\mu_i}{1}$ where $i=\{1,\ldots,n\}$ is given by $Y = \sum\limits_{i=1}^{n} X_i^2 \sim \chi^{\prime 2}_n \left( \lambda \right)$ where $\lambda = \sum\limits_{i=1}^{n} \mu_i^2 $.

            If $n>20$:
            \begin{equation}
                \boxed{\Rightarrow Y \simeq \mathcal{N}\left(n + \lambda, 2 n + 4 \lambda \right)}
            \end{equation}.
        \end{alertblock}
        \endgroup
    \end{column}
\end{columns}
\end{frame}

\begin{frame}[t, label=current]{Localization Uncertainty Given Noisy Accelerometers}{Derivation of the Localization Uncertainty}
\begin{columns}[T]
    \begin{column}{0.5\textwidth}
        \begingroup
        \justifying
        \normalsize
        \alert{Define:} \\ 
        $\vect{x}^i = (x^i, y^i)^\top \in \mathbb{R}^2$: Location of the $i^{th}$ sensor \\
        $\vect{x} = (x, y)^\top \in \mathbb{R}^2$: Estimated occupant location \\
        $d = \norm{\vect{x} - \vect{x}^i} = \dfrac{1}{\beta_0} \log{\dfrac{e_i-\beta_1}{e_0}}$: Distance between the impact and the $i^{th}$ sensor. 
        \vspace*{\fill}
        \endgroup
    \end{column}

    \begin{column}{0.5\textwidth}
        \vspace*{-\baselineskip}
        \begin{alertblock}{Localization Function:}
            Following \citet{Alajlouni2018}, the localization function is as given below:
            \begin{equation*}
                \boxed{d = \vect{f}(e) = \frac{1}{\beta} \log{\frac{e}{e_0}}}
            \end{equation*}
            \begin{figure}[t]
                \includegraphics[width=0.6\textwidth]{figures/decay.eps}
                \caption{The localization function $\vect{f}(e)$ assumes that energy of the vibration signal decays exponentially as a function of the distance between the sensor and the occupant.}
            \end{figure}
        \end{alertblock}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}[t, allowframebreaks]{Localization Uncertainty Given Noisy Accelerometers}
    \alert{Derive:} $f_{\vect{X}}(\vect{x})$ Density Transformation Theorem [cf. Appendix] \\
    \begin{subequations}
        \begin{equation}
            f_{\vect{X}}(\vect{x}) = f_{D, \theta}\left( \norm{\vect{x} - \vect{x}^i}, \tan^{-1}(\norm{\vect{x} - \vect{x}^i}) \right)
        \end{equation}
        because $d$ and $\theta$ are independent from each other: \\
        \begin{equation}
            f_{\vect{X}}(\vect{x}) = \frac{1}{\norm{\vect{x} - \vect{x}^i}} f_{D}\left( \norm{\vect{x} - \vect{x}^i} \right) f_{\theta}\left( \tan^{-1}(\norm{\vect{x} - \vect{x}^i}) \right)
        \end{equation}.
        Since accelerometers cannot inform us about the directionality of the occupant $\theta$, we assume $f_\theta(\theta)$ is uniformly distributed between $[0, 2\pi)$, i.e., $f_\theta(\theta) = \dfrac{1}{2 \pi}$.
        \begin{equation}
            f_{\vect{X}}(\vect{x}) = \frac{1}{2 \pi \norm{\vect{x} - \vect{x}^i}} f_{D}\left( \norm{\vect{x} - \vect{x}^i} \right) 
        \end{equation}
        where $f_D(d)$ can be obtained by invoking the Density Transformation Theorem between $e$ and $d$:\\ 
        \begin{equation}
            f_D(d) = \left \lvert \beta_0 \right \rvert e_0 \exp{\left(\beta_0 d\right)} f_E\left(e_0 \exp{\left(\beta_0 d \right)} + \beta_1; e_t \right) 
        \end{equation}
        By compiling everything together:
        \begin{equation*}
            \boxed{
                f_{\vect{X}}(\vect{x}; e_t) = \frac{\left \lvert \beta_0 \right \rvert  e_0 \exp{\left(\beta_0 \norm{\vect{x} - \vect{x}^i}\right)}}{2 \pi \norm{\vect{x} - \vect{x}^i}} f_E\left( e_0 \exp{\left(\beta_0 \norm{\vect{x} - \vect{x}^i} \right)} + \beta_1 ; e_t \right)
            } 
        \end{equation*}
        Since $m$ number of accelerometers are available in the environment and they are indepedent from each other, we can parameterize the joint likelihood as:
        \begin{equation*}
            f_{\vect{X}}\left(\vect{x}_1, \ldots, \vect{x}_m; e_{t,1} \ldots e_{t, m} \right) = \prod_{i=1}^{m} f_{\vect{X}}\left(\vect{x}_i ; e_{t,i} \right) 
        \end{equation*}
    \end{subequations}
    \begin{subequations}
        \begin{equation*}
            \hat{\vect{x}} = \argmax_{\vect{x}} \prod_{i=1}^{m} f_{\vect{X}}\left(\vect{x}_i ; e_{t,i} \right) 
        \end{equation*}
        or equivalently,
        \begin{equation*}
            \hat{\vect{x}} = \argmin_{\vect{x}} -\sum_{i=1}^{m} \log{f_{\vect{X}}\left(\vect{x}_i ; e_{t,i} \right)} 
        \end{equation*}
    \end{subequations}
    Finally, we can discretize $f_{\vect{x}}(\cdot)$ for a given set of $(x_i, y_i)^\top$ tuples; thus, evaluating the \gls{pdf} over the localization space.

    The optimization problem in hand is now much easier to solve with gradient descent algorithm tweaking the energy error values $(\varepsilon_1, \cdots, \varepsilon_m)$.
\end{frame}

\subsection[Simulations]{Parametric Study with Simulations}
\begin{frame}[t, allowframebreaks]{Simulation Setup}
    \begin{itemize}
        \item Floor plane is simulated as a thin-plate.
        \item The thin plate model is later discretized and linearized with \gls{fem}.
    \end{itemize}
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \begin{table}
                \begin{tabular}{l c}
                    Property & Unit \\ \toprule
                    Mass distribution $\rho$: & 2443 $[g \cdot m^{-2}]$ \\
                    Young's modulus $E$: & 31.49e9 $[Pa]$ \\
                    Poisson's ratio $v$: & 0.2 \\ \bottomrule
                \end{tabular}
                \caption{Material properties of the plate}
            \end{table}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{table}
                \begin{tabular}{l c}
                    Property & Unit \\ \toprule
                    Dimensions: & 1x1x25e-3 $[m]$ \\
                    Number of Elements: & 12x12 \\
                    B.C.: & Simply Supported \\ \bottomrule
                \end{tabular}
                \caption{Geometric Properties of the plate}
            \end{table}
        \end{column}
    \end{columns}
\end{frame}

% \begin{frame}{Forcing}
%     \begin{figure}
%         \includegraphics[width=\textwidth]{example-image-a}
%         \caption{Forcing}
%     \end{figure}
% \end{frame}

% \begin{frame}{Sensor Placement \& Measurements}
%     \begin{figure}
%         \includegraphics[width=0.6\textwidth]{example-image-a}
%         \caption{Sensor Placement}
%     \end{figure}
% \end{frame}

\begin{frame}{Propagation Curve (Calibration) -- \rnum{1}}
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{figures/calibration-measured-energy-spatial.png}
                \caption{Measured Signal Energy over Localization Space}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{figures/calibration-propagation-curve.png}
                \caption{Propagation Curve}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Propagation Curve (Calibration) -- \rnum{2}}
    \begin{columns}[T]
        \begin{column}{0.3\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{figures/calibration-measured-energy-spatial.png}
                \caption{Measured Signal Energy over Localization Space}
            \end{figure}
        \end{column}
        \begin{column}{0.3\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{figures/calibration-estimated-energy-spatial.png}
                \caption{Estimated Signal Energy over Localization Space}
            \end{figure}
        \end{column}
        \begin{column}{0.3\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{figures/calibration-residuals-energy-spatial.png}
                \caption{Calibration Residuals}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Results}
\begin{frame}{Localization Results -- \rnum{1}}
    \begin{figure}
        \includegraphics[height=0.85\textheight]{figures/localization-baseline-spatial.png}
        \caption{Baseline (Multilateration Results)}
    \end{figure}
\end{frame}

\begin{frame}{Localization Results -- \rnum{2}}
    \begin{figure}
        \includegraphics[width=0.85\textwidth]{figures/spatial-4.png} \\
        \includegraphics[width=0.85\textwidth]{figures/spatial-6.png} \\
        \includegraphics[width=0.85\textwidth]{figures/spatial-7.png}

        \caption{Joint Likelihood}
    \end{figure}
\end{frame}
