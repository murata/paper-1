load("../data/goodwin_step_data.mat");
experiment = experiment2;
names = ["Baseline", "Proposed-1"];

nx = 900; ny = 300;
space = generate_space(13, 31, 27, 33, nx, ny);
d_true = pdist2(experiment.sensor_locations', experiment.groundtruth');
xy = [space.xx(:), space.yy(:)];
options = optimset('Display', 'off');
fd_model = "uniform";

nsensors_used = 11;
nsteps = 3;
ncases_total = 2^nsensors_used - 1;
joint_information = zeros(nsteps, ncases_total);
results_all_ours = zeros(nsteps, ncases_total, 2);
results_all_theirs = zeros(nsteps, ncases_total, 2); % techniques x steps x cases x ndim

errors_all_ours = zeros(nsteps, ncases_total, 2);
errors_all_theirs = zeros(nsteps, ncases_total, 2);
cases = zeros(ncases_total, nsensors_used);

for i=1:ncases_total
    bstr = dec2bin(i, nsensors_used);
    cases(i, :) = str2num(reshape(bstr',[],1));
end
nsensors_total = sum(cases, 2);
fit_curves;
duration = zeros(nsteps, 1);
code_start = time();
kappas = zeros(nsteps, nsensors_used);
sensors_chosen = zeros(nsteps, ncases_total, 2);

for step_idx=[5, 15, 22]
    % fprintf("Started Estimating the Latent-Variable kappa for all sensors\n");
    % parfor sensor_idx = 1:nsensors_used
    %     fd_model = "uniform";
    %     fun = @(x)problem(x, step_idx, sensor_idx, experiment, e, models{sensor_idx}, nx, ny, nsamples, xy, space, fd_model);
    %     kappas(step_idx, sensor_idx) = fminbnd(fun, 0, 1, options);
    % end
    % fprintf("Estimation of the Latent-Variable kappa for all sensors is done\n");

    step_start = time();
    parfor case_idx=randsample(1:ncases_total, 100)
        warning('off','all');
        now = time();
        template = cases(case_idx, :);
        sensor_idx = find(template);
        nsensors = length(sensor_idx);

        if nsensors == 1
            continue;
        end

        L = experiment.sensor_locations(:, sensor_idx);
        ee = e(step_idx, sensor_idx);
        estimate = L * ee' / sum(ee);
        results_all_theirs(step_idx, case_idx, :) = estimate;
        errors_all_theirs(step_idx, case_idx, :) = estimate - experiment.groundtruth(:, step_idx);
        fxy = zeros(ny, nx, nsensors);
        metric = zeros(nsensors, 1);

        for qq = 1:nsensors
            sensor_idx_idx = sensor_idx(qq);
            fun = @(x)problem(x, step_idx, sensor_idx_idx, experiment, e, models{sensor_idx_idx}, nx, ny, nsamples, xy, space, fd_model);
            [~, fx] = fun(1);
            % [~, fx] = fun(kappas(step_idx, sensor_idx_idx));
            info = -fx .* log2(fx);
            info(isnan(info)) = 0;
            % mu_x = trapz(space.x, trapz(space.y, space.x .* fx));
            % mu_y = trapz(space.x, trapz(space.y, space.y' .* fx));
            % var_x = trapz(space.x, trapz(space.y, space.x.^2.* fx)) - mu_x^2;
            % var_y = trapz(space.x, trapz(space.y, space.y'.^2 .* fx)) - mu_y^2;
            fxy(:, :, qq) = fx;
            metric(qq) = trapz(space.x, trapz(space.y, info));
        end

        if nsensors == 2
            idx = [1, 2];
        else
            %% median separation
            med = median(metric);
            class_labels = metric >= med;
            %% kmeans
            % [idx,C] = kmeans(metric, 2);
            % [~, coi] = max(C);
            % class_labels = idx(coi);
            idx = find(class_labels==1);
        end
        % [sorted_metric, idx] = sort(metric, 'descend'); 
        % sensors_choosen_this_step = idx(1:2);
        % sensors_chosen(step_idx, case_idx, :) = sensors_choosen_this_step;
        joint = prod(fxy(:, :, idx), 3);
        division = trapz(space.x, trapz(space.y, joint));
        joint_pdf = joint / (division+eps);
        joint_info = -joint_pdf .* log2(joint_pdf);
        joint_info(isnan(joint_info)) = 0;

        joint_information(step_idx, case_idx) = trapz(space.x, trapz(space.y, joint_info));

        [C,I] = max(joint(:));
        if C == 0
            estimate = [mean(space.x); mean(space.y)];
        else
            [Ji, Ii] = ind2sub(size(joint), I);
            estimate =  [space.x(Ii); space.y(Ji)];
        end
        % Mean or Mode?
        % mu_x = trapz(space.x, trapz(space.y, space.x .* joint_pdf));
        % mu_y = trapz(space.x, trapz(space.y, space.y' .* joint_pdf));

        %% Plots for the debugging
        mkdir(sprintf("%s/%s/step-%03d", foldername, names(2), step_idx));
        % mySave(sprintf("%s/%s/step-%03d/case-%03d.mat", foldername, names(2), step_idx, case_idx), fxy);

        fname = sprintf("%s/%s/step-%03d/case-%04d.png", foldername, names(2), step_idx, case_idx);
        h = figure('visible', 'off', 'renderer', 'painter','Position', [0, 0, 400, 100]);
        contour(space.x, space.y, joint, 50); hold on;
        scatter(estimate(1), estimate(2), 'rx');
        scatter(experiment.groundtruth(1, step_idx), experiment.groundtruth(2, step_idx), 'g.');
        scatter(experiment.sensor_locations(1, sensor_idx), experiment.sensor_locations(2, sensor_idx), 'bo');
        scatter(experiment.sensor_locations(1, sensor_idx(idx)), experiment.sensor_locations(2, sensor_idx(idx)), 'b+');

        xlabel('$x$ [m]'); ylabel('$y$ [m]'); grid on; grid minor;
        axis equal; axis tight; drawnow;
        saveas(h, fname);
        close(h);
        results_all_ours(step_idx, case_idx, :) = estimate;
        errors_all_ours(step_idx, case_idx, :) = estimate - experiment.groundtruth(:, step_idx);

        if mod(case_idx, 89) == 0
            fprintf("Step ID: %3d, Case ID: %4d, nsensors: %3d Duration: %1.3f\n", step_idx, case_idx, nsensors, seconds(time()- now));
            now = time();
        end
    end
    qq_theirs = squeeze(vecnorm(errors_all_theirs(step_idx, :, :), 2, 3));
    qq_ours = squeeze(vecnorm(errors_all_ours(step_idx, :, :), 2, 3));
    for i=2:nsensors_used
        case_idx = find(nsensors_total==i);
        % sensor_idx = find(cases(case_idx, :)==1);
        this_errors_theirs = qq_theirs(case_idx);
        desc_theirs = describe(this_errors_theirs(:));
        this_errors_ours = qq_ours(case_idx);
        desc_ours = describe(this_errors_ours(:));
        fprintf("Processing: %10s nsensors: %3d Mean: %1.2f Std: %1.2f Med: %1.2f RMS: %1.2f Min: %1.2f Max:%1.2f\n", "Baseline", i, desc_theirs);
        fprintf("Processing: %10s nsensors: %3d Mean: %1.2f Std: %1.2f Med: %1.2f RMS: %1.2f Min: %1.2f Max:%1.2f\n", "Proposed", i, desc_ours);
    end
    duration(step_idx) = seconds(time()-step_start);
    mean_duration = sum(duration) / nnz(duration);
    ncompleted = nnz(duration) / numel(duration);
    fprintf("Step Idx: %3d Duration: %3.3f [secs] Completed: %2.2f ETA: %10s\n", step_idx, duration(step_idx), 100*ncompleted, code_start + seconds(nsteps * mean_duration));
end

save(foldername + "/results_all.mat", '-v7.3');
% analyze;
% make_video;

function [f, varargout] = problem(kappa, step_idx, sensor_idx, experiment, e, model, nx, ny, nsamples, xy, space, fd_model)
    xi = experiment.sensor_locations(:, sensor_idx)';
    delta = xi - experiment.groundtruth(:, step_idx)';
    [~, qp] = min(abs(space.x - experiment.groundtruth(1, step_idx)));
    [~, qq] = min(abs(space.y - experiment.groundtruth(2, step_idx)));

    beta = [model.a, model.b, atan2(delta(2), delta(1))];
    prob = pdf_fx(xy, xi, e(step_idx, sensor_idx)*kappa, nsamples(step_idx), experiment.sigma_zeta, beta, fd_model);
    qwert = reshape(prob, [ny, nx]);
    division = trapz(space.x, trapz(space.y, qwert));
    fxy = qwert / (division + eps);
    % f = -log(max(fxy(:)));
    f = -log(fxy(qq, qp));
    if isinf(f)
        f = 999999999999;
    end
    nout = max(nargout,1) - 1;
    for k = 1:nout
        varargout{k} = fxy;
    end

end

function mySave(filenm, fxy)
    save(filenm, 'fxy');
end
