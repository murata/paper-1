function [consensus_set, cost_history, joint_consensus, entropy_consensus] = bse(fxy, space)
%bse - Byzantine Sensor Elimination algorithm
%
nsensors = size(fxy, 3);
index_set = 1:nsensors;
initial_sets = nchoosek(index_set, 2);
nsets = size(initial_sets, 1);

initial_metric = zeros(nsets, 1); 
for i=1:size(initial_sets, 1);
    [~, info] = fusion(fxy(:, :, initial_sets(i, :)), space);
    initial_metric(i) = info;
end
[~, min_id] = min(abs(initial_metric));
consensus_set = initial_sets(min_id, :);
[joint_consensus, entropy_consensus] = fusion(fxy(:, :, consensus_set), space);
diff_set = setdiff(index_set, consensus_set);
cost_history = zeros(size(diff_set));

for i=1:length(diff_set)
    candidate = diff_set(i);
    hypothesis_set = union(consensus_set, candidate);
    [joint_hypothesis, entropy_hypothesis] = fusion(fxy(:, :, hypothesis_set), space);

    if entropy_hypothesis < entropy_consensus
        consensus_set = hypothesis_set;
        [joint_consensus, entropy_consensus] = fusion(fxy(:, :, consensus_set), space);
    end
    cost_history(i) = entropy_consensus;
end

end
