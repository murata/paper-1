clc; close all; clearvars; prepare_generic;
warning off;
if ~exist('oc1', 'var')
    oc1 = load("../data/occupant1.mat");
end
if ~exist('oc2', 'var')
    oc2 = load("../data/occupant2.mat");
end
load("../data/goodwin_step_data.mat");
energy_oc1 = load("../data/automatic_detection_energy_subject_1.mat");
energy_oc2 = load("../data/automatic_detection_energy_subject_2.mat");
experiment = experiment1;
error = nan(162, 2047, 2);
nsensors = zeros(2047, 1);
%% baseline
for step_idx=1:162
    gt = experiment.groundtruth(:, step_idx);
    for case_idx=1:2047
        sensor_idx = find(str2num(reshape(dec2bin(case_idx, 11), [], 1)));
        if step_idx == 1
            nsensors(case_idx) = length(sensor_idx);
        end
        if length(sensor_idx) < 2
            continue;
        end
        L = experiment.sensor_locations(:, sensor_idx);
        ee_oc1 = energy_oc1.e(step_idx, sensor_idx);
        estimate_oc1 = L * ee_oc1' / sum(ee_oc1);
        error(step_idx, case_idx, 1) = vecnorm(estimate_oc1 - gt, 2);
        ee_oc2 = energy_oc2.e(step_idx, sensor_idx);
        estimate_oc2 = L * ee_oc2' / sum(ee_oc2);
        error(step_idx, case_idx, 2) = vecnorm(estimate_oc2 - gt, 2);
    end
end
%% error analysis - theirs
error_descs_theirs = zeros(10, 6, 2);
for sensor_idx = 2:11
    idx = find(nsensors == sensor_idx);
    error_descs_theirs(sensor_idx-1, :, 1) = describe(reshape(error(:, idx, 1), [], 1));
    error_descs_theirs(sensor_idx-1, :, 2) = describe(reshape(error(:, idx, 2), [], 1));
end

%% error analysis - ours
error_descs = zeros(10, 6, 2);
entropy_descs = zeros(10, 6, 2);
for nsensors = 2:11
    idx = oc1.t.nsensors == nsensors;
    errors = oc1.t(idx, :).normed_error;
    entropies = oc1.t(idx, :).entropy;
    error_descs(nsensors-1, :, 1) = describe(errors);
    entropy_descs(nsensors-1, :, 1) = describe(entropies);
    idx = oc2.t.nsensors == nsensors;
    errors = oc2.t(idx, :).normed_error;
    entropies = oc2.t(idx, :).entropy;
    error_descs(nsensors-1, :, 2) = describe(errors);
    entropy_descs(nsensors-1, :, 2) = describe(entropies);
end
return 
% errors vs number of sensors

h = figure('visible', 'off', 'renderer',  'painters', 'Units', 'centimeters', 'InnerPosition', [0 0 5 5]); hold on;
espace = linspace(0, 20, 210);
qq = error(:, :, 1);
qq = qq(:);
qq(isnan(qq)) = [];
[~, epdf_baseline_oc1] = epdf(qq, espace);
[~, epdf_proposed_oc1] = epdf(oc1.t.normed_error, espace);
plot(espace, epdf_baseline_oc1);
plot(espace, epdf_proposed_oc1);
grid on; grid minor; legend("Baseline", "Proposed");
exportgraphics(h, "../figures/error-pdf-oc-1.eps", "Resolution", 600);
close(h);

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'InnerPosition', [0 0 5 5]); hold on;
qq = error(:, :, 2);
qq = qq(:);
qq(isnan(qq)) = [];
[~, epdf_baseline_oc2] = epdf(qq, espace);
[~, epdf_proposed_oc2] = epdf(oc2.t.normed_error, espace);
plot(espace, epdf_baseline_oc2);
plot(espace, epdf_proposed_oc2);
grid on; grid minor; legend("Baseline", "Proposed");
exportgraphics(h, "../figures/error-pdf-oc-2.eps", "Resolution", 600);
close(h);

h = figure('visible', 'off', 'renderer',  'painters', 'Units', 'centimeters', 'InnerPosition', [0 0 5 5]); hold on;
plot(espace, cumtrapz(espace, epdf_baseline_oc1)/max(cumtrapz(espace, epdf_baseline_oc1)));
plot(espace, cumtrapz(espace, epdf_proposed_oc1)/max(cumtrapz(espace, epdf_proposed_oc1)));
grid on; grid minor; legend("Baseline", "Proposed");
exportgraphics(h, "../figures/error-cdf-oc-1.eps", "Resolution", 600);
close(h);

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'InnerPosition', [0 0 5 5]); hold on;
[~, epdf_baseline_oc2] = epdf(qq, espace);
[~, epdf_proposed_oc2] = epdf(oc2.t.normed_error, espace);
plot(espace, cumtrapz(espace, epdf_baseline_oc2)/max(cumtrapz(espace, epdf_baseline_oc2)));
plot(espace, cumtrapz(espace, epdf_proposed_oc2)/max(cumtrapz(espace, epdf_proposed_oc2)));
grid on; grid minor; legend("Baseline", "Proposed");
exportgraphics(h, "../figures/error-cdf-oc-2.eps", "Resolution", 600);
close(h);
return

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 10.5 5]); hold on;
plot(2:11, mean_errors(2:11, 1), 'r-o');
plot(2:11, mean_errors(2:11, 2), 'k-o');
plot(2:11, mean_errors(2:11, 1)+2*std_errors(2:11, 1), 'r-.');
plot(2:11, mean_errors(2:11, 2)+2*std_errors(2:11, 2), 'k-.');
% plot(2:11, rms_errors(2:11, 1), 'r');
% plot(2:11, rms_errors(2:11, 2), 'k');
plot(2:11, mean_errors(2:11, 1), 'r');
plot(2:11, mean_errors(2:11, 2), 'k');
grid on; grid minor; xticks([2:12]); yticks([0:11]); xlim([2, 11]); ylim([0 11]);
xlabel('Number of Sensors $m$');
ylabel('Localization Error [m]')
legend("Baseline (mean)", "Proposed (mean)", "Baseline (CI 0.95)", "Proposed (CI 0.95)", "Baseline (rms)", "Proposed (rms)", "location", "bestoutside", "box", "off")
exportgraphics(h, oc1.foldername + "/sensor-errors.eps", "Resolution", 600);
close(h);
%% information analysis
min_information = zeros(11, 2);
mean_information = zeros(11, 2);
std_information = zeros(11, 2);
for i=2:11
    case_idx = find(oc1.nsensors_total==i);
    this_info = squeeze(oc1.joint_information(:, case_idx));
    desc = describe(this_info(:));
    min_information(i, 1) = desc(end-1);
    mean_information(i, 1) = desc(1);
    std_information(i, 1) = desc(2);

    this_info = squeeze(oc2.joint_information(:, case_idx));
    desc = describe(this_info(:));
    min_information(i, 2) = desc(end-1);
    mean_information(i, 2) = desc(1);
    std_information(i, 2) = desc(2);
end
h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 10.5 5]); hold on;
plot(2:11, min_information(2:11, 1), 'k.');
plot(2:11, min_information(2:11, 2), 'r.');
plot(2:11, min_information(2:11, 1), 'k');
plot(2:11, min_information(2:11, 2), 'r');
plot(2:11, mean_information(2:11, 1), 'r');
plot(2:11, mean_information(2:11, 2), 'k');
plot(2:11, mean_information(2:11, 1) + 2 * std_information(2:11, 1), 'r:');
plot(2:11, mean_information(2:11, 1) - 2 * std_information(2:11, 1), 'r:');
plot(2:11, mean_information(2:11, 2) + 2 * std_information(2:11, 2), 'k:');
plot(2:11, mean_information(2:11, 2) - 2 * std_information(2:11, 2), 'k:');
lgd = legend("Min. Ent.  - OC1", "Min. Ent.  - OC2", "", "", "Mean Ent. - OC1", "Mean Ent. - OC2", "CI - OC1", "", "CI - OC2", "location", "bestoutside", "box", "off");
xlabel('Number of Sensors $m$'); ylabel("Joint Entropy [Shannons]");
xticks([2:11]); xlim([2, 11]); ylim([-12 5]);grid on; grid minor;
exportgraphics(h, oc1.foldername + "/sensor-entropies.eps", "Resolution", 600);
close(h);
%
h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 10.5 5]); hold on;
scatter(min_information(2:11, 1), mean_errors(2:11, 2, 1), 'kx');
scatter(min_information(2:11, 2), mean_errors(2:11, 2, 2), 'r+');

% for i=2:11
%     text(min_information(i, 1) + 0.2, mean_errors(i, 2, 1), "$\leftarrow$ " + num2str(i));
% end

r2_oc1 = round(corr(min_information(2:11, 1), mean_errors(2:11, 2, 1)), 2);
r2_oc2 = round(corr(min_information(2:11, 2), mean_errors(2:11, 2, 2)), 2);
legend("Occupant-1", "Occupant-2", "location", "bestoutside", "box", "off");
title("$\rho_1$:" + string(r2_oc1) + ", $\rho_2$:" + string(r2_oc2));
xlabel('Min. Joint Entropy [Shannons]'); ylabel('Localization Error [meters]');
xlim([-12 -5]); ylim([0.5, 2.5]); xticks([-12:-5]); yticks([0.5:2.5]);
grid on; grid minor;
exportgraphics(h, oc1.foldername + "/entropy-vs-error.eps", "Resolution", 600);
close(h);
%

% h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'inches', 'Position', [0 0 3 3]); hold on;
% [xx, yy, zz] = ndgrid(2:11, min_information(2:11, 1), mean_errors(2:11, 2, 1));
% scatter3(xx(:), yy(:), zz(:));
% saveas(h, oc1.foldername + "/sensor-vs-entropy-vs-error.eps");
% close(h);

% h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'inches', 'Position', [0 0 3 3]); hold on;
% error_x = squeeze(oc1.errors_all(1, :, :, 1));
% error_y = squeeze(oc1.errors_all(1, :, :, 2));
% scatter(error_x(:), error_y(:), 'k.');
% %
% error_x = squeeze(oc1.errors_all(2, :, :, 1));
% error_y = squeeze(oc1.errors_all(2, :, :, 2));
% scatter(error_x(:), error_y(:), 'r.');
% %
% error_x = squeeze(oc2.errors_all(2, :, :, 1));
% error_y = squeeze(oc2.errors_all(2, :, :, 2));
% scatter(error_x(:), error_y(:), 'g.');
% axis equal; grid on; grid minor;
% saveas(h, oc1.foldername + "/errors.eps");
% close(h);

h = figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
boxplot(squeeze(oc1.joint_information(2, :, :)), oc1.nsensors_total);
xlim([1, 12]);
grid on; grid minor;
ylabel("Joint Entropy [Shannons]");
xlabel("Number of Sensors $m$");
exportgraphics(h, oc1.foldername + "/entropy-violin.eps", "Resolution", 600);
close(h);

% h = figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
% boxplot(squeeze(normed_errors_oc1(2, :, :)), oc1.nsensors_total);
% grid on; grid minor;
% ylabel("Joint Entropy [Shannons]");
% xlabel("Number of Sensors $m$");
% exportgraphics(h, oc1.foldername + "/sensor-error-boxplot-proposed.eps", "Resolution", 600);
% close(h);

h = figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
for i=2:11
    case_idx = find(oc1.nsensors_total==i);
    this_info = squeeze(normed_errors_oc1(2, :, case_idx));
    boxchart(ones(numel(this_info), 1)*i, this_info(:));
end
ylim([0 20]);
grid on; grid minor;
ylabel("Localization Error [m]");
xlabel("Number of Sensors $m$");
exportgraphics(h, oc1.foldername + "/sensor-error-boxplot-proposed.eps", "Resolution", 600);
close(h);


h = figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
for i=2:11
    case_idx = find(oc1.nsensors_total==i);
    this_info = squeeze(normed_errors_oc1(1, :, case_idx));
    boxchart(ones(numel(this_info), 1)*i, this_info(:));
end
ylim([0 20]);
grid on; grid minor;
ylabel("Localization Error [m]");
xlabel("Number of Sensors $m$");
exportgraphics(h, oc1.foldername + "/sensor-error-boxplot-baseline.eps", "Resolution", 600);
close(h);

% h = figure('Units', 'inches', 'InnerPosition', [0 0 3 2.5]); hold on;
% running_sum = 0;
% boxplot(squeeze(normed_errors_oc1(2, :, :)), oc1.nsensors_total);
% xlim([1, 12]);
% grid on; grid minor;
% ylabel("Joint Differential Entropy [Shannons]");
% xlabel("Number of Sensors $m$");
% exportgraphics(h, oc1.foldername + "/entropy-violin.eps", "Resolution", 600);
% close(h);

