function [f, h] = fusion(f, space)
    f = prod(f, 3);
    f = normalize_likelihood(f, space);
    [h, ~] = entropy(f, space);
end
