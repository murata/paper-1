function [state_space, state_space_discrete, initial_state] = generate_state_space(plate, impact_idx, nstep, ts, method)
    state_space = cell(length(impact_idx), 1);
    for index = 1:length(impact_idx)
        tstart = time();
        logging("Generating state-space realization: Case %s", index);

        impact_id = impact_idx(index);
        At = [zeros(plate.dof) eye(plate.dof);
             -plate.K/plate.M   -plate.C/plate.M];
        Ct = At(plate.dof+1:end, :);
        Phi = zeros(plate.dof, nstep);
        Phi(impact_id*3, :) = 1;

        Bt = [zeros(plate.dof, nstep);
                (plate.M\Phi)];

        Dt = Bt(plate.dof+1:end, :);
        sys = ss(At, Bt, Ct, Dt);
        state_space{index} = sys;
        logging("State-space realization is done: Case %s", index);
        logging("Discretizing state-space realization: Case %s", index);
        state_space_discrete{index} = c2d(sys, ts, method);
        logging("Case %s was completed in %2.2f [secs]", index, seconds(time()-tstart));
    end
    initial_state = zeros(2*plate.dof, 1);
end
