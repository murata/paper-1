%% Import plate model (M,C,K)
%this file finds mode frequencies and mode shapes and addes this info to
%the plate variable FEplate
% close all;
% clear;
% clc;
% plateFile='fem.mat'; %plate of interest
% %filename='AlPlateModesDense'; %filename that will be saved with modes data
% tic
% %opengl software
% load(plateFile);
disp(' ');
disp(FEplate);
disp(' ');
M=FEplate.M;
K=FEplate.K;

xr=round(FEplate.xr,2);
yr=round(FEplate.yr,2);

dof=length(xr)*3;
nnode=length(xr);
%% View mode shapes and frequencies
%if plate is fixed and has 56 nodes then there will be 56 out of plane modes
%the remaining modes are in-plane modes
%It has been observed that most of the first 32 modes are out of plane modes
%%% modal parameters
tic
[UU,SS]=eig(K,M,'vector');
toc
SS=sqrt(SS)/(2*pi); %undamped natural frequencies
[S,I]=sort(SS);
U=UU(:,I);
% clear FEplateModes
% FeEplateModes.PlateName=PlateName;
% FEplateModes.Properties=FEplate;
% FEplateModes.ModeFreqs=S;
% FEplateModes.ModeShapes=U;
% save(filename,'FEplateModes');
FEplate.ModeFreqs=S;
FEplate.ModeShapes=U;
% save(plateFile,'FEplate');
%% Plot of first couple mode shapes
tri = delaunay(xr,yr);
figure('OuterPosition', [0 0 2000 2000],...
       'Units', 'points'); %first 9 modes (out of plane)
for j=1:9
subplot(3,3,j)
outPlaneComp=U(3:3:end,j)/norm(U(:,j));
trisurf(tri, xr, yr, outPlaneComp);

lighting phong
shading interp
colorbar
colormap(jet)

title({['mode No. ',num2str(j),' (',num2str(S(j)),' Hz)'];...
    ['norm of out-of-plane components in EigenVector=',...
    num2str(norm(outPlaneComp))]})
end
saveas(gcf,'1','epsc')

figure('OuterPosition', [0 0 2000 2000],...
       'Units', 'points');
count=0;
for j=10:18
    count=count+1;
subplot(3,3,count)
outPlaneComp=U(3:3:end,j)/norm(U(:,j));
trisurf(tri, xr, yr, outPlaneComp);

lighting phong
shading interp
colorbar
colormap(jet)

title({['mode No. ',num2str(j),' (',num2str(S(j)),' Hz)'];...
    ['norm of out-of-plane components in EigenVector=',...
    num2str(norm(outPlaneComp))]})
end

saveas(gcf,'2','epsc')
