function [f] = normalize_likelihood(f, space)
    division = trapz(space.x, trapz(space.y, f));
    f = f / (division+eps);
end
