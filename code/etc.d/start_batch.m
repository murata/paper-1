function [j, c] = start_batch(script_name, varargin)
    % start_batch Function
    %
    % Description:
    %   The start_batch function is designed to initiate a batch job on a parallel computing cluster.
    %   It prepares the environment, configures the cluster, and then submits a batch job using the specified parameters.
    %
    % Syntax:
    %   [j, c] = start_batch(script_name, varargin)
    %
    % Input Arguments:
    %   - script_name (string): Name of the script to be executed in the batch job.
    %   - varargin (variable input arguments): A series of optional name-value pairs to specify additional parameters for the batch job.
    %       - 'a': Account name for the batch job. Default is 'localization_experiment'.
    %       - 'n': Number of nodes to be used in the cluster. Default is 50.
    %       - 'd': Duration in days for the batch job. Default is 0.
    %       - 'h': Duration in hours for the batch job. Default is 5.
    %       - 'm': Duration in minutes for the batch job. Default is 30.
    %       - 'q': Queue name for the batch job. Default is 'normal_q'.
    %
    % Output Arguments:
    %   - j: Batch job object representing the submitted job.
    %   - c: Parallel cluster object used for the batch job.
    %
    % Example Usage:
    %   To start a batch job with the script named "myScript.m", using 100 nodes, for a duration of 1 day, 10 hours, and 45 minutes, in the "high_priority_q" q:
    %   [j, c] = start_batch('myScript', 'n', 100, 'd', 1, 'h', 10, 'm', 45, 'q', 'high_priority_q');

    prepare_generic; % Set up the environment
    configCluster;   % Configure the cluster

    p = inputParser;

    % Define optional name-value pairs for the input parser
    addOptional(p, 'a', 'localization_experiment');
    addOptional(p, 'n', 50);
    addOptional(p, 'd', 0);
    addOptional(p, 'h', 5);
    addOptional(p, 'm', 30);
    addOptional(p, 'q', 'normal_q');

    parse(p, varargin{:});

    % Extract parsed results
    a = p.Results.a;
    n = p.Results.n;
    d = p.Results.d;
    h = p.Results.h;
    m = p.Results.m;
    q = p.Results.q;

    % Format the duration for the batch job
    duration = sprintf("%d-%02d:%02d:00", d, h, m);
    setSchedulerMessageHandler(@disp);

    % Create a parallel cluster object and set its properties
    c = parcluster;
    c.AdditionalProperties.AccountName = a;
    c.AdditionalProperties.WallTime = convertStringsToChars(duration);
    c.AdditionalProperties.Partition = q;

    % Submit the batch job
    j = batch(c, script_name, 'pool', n);
end
