%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%             Plate FE Model with Fixed/Free BCs               %%%%%%%
%%%%%%                    Mohammad I. Albakri                       %%%%%%%
%%%%%%                     December 2015, VT                        %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{
Notes on code by Sa'ed Alajlouni:

This code builds a finite element model for a thin plate with either fixed
or free boundary conditions. The code generates Mass (M) and Stiffeness (K)
matrices in addition to a spatial distribution of nodes. 
A single node is a point on the plate that has 3
degrees of freedom; x,y, and z displacement. Each element in the FE model
is a rectangle/square with controllable dimentions and 8 nodes. The
resulting M, and K matrices describe an undamped (not true in reality)
plate, therefore a damping matrix (C) must be added to the system to
describe damping effects. For a free Aliminium Plate, C can be chosen to be
C = M + 7.2*10^(-8)*K  .Then the system dynamics can be described by:
M * (d^2/dt^2)(q) + C * (d/dt)(q) + K * (q)=0;
where q is vector containing the plates nodal degrees of freedom (dof);
q=[x1,y1,z1,.....,xn,yn,zn]' where the subscript denotes the respective
node on the plate.
The node count starts with the bottom left node then increases by moving 
vertically, then the count moves to the second node from the bottom left 
 and adds by moving vertically again, and so on. Example (a single element)

3 5 8
2   7 
1 4 6

The variables {x,y} denote spatial co-ordinates of nodes.
If fixed boundary conditions are used then M and K can be further reduced
since the response is zero for all boundary nodes, therefore the variables
{xr,yr} denote the reduced nodal co-ordinates vectors excluding nodes on
 the boundary.
%}
clc
clear all
fileName='data/FEplate_2x2_13x13.mat';
%% Plate Geometry and FE Mesh
%Thk=0.125*25.4e-3;
Thk=0.025;
%Width=240*25.4e-3;
Width=2;
Length=2;
%Length=60*25.4e-3;
BCs='Fixed'; % Select BCs 'Free' or 'Fixed'
%nw=8;nl=2;%nw*(Length/Width);  % No. of elements on length and width directions
nw=13;nl=13;
%% Material Properties
% Base Plate
%rho=2700; E=69e9; v=0.33;       material='Aluminum';
% rho=7854; E=206.84e9; v=0.3;   material='Steel';
rho=2443; E=31.49e9; v=0.2;      material='GWC';

%%
% Mesh generation
% Important: Elements should be all of the same size

nel=nl*nw;
nnode=2*nw+1;           %no. of nodes on a horizontal edge
ICt0=[1 3*(nw*2+2)-2 3*(3*nw+3)-2 3*(3*nw+4)-2 3*(3*nw+5)-2 3*(2*nw+3)-2 7 4];
% ICt1: Connectivity array for the first element, translation DoFs
ICt1=[1 2 3 3*(nw*2+2)-2 3*(nw*2+2)-1 3*(nw*2+2) 3*(3*nw+3)-2 3*(3*nw+3)-1 3*(3*nw+3)...
    3*(3*nw+4)-2 3*(3*nw+4)-1 3*(3*nw+4) 3*(3*nw+5)-2 3*(3*nw+5)-1 3*(3*nw+5) 3*(2*nw+3)-2 3*(2*nw+3)-1 3*(2*nw+3) 7 8 9 4 5 6];
cod=[2 2 2 1 1 1 2 2 2 2 2 2 2 2 2 1 1 1 2 2 2 2 2 2];   % Shift matrix to generate connectivity array

for k=1:nl
    for ii=1:nw
        for jj=1:24
            ICt(ii+(k-1)*nw,jj)=ICt1(jj)+3*(ii-1)*cod(jj)+(k-1)*(nw*3+2)*3;
        end
    end
end

cod=[2 2 1 1 2 2 2 2 2 2 1 1 2 2 2 2];   % Shift matrix to generate connectivity array for rotational DoFs
ICr0=[1 2 2*(nw*2+2)-1 2*(nw*2+2) 2*(3*nw+3)-1 2*(3*nw+3) 2*(3*nw+4)-1 2*(3*nw+4) 2*(3*nw+5)-1 2*(3*nw+5)...
    2*(2*nw+3)-1 2*(2*nw+3) 5 6 3 4];
ICr1=[ICr0 ICr0+1];
for k=1:nl
    for ii=1:nw
        for jj=1:16
            ICr(ii+(k-1)*nw,jj)=ICr1(jj)+(ii-1)*cod(jj)*2+(k-1)*(nw*3+2)*2;
        end
    end
end

nnod=max(max(ICt));
ndf=nnod;                   %translational dof
nrdf=max(max(ICr));         %rotational dof

% Coordinates generation
y0=0:Width/(2*nw):Width;
y1=0:2*Width/(2*nw):Width;
y=[y0';y1'];
x0=zeros(2*nw+1,1);
x1=zeros(nw+1,1)+Length/(2*nl);
x=[x0;x1];
for k=2:nl
    y=[y;y0';y1'];
    x=[x;x0+(k-1)*Length/nl;x1+(k-1)*Length/nl];
end
y=[y;y0'];
x=[x;x0+Length];
figure(1)
plot(x,y,'*')
axis equal
%% More Material properties

Q11=E/(1-v^2); Q22=Q11; Q12=v*E/(1-v^2);Q66=E/2/(1+v);
Cb=[Q11 Q12 0; Q12 Q11 0;0 0 Q66];
Cs=[Q66 0;0 Q66];

%Rigidity matrix
h1=-Thk/2;h2=h1+Thk;
ah=h2-h1;
bh=(h2^2-h1^2)/2;
ch=(h2^3-h1^3)/3;

Dtb=ah*Cb;  Dtrb=bh*Cb;  Drrb=ch*Cb;
Dts=ah*Cs;  Dtrs=Dts;    Drrs=Dts;

%% Elemental Formulation
% Coordinates
ecod=[0 0 0 Width/(2*nw) Width/nw Width/nw Width/nw Width/(2*nw);
    0 Length/(2*nl) Length/nl Length/nl Length/nl Length/(2*nl) 0 0];
area=0;

% Global Matrices Size
M=zeros(ndf,ndf); F=zeros(ndf,1);        %Global
Ktt=zeros(ndf,ndf); Ktr=zeros(ndf,nrdf); Krr=zeros(nrdf,nrdf);
% Elemental Matreces Size
Fe=zeros(24,1);
Ktbe=zeros(24,24);  Ktse=Ktbe; Ktrbe=zeros(24,16); Ktrse=Ktrbe;
Krrbe=zeros(16,16); Krrse=Krrbe;
Me=zeros(24,24);
% Elemental matrices generation, BENDING
gp=[-0.7745966692 0 0.7745966692]; % Full integration rule for quadratic elements
wgp=[0.5555555555 0.888888888 0.5555555555];

for igp=1:3
    for jgp=1:3
        r=gp(igp);s=gp(jgp);
        r1=1-r;r2=1+r;s1=1-s;s2=1+s;
        shape(1)=-r1*s1*(r2+s)/4;shape(2)=r1*r2*s1/2;
        shape(3)=-r2*s1*(r1+s)/4;shape(4)=r2*s1*s2/2;
        shape(5)=r2*s2*(s-r1)/4;shape(6)=r1*r2*s2/2;
        shape(7)=r1*s2*(s-r2)/4;shape(8)=r1*s1*s2/2;
        deriv(1,1)=s1*(s+2*r)/4;deriv(1,2)=-r*s1;
        deriv(1,3)=s1*(2*r-s)/4;deriv(1,4)=s1*s2/2;
        deriv(1,5)=s2*(s+2*r)/4;deriv(1,6)=-r*s2;
        deriv(1,7)=s2*(2*r-s)/4;deriv(1,8)=-s1*s2/2;
        deriv(2,1)=r1*(2*s+r)/4;deriv(2,2)=-r1*r2/2;
        deriv(2,3)=r2*(2*s-r)/4;deriv(2,4)=-s*r2;
        deriv(2,5)=r2*(r+2*s)/4;deriv(2,6)=r1*r2/2;
        deriv(2,7)=r1*(2*s-r)/4;deriv(2,8)=-r1*s;
        jacm=deriv*ecod';
        jaci=inv(jacm);
        cartd=jaci*deriv;
        
        Btb=zeros(3,24); kg=0;
        for k=1:3:22
            kg=kg+1;
            Btb(1,k)=cartd(1,kg);Btb(2,k+1)=cartd(2,kg);
            Btb(3,k)=cartd(2,kg);Btb(3,k+1)=cartd(1,kg);
        end
        
        Brb=zeros(3,16);kg=0;
        for k=1:2:15
            kg=kg+1;
            Brb(1,k)=cartd(1,kg);Brb(2,k+1)=cartd(2,kg);
            Brb(3,k)=cartd(2,kg);Brb(3,k+1)=cartd(1,kg);
        end
        
        Nt=zeros(3,24); in=0;
        for ik=1:3:22
            in=in+1;
            for jk=1:3
                Nt(jk,ik+jk-1)=shape(in);
            end
        end
        
        darea=det(jacm)*wgp(igp)*wgp(jgp);
        area=area+darea;
        Ktbe=Ktbe+Btb'*Dtb*Btb*darea;
        Ktrbe=Ktrbe+Btb'*Dtrb*Brb*darea;
        Krrbe=Krrbe+Brb'*Drrb*Brb*darea;
        %Elemenal mass matrices (Lumped mass matrix)
        Me=Me+Nt'*Nt*darea*rho*ah;
        
    end
end


%Elemental matrices generation, SHEAR
gp=[-1/sqrt(3) 1/sqrt(3)]; wgp=[1 1]; % reduced integration rule
% gp=[-0.7745966692 0 0.7745966692];
% wgp=[0.5555555555 0.888888888 0.5555555555];

for igp=1:length(gp)
    for jgp=1:length(gp)
        r=gp(igp);s=gp(jgp);
        r1=1-r;r2=1+r;s1=1-s;s2=1+s;
        shape(1)=-r1*s1*(r2+s)/4;shape(2)=r1*r2*s1/2;
        shape(3)=-r2*s1*(r1+s)/4;shape(4)=r2*s1*s2/2;
        shape(5)=r2*s2*(s-r1)/4;shape(6)=r1*r2*s2/2;
        shape(7)=r1*s2*(s-r2)/4;shape(8)=r1*s1*s2/2;
        deriv(1,1)=s1*(s+2*r)/4;deriv(1,2)=-r*s1;
        deriv(1,3)=s1*(2*r-s)/4;deriv(1,4)=s1*s2/2;
        deriv(1,5)=s2*(s+2*r)/4;deriv(1,6)=-r*s2;
        deriv(1,7)=s2*(2*r-s)/4;deriv(1,8)=-s1*s2/2;
        deriv(2,1)=r1*(2*s+r)/4;deriv(2,2)=-r1*r2/2;
        deriv(2,3)=r2*(2*s-r)/4;deriv(2,4)=-s*r2;
        deriv(2,5)=r2*(r+2*s)/4;deriv (2,6)=r1*r2/2;
        deriv(2,7)=r1*(2*s-r)/4;deriv(2,8)=-r1*s;
        jacm=deriv*ecod';
        jaci=inv(jacm);cartd=jaci*deriv;
        
        Bts=zeros(2,24); kg=0;
        for k=1:3:22
            kg=kg+1;
            Bts(1,k+2)=cartd(1,kg);
            Bts(2,k+2)=cartd(2,kg);
        end
        
        Brs=zeros(2,16); kg=0;
        for k=1:2:15
            kg=kg+1;
            Brs(1,k)=shape(kg);Brs(2,k+1)=shape(kg);
        end
        
        darea=det(jacm)*wgp(igp)*wgp(jgp);
        area=area+darea;
        Ktse=Ktse+Bts'*Dts*Bts*darea;
        Ktrse=Ktrse+Bts'*Dtrs*Brs*darea;
        Krrse=Krrse+Brs'*Drrs*Brs*darea;
    end
end

%Elemenal stiffness matrices
Ktte=Ktbe+Ktse;
Ktre=Ktrbe+Ktrse;
Krre=Krrbe+Krrse;

% Global matrices
for ll=1:nel
    for mm=1:24
        for nn=1:24
            Ktt(ICt(ll,mm),ICt(ll,nn))=Ktt(ICt(ll,mm),ICt(ll,nn))+Ktte(mm,nn);
            M(ICt(ll,mm),ICt(ll,nn))=M(ICt(ll,mm),ICt(ll,nn))+Me(mm,nn);
        end
    end
end
for ll=1:nel
    for mm=1:24
        for nn=1:16
            Ktr(ICt(ll,mm),ICr(ll,nn))=Ktr(ICt(ll,mm),ICr(ll,nn))+Ktre(mm,nn);
        end
    end
end
for ll=1:nel
    for mm=1:16
        for nn=1:16
            Krr(ICr(ll,mm),ICr(ll,nn))=Krr(ICr(ll,mm),ICr(ll,nn))+Krre(mm,nn);
        end
    end
end

Krri=inv(Krr); A2=Ktr*Krri;
Kglobal=Ktt-A2*Ktr';

%% Applying Boundary Conditions
%  Finding edges for applying BCs
LeftE=1:nw;Leftdof=[1 2 3 19:24];
BotE=1:nw:nw*nl;Botdof=1:9;
TopE=nw:nw:nw*nl; Topdof=13:21;
RightE=(nl-1)*nw+1:nl*nw; Rightdof=7:15;

delta=speye(ndf,ndf);

switch BCs
    case 'Fixed'
        BC_Dof=[ICt(LeftE,Leftdof);ICt(BotE,Botdof);ICt(TopE,Topdof);ICt(RightE,Rightdof)];
        a=0; b=1; c=0; RBM=0;
    case 'Free'
        BC_Dof=[];
        a=1; b=0; c=0; RBM=6;
end

BC_Dof=BC_Dof(:);
BC_Dof=unique(BC_Dof);


Kglobal(BC_Dof,:)=[];
M(BC_Dof,:)=[];
Kglobal(:,BC_Dof)=[];
M(:,BC_Dof)=[];
xr=x; xr(BC_Dof(3:3:end)/3)=[];
yr=y; yr(BC_Dof(3:3:end)/3)=[];

%% Sa'ed's workflow (create a matlab structure to hold important variables)
clear FEplate
FEplate.material=material;
FEplate.BC=BCs;
FEplate.length=Length;
FEplate.width=Width;
FEplate.thickness=Thk;
FEplate.widthElements=nw;
FEplate.lengthElements=nl;
FEplate.xCoordinates=x;
FEplate.yCoordinates=y;
FEplate.xrCoordinates=xr;
FEplate.yrCoordinates=yr;
FEplate.M=M;
FEplate.K=Kglobal;
FEplate.info={'A single node is a point on the plate that has 3';
'degrees of freedom; x,y, and z displacement. Each element in the FE model';
'is a rectangle/square with controllable dimentions and 8 nodes. The';
'resulting M, and K matrices describe an undamped (not true in reality)';
'plate, therefore a damping matrix (C) must be added to the system to';
'describe damping effects. For a free Aliminium Plate, C can be chosen to be';
'C = M + 7.2*10^(-8)*K  .Then the system dynamics can be described by:';
'M * (d^2/dt^2)(q) + C * (d/dt)(q) + K * (q)=0;';
'where q is vector containing the plates nodal degrees of freedom (dof);';
'q=[x1,y1,z1,.....,xn,yn,zn]'' where the subscript denotes the respective';
'node on the plate.';
'The node count starts with the bottom left node then increases by moving'; 
'vertically, then the count moves to the second node from the bottom left'; 
 'and adds by moving vertically again, and so on. Example (a single element)';
''
'3 5 8';
'2   7'; 
'1 4 6';
''
'The variables {x,y} denote spatial co-ordinates of nodes.';
'If fixed boundary conditions are used then M and K can be further reduced';
'since the response is zero for all boundary nodes, therefore the variables';
'{xr,yr} denote the reduced nodal co-ordinates vectors excluding nodes on';
 'the boundary.'};

save(fileName,'FEplate');

%% Mico's workflow (save important variables into a .mat file)

% mass=M;
% stiff=Kglobal;
% save(fileName,'mass', 'stiff', 'xr', 'yr', 'x', 'y', 'nw', 'nl','Width','Length')
