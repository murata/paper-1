function [h_avg, h] = entropy(f, space)
    h = -f .* log2(f);
    h(isnan(h)) = 0;
    h_avg = trapz(space.x, trapz(space.y, h));
end
