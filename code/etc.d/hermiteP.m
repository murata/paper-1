function f = hermiteP(n, x)
    f = 2.^(-n./2).*hermiteH(n, x/sqrt(2));
end