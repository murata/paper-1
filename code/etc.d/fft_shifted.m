function [y, freqs, y_shifted, freqs_shifted] = fft_shifted(x, fs)
    y = fft(x);
    L = length(x);
    y_shifted = fftshift(y);
    y = y(1:L/2+1);
    freqs = fs*(0:(L/2))/L;
    freqs_shifted = (-L/2:L/2-1)/L*fs;
end
