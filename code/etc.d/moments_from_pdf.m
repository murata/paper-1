function [mean, var] = moments_from_pdf(x, fx)
    mean = trapz(x, x.*fx);
    var = trapz(x, x.^2.*fx)-mean^2;
end