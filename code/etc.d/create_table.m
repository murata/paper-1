clc; clearvars; close all; prepare_generic;

t = table;
nsensors = 11;
nsteps = 162;
ncases = 0;

for i=1:nsensors
    ncases = ncases + nchoosek(nsensors, i);
end

for step_idx = 1:nsteps
    start_time = time();
    parfor case_idx=1:ncases
        sensors = find(str2num(reshape(dec2bin(case_idx, nsensors), [], 1)));
        nsensors_this = length(sensors);
        if nsensors_this > 1
            row = table(case_idx, step_idx, nsensors_this, mat2cell(sensors, nsensors_this), cell(1, 1), cell(1, 1), cell(1, 1), nan, nan, nan, nan, nan, nan, nan, nan, nan,...
                        'VariableNames', {'case_idx', 'step_idx', 'nsensors', 'sensor_idx', 'kappa', 'kappa_joint', 'consensus_set', 'entropy', 'estimate_x', 'estimate_y', 'groundtruth_x', 'groundtruth_y', 'error_x', 'error_y', 'normed_error', 'duration'});
            t = [t; row];
        end
    end
    fprintf("Step ID: %3d Duration: %1.2f [secs]\n", step_idx, seconds(time()-start_time));
end

save("../data/results-bookkeeping.mat", 't');
