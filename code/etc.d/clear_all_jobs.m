function clear_all_jobs()
    c = parcluster;
    jobs = findJob(c);

    for i=1:length(jobs)
        cancel(jobs(i));
        delete(jobs(i));
    end
end
