fname_out = '../figures/k_study/anim.gif';
filepath = dir('../figures/k_study/result_*.png');
filepath = natsortfiles(filepath);
for k = 1:numel(filepath)
    fname = fullfile(filepath(k).folder, filepath(k).name);
    img = imread(fname);
    [A,map] = rgb2ind(img,256);

    if k ==1
        imwrite(A, map, fname_out, 'gif', 'LoopCount', Inf, 'DelayTime', 1);
    else
        imwrite(A, map, fname_out, 'gif', 'WriteMode', 'append', 'DelayTime', 1);
    end
end
