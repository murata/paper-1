clc; clearvars; close all; prepare_generic;
syms e_i theta x y x_i y_i a b real;
syms et sigma_i n k g_inv(d) real;

assumeAlso(b<0);
assumeAlso(a>0);
assumeAlso(et>=0);
assumeAlso(e_i>0);
assumeAlso(x~=x_i);
assumeAlso(y~=y_i);
assumeAlso(sigma_i>0);
assumeAlso(n>0);
assumeAlso(k>0);
assumeAlso(k<1);
d = sqrt((x-x_i)^2 + (y-y_i)^2);
g(e_i) = a * e_i ^ b; 
g_inv(x) = (x/a)^(1/b);
mu(et) = et + n * sigma_i^2;
sigma(et) = sqrt(4 * et * sigma_i^2 + 2 * n * sigma_i^4);
h_inv(x, y) = [g_inv(d); atan2(y-y_i, x-x_i)]
J = jacobian(h_inv);

detJ = simplify_auto(det(J), true, 1000);
fE(e_i, a, b) = norm_pdf(e_i, mu, sigma, sym('pi'));
muk = mu(e_i*k)
mur = sigma(e_i*k)
f(x, y) = fE(g_inv(d), muk, mur) * -detJ / (2*pi);
f(x, y) = subs(f, et, e_i*k)
rho = -log(f);
% psi_x = simplify_auto(diff(rho, k), true, 1000)
grad = simplify_auto(diff(rho, k), true, 1000)
save('../data/grad.mat', 'grad')
% k_ = simplify_auto(solve(psi_x, k), true, 1000);
% abs_k = simplify_auto(abs(k_), true, 1000)
% pretty(abs_k)


