clc; clearvars; close all;

names = ["Proposed-1", "Proposed-2"];
steps = 1:162;
% for name=names
%     parfor step=steps
%         start_time = time();
%         foldername = sprintf("../figures/%s/step-%03d/", name, step);
%         imagenames = dir(fullfile(foldername,'*.png'));
%         imagenames = {imagenames.name}';
%         video = VideoWriter(fullfile("../figures/", sprintf('%s-step-%03d-all-cases.avi', name, step)));
%         video.FrameRate = 10;
%         open(video)
%         for ii = 1:length(imagenames)
%             img = imread(fullfile(foldername,imagenames{ii}));
%             writeVideo(video,img);
%         end
%         close(video);
%         fprintf("Finished: %03d of %s in %4d [sec] with %4d frames\n", step, name, seconds(time() - start_time), length(imagenames));
%     end
% end 

for name=names(1)
    parfor case_idx=1:2047
        warning('off','all')

        fname = sprintf("../figures/Occupant-1/Proposed-1/step-001/case-%04d.png", case_idx);
        if ~exist(fname, 'file')
            % fprintf("%s \n", fname)
            continue
        end
        video = VideoWriter(fullfile("../reports/Occupant-1/", sprintf('%s-case-%04d-all-steps.avi', name, case_idx)));
        video.FrameRate = 1;
        open(video);
        for step=steps
            fname = sprintf("../figures/Occupant-2/%s/step-%03d/case-%04d.png", name, step, case_idx);
            img = imread(fname);
            writeVideo(video, img);
        end
        close(video);
    end
end 
