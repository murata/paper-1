f = 3;
fs = 5000;
tfinal = 1.2;
ts = -0.25;
m = 0.01;                     % Mass (kg)
k = (2*f*pi)^2 * m;           % Spring constant (N/m)
c = 0.3 * 2 * sqrt(k * m);    % Damping coefficient (N.s/m)

% Define the transfer function
s = tf('s');
H = 1/(m*s^2 + c*s + k);
t = 0:1/fs:tfinal;
tbefore = ts:1/fs:0;
[y, t] = impulse(H, t(end));
t = [tbefore,  t'];
y = [zeros(size(tbefore)), y'];
y = y + randn(size(t)) * 0.05;
t_baseline_start = -0.05;
t_baseline_final = 0.08;
t_proposed_start = -0.09;
t_proposed_final = 0.65;
[~, t_baseline_start_idx] = min(abs(t - t_baseline_start)); 
[~, t_baseline_final_idx] = min(abs(t - t_baseline_final)); 
[~, t_proposed_start_idx] = min(abs(t - t_proposed_start)); 
[~, t_proposed_final_idx] = min(abs(t - t_proposed_final)); 
e_baseline = energy(y(t_baseline_start_idx:t_baseline_final_idx), 2);
e_proposed = energy(y(t_proposed_start_idx:t_proposed_final_idx), 2);

% Plot the impulse response
h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 13.5 4]); hold on;
plot(t, y, 'k');
xline(t_baseline_start, 'r-.', 'LineWidth', 1.5);
xline(t_baseline_final, 'r-.', 'LineWidth', 1.5);
xline(t_proposed_start, 'g-.', 'LineWidth', 1.5);
xline(t_proposed_final, 'g-.', 'LineWidth', 1.5);
grid on; grid minor; axis tight;

legend("Signal $\mathbf{z}$", "Baseline", "", "Proposed")
xlim([ts, tfinal]);
% ylim([-4, 4]);
xlabel('Time Sample [$k$]'); ylabel('Measurement [V]'); 
exportgraphics(h, "../figures/detection-results.eps", "Resolution", 600);
close(h);

