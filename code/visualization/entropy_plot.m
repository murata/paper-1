% analyze;
entropy_features_oc1 = zeros(10, 3);
entropy_features_oc2 = zeros(10, 3);
for nsensors=2:11
    idx = oc1.t.nsensors == nsensors;
    entropies = oc1.t(idx, :).entropy;
    entropies(entropies==0) = [];
    entropy_features_oc1(nsensors-1, :) = quantile(entropies, [0.75, 0.5, 0.25]);
    entropies = oc2.t(idx, :).entropy;
    entropies(entropies==0) = [];
    entropy_features_oc2(nsensors-1, :) = quantile(entropies, [0.75, 0.5, 0.25]);
end

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 13.5 5]); hold on;
plot(NaN, NaN, 'linestyle', "none");
plot(2:11, entropy_features_oc1(:, 1), 'r:', "Linewidth", 1.5);
plot(2:11, entropy_features_oc1(:, 2), 'r-', "Linewidth", 1.5);
plot(2:11, entropy_features_oc1(:, 3) , 'r-.', "Linewidth", 1.5);
plot(NaN, NaN, 'linestyle', "none");
plot(2:11, entropy_features_oc2(:, 1), 'k:', "Linewidth", 1.5);
plot(2:11, entropy_features_oc2(:, 2), 'k-', "Linewidth", 1.5);
plot(2:11, entropy_features_oc2(:, 3) , 'k-.', "Linewidth", 1.5);

grid on; grid minor; axis tight;
% xticks([2:12]); xlim([2, 11]);
% yticks([0:5]); ylim([0, 5]);
xlabel('Number of Sensors $m$'); ylabel('Entropy [Bits]');
legend("\textbf{Occupant-1}", "$Q(0.75)$", "$Q(0.50)$", "$Q(0.25)$", "\textbf{Occupant-2}", "$Q(0.75)$", "$Q(0.50)$", "$Q(0.25)$", "NumColumns", 1, "location", "northeastoutside", "box", "off");
exportgraphics(h, "../figures/sensor-entropies.eps", "Resolution", 600);
exportgraphics(h, "../figures/sensor-entropies.png", "Resolution", 600);

close(h);
