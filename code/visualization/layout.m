clc; clearvars -except oc1 oc2 energy_oc1 energy_oc2 experiment; close all; prepare_generic; warning off;
subplot = @(m,n,p) subtightplot(m, n, p, [0.10 0.05], [0.1 0.1], [0.1 0.1]);

if ~exist('oc1', 'var')
    oc1 = load("../data/occupant1.mat");
end
if ~exist('oc2', 'var')
    oc2 = load("../data/occupant2.mat");
end
if ~exist('energy_oc1', 'var')
    energy_oc1 = load("../data/automatic_detection_energy_subject_1.mat");

end
if ~exist('ocenergy_oc2', 'var')
    energy_oc2 = load("../data/automatic_detection_energy_subject_2.mat");
end
if ~exist('experiment1', 'var')
    load("../data/goodwin_step_data.mat");
    experiment = experiment1;
end
%%
h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'OuterPosition', [0 0 13.5 8]); hold on;

scatter(experiment.sensor_locations(1, :), experiment.sensor_locations(2, :), 's', 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k');
plot(experiment.groundtruth(1, 1:27), experiment.groundtruth(2, 1:27), 'o', 'MarkerSize', 3, 'MarkerFaceColor', 'green', 'MarkerFaceColor', 'green', 'Color', 'k');
plot(experiment.groundtruth(1, 28:54), experiment.groundtruth(2, 28:54), 'o', 'MarkerSize', 3, 'MarkerFaceColor', 'green', 'MarkerFaceColor', 'green', 'Color', 'k')
plot(experiment.groundtruth(1, 55:81), experiment.groundtruth(2, 55:81), 'o', 'MarkerSize', 3, 'MarkerFaceColor', 'green', 'MarkerFaceColor', 'green', 'Color', 'k')
legend("Sensor Locations", "Step Locations", "location", "southoutside", "box", "off", "orientation", "horizontal")
axis equal; grid on; grid minor;
xlim([13, 31]); ylim([30, 33]);
xlabel('$x \rightarrow$ [m]');
ylabel('$y \rightarrow$ [m]');

exportgraphics(h, "../figures/layout.eps", "Resolution", 600);
exportgraphics(h, "../figures/layout.png", "Resolution", 600);

close(h);
