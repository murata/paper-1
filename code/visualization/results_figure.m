clc; clearvars -except oc1 oc2 energy_oc1 energy_oc2 experiment; close all; prepare_generic; warning off;
subplot = @(m,n,p) subtightplot(m, n, p, [0.10 0.05], [0.1 0.1], [0.1 0.1]);

if ~exist('oc1', 'var')
    oc1 = load("../data/occupant1.mat");
end
if ~exist('oc2', 'var')
    oc2 = load("../data/occupant2.mat");
end
if ~exist('energy_oc1', 'var')
    energy_oc1 = load("../data/automatic_detection_energy_subject_1.mat");

end
if ~exist('ocenergy_oc2', 'var')
    energy_oc2 = load("../data/automatic_detection_energy_subject_2.mat");
end
if ~exist('experiment1', 'var')
    load("../data/goodwin_step_data.mat");
    experiment = experiment1;
end
%%
[case_idx, errors_1, errors_2] = identify_case(oc1, oc2); 

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'OuterPosition', [0 0 13.5 10]); hold on;
for ii=1:3
    ax = subplot(3, 1, ii); hold on;
    make_figure(ax, case_idx(ii), experiment, oc1);
    if ii == 1
        legend("Sensors", "Consensus Set", "Estimation $\mathbf{x}^*$", "Ground Truth $\mathbf{x}_{true}$", "location", "northeast");
        title("\textbf{Occupant-1}");
        subtitle("(\textbf{a}) $m$=2 $\diamond$ Localization Error " + round(errors_1(ii), 3) + " [m]");
    elseif ii == 2
        ylabel('$y \rightarrow$ [m]');
        subtitle("(\textbf{c}) $m$=6 $\diamond$ Localization Error " + round(errors_1(ii), 3) + " [m]");
    elseif ii == 3
        subtitle("(\textbf{e}) $m$=11 $\diamond$ Localization Error " + round(errors_1(ii), 3) + " [m]");
        xlabel('$x \rightarrow$ [m]');
    end
    axis equal; grid on; grid minor;
    xlim([13, 31]); ylim([29, 34]);
end
exportgraphics(h, "../figures/results-1.eps", "Resolution", 600);
close(h);

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'OuterPosition', [0 0 13.5 10]); hold on;
for ii=1:3
    ax = subplot(3, 1, ii); hold on;
    make_figure(ax, case_idx(ii), experiment, oc2);
    if ii == 1
        legend("Sensors", "Consensus Set", "Estimation $\mathbf{x}^*$", "Ground Truth $\mathbf{x}_{true}$", "location", "northeast");
        title("\textbf{Occupant-2}");
        subtitle("(\textbf{b}) $m$=2 $\diamond$ Localization Error " + round(errors_2(ii), 3) + " [m]");
    elseif ii == 2
        ylabel('$y \rightarrow$ [m]');
        subtitle("(\textbf{d}) $m$=6 $\diamond$ Localization Error " + round(errors_2(ii), 3) + " [m]");
    elseif ii == 3
        subtitle("(\textbf{f}) $m$=11 $\diamond$ Localization Error " + round(errors_2(ii), 3) + " [m]");
        xlabel('$x \rightarrow$ [m]');
    end
    axis equal; grid on; grid minor;
    xlim([13, 31]); ylim([29, 34]);
end

exportgraphics(h, "../figures/results-2.eps", "Resolution", 600);
close(h);

function make_figure(ax, case_idx, experiment, oc1)
    consensus_set = oc1.t(case_idx, :).consensus_set{1};
    sensor_idx = oc1.t(case_idx, :).sensor_idx{1};

    scatter(ax, experiment.sensor_locations(1, sensor_idx), experiment.sensor_locations(2, sensor_idx), 's', 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k');
    scatter(ax, experiment.sensor_locations(1, sensor_idx(consensus_set)), experiment.sensor_locations(2, sensor_idx(consensus_set)), 50, 'o', 'MarkerEdgeColor', 'k');
    scatter(ax, oc1.t(case_idx, :).estimate_x, oc1.t(case_idx, :).estimate_y, 'rx', "Linewidth", 1);
    scatter(ax, oc1.t(case_idx, :).groundtruth_x, oc1.t(case_idx, :).groundtruth_y, 'g+', "Linewidth", 1);
end

function [case_idx, error_1, error_2] = identify_case(oc1, oc2)
    idx1 = oc1.t.nsensors == 11 & oc1.t.groundtruth_x < 20;
    idx2 = oc1.t.normed_error <= 1;
    idx3 = oc2.t.normed_error <= 1;
    idx = find(idx1 & idx2 & idx3);

    step_idx = oc1.t(idx, :).step_idx;
    all_cases2 = find(oc1.t.step_idx == step_idx & oc1.t.nsensors == 2);
    all_cases6 = find(oc1.t.step_idx == step_idx & oc1.t.nsensors == 6);
    all_cases11 = find(oc1.t.step_idx == step_idx & oc1.t.nsensors == 11);
    stop = false;
    best_cost = inf;
    for case2 = all_cases2'
        for case6 = all_cases6'
            if oc1.t(case2, :).normed_error > oc1.t(case6, :).normed_error & oc1.t(case6, :).normed_error > oc1.t(all_cases11, :).normed_error
                if oc2.t(case2, :).normed_error > oc2.t(case6, :).normed_error & oc2.t(case6, :).normed_error > oc2.t(all_cases11, :).normed_error
                    feature_1 = [oc1.t(case2, :).normed_error, oc1.t(case6, :).normed_error, oc1.t(all_cases11, :).normed_error];
                    feature_2 = [oc2.t(case2, :).normed_error, oc2.t(case6, :).normed_error, oc2.t(all_cases11, :).normed_error];

                    cost = sum(feature_1);
                    if cost < best_cost
                        error_1 = feature_1;
                        error_2 = feature_2;
                        best_cost = cost;
                        case_idx = [case2, case6, all_cases11];
                    end
                    if cost < 3.4;
                        stop = true;
                    end
                end
            end
            if stop
                break
            end
        end
        if stop
            break
        end
    end
end
