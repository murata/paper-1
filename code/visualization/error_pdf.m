% analyze;
ns = 50;
sky_blue = [0.35, 0.7, 0.9];
orangey_brown = [0.8, 0.4, 0];
error_max = 8;
qq = linspace(0, error_max, ns);
errors_oc1 = oc1.t.normed_error;
errors_oc1(isnan(errors_oc1)) = [];
[~, error_pdf_oc1] = epdf(errors_oc1, qq);
error_pdf_oc1_theirs = reshape(error(:, :, 1), [], 1);
[~, epdf_baseline_oc1] = epdf(error_pdf_oc1_theirs, qq);

errors_oc2 = oc2.t.normed_error;
errors_oc2(isnan(errors_oc2)) = [];
[~, error_pdf_oc2] = epdf(errors_oc2, qq);
error_pdf_oc2_theirs = reshape(error(:, :, 2), [], 1);
error_pdf_oc2_theirs(isnan(error_pdf_oc2_theirs)) = [];
[~, epdf_baseline_oc2] = epdf(error_pdf_oc2_theirs, qq);

cdf_oc1_baseline = cumtrapz(qq, epdf_baseline_oc1)/trapz(qq, epdf_baseline_oc1);
cdf_oc1 = cumtrapz(qq, error_pdf_oc1)/trapz(qq, error_pdf_oc1);
[~, space_idx_oc1] = min(abs(cdf_oc1-0.8));
[~, space_idx_oc1_baseline] = min(abs(cdf_oc1_baseline-0.8));

cdf_oc2_baseline = cumtrapz(qq, epdf_baseline_oc2)/trapz(qq, epdf_baseline_oc2);
cdf_oc2 = cumtrapz(qq, error_pdf_oc2)/trapz(qq, error_pdf_oc2);
[~, space_idx_oc2] = min(abs(cdf_oc2-0.8));
[~, space_idx_oc2_baseline] = min(abs(cdf_oc2_baseline-0.8));

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 13.5 4]);
t = tiledlayout(1, 2, 'Padding', 'none', 'TileSpacing', 'compact');
ax(1) = nexttile; hold on;
title("\textbf{Occupant-1}");
plot(qq, error_pdf_oc1, "Linewidth", 1.5, "Color", sky_blue);
plot(qq, epdf_baseline_oc1, "Linewidth", 1.5, "Color", orangey_brown);
plot(qq, cdf_oc1, "-.", "Linewidth", 1.5,  "Color", sky_blue);
plot(qq, cdf_oc1_baseline, "-.", "Linewidth", 1.5,  "Color", orangey_brown);
grid on; grid minor; axis tight;
xticks([0:error_max]); yticks([0:0.2:1]);
xlabel('Error [m]'); ylabel('Probability');


ax(2) = nexttile; hold on;
title("\textbf{Occupant-2}");

plot(NaN, NaN, 'linestyle', "none");
plot(qq, error_pdf_oc2, "Linewidth", 1.5, "Color", sky_blue);
plot(qq, epdf_baseline_oc2, "Linewidth", 1.5, "Color", orangey_brown)
plot(NaN, NaN, 'linestyle', "none");
plot(qq, cdf_oc2, "-.", "Linewidth", 1.5, "Color", sky_blue);
plot(qq, cdf_oc2_baseline, "-.", "Linewidth", 1.5, "Color", orangey_brown);
xlabel('Error [m]'); ylabel('Probability');
leg = legend("PDFs", "Proposed", "Baseline", "CDFs", "Proposed", "Baseline", "location", "layout", "numcolumns", 1, "orientation", "vertical", "box", "off");
leg.Layout.Tile = 'east';
grid on; grid minor; axis tight;
xticks([0:error_max]); yticks([0:0.2:1]);
linkaxes(ax, 'xy');
exportgraphics(h, "../figures/error-pdf.eps", "Resolution", 600);
exportgraphics(h, "../figures/error-pdf.png", "Resolution", 600);
