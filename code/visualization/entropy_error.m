% analyze;
ns = 20;
colors = [
    0, 0, 0;          % Black
    0.4, 0.4, 0.4;    % Dark Gray
    0.7, 0.7, 0.7;    % Light Gray
    0.9, 0.9, 0;      % Yellow
    0, 0.6, 0;        % Dark Green
    0.35, 0.7, 0.9;   % Sky Blue
    0, 0.45, 0.7;     % Steel Blue
    0.8, 0.4, 0;      % Orangey-Brown
    0.9, 0.6, 0.6;    % Salmon Pink
    0.6, 0.3, 0.6     % Purple-ish
];
entropy_features_oc1 = zeros(10, ns);
entropy_features_oc2 = zeros(10, ns);
error_features_oc1 = zeros(10, ns);
error_features_oc2 = zeros(10, ns);

qq = linspace(0.1, 0.9, ns);
for nsensors=2:11
    idx = oc1.t.nsensors == nsensors;
    entropies = oc1.t(idx, :).entropy;
    errors = oc1.t(idx, :).normed_error;
    errors(entropies == 0) = [];
    entropies(entropies == 0) = [];
    entropy_features_oc1(nsensors-1, :) = quantile(entropies, qq);
    error_features_oc1(nsensors-1, :) = quantile(errors, qq); 
    entropies = oc2.t(idx, :).entropy;
    errors = oc2.t(idx, :).normed_error;
    errors(entropies == 0) = [];
    entropies(entropies == 0) = [];
    
    entropy_features_oc2(nsensors-1, :) = quantile(entropies, qq);
    error_features_oc2(nsensors-1, :) = quantile(errors, qq); 
end

h = figure('visible', 'off', 'renderer', 'painters', 'Units', 'centimeters', 'Position', [0 0 13.5 5]);
t = tiledlayout(1, 2, 'Padding', 'none', 'TileSpacing', 'compact');

ax(1) = nexttile; hold on;
title("\textbf{Occupant-1}");
for ii=2:11
    plot(entropy_features_oc1(ii-1, :), error_features_oc1(ii-1, :), "DisplayName", "$m$ = "+ num2str(ii), "Linewidth", 1.5, "Color", colors(ii-1, :));
end
grid on; grid minor; axis tight;
xlabel('Entropy = $Q(x)$'); ylabel('Error = $Q(y)$');

%% Original code
ax(2) = nexttile; hold on;
title("\textbf{Occupant-2}");
plot(NaN, NaN, 'linestyle', "none",  "DisplayName", "Number of Sensors");
for ii=2:11
    plot(entropy_features_oc2(ii-1, :), error_features_oc2(ii-1, :), "DisplayName", "$m$ = "+ num2str(ii), "Linewidth", 1.5, "Color", colors(ii-1, :));
end
xlabel('Entropy = $Q(x)$'); ylabel('Error = $Q(y)$');
leg = legend("location", "layout", "NumColumns", 1, "box", "off");
%% Original code end


leg.Layout.Tile = 'east';
linkaxes(ax, 'xy');
grid on; grid minor; axis tight;
exportgraphics(h, "../figures/entropy-vs-error.eps", "Resolution", 600);
exportgraphics(h, "../figures/entropy-vs-error.png", "Resolution", 600);

function z = nan_zscore(x)
    % Compute the mean and standard deviation ignoring NaN values
    m = nanmean(x);
    s = nanstd(x);
    
    % Compute the z-score
    z = (x - m) ./ s;
end
