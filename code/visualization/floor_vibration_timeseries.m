function [h] = floor_vibration_timeseries(sim, k)
    tri = delaunay(sim.plate.plate.xr(:), sim.plate.plate.yr(:));
    h = figure('visible', 'off');
    displacement = squeeze(sim.signals.raw.displacement(:, :,  k));
    tr = triangulation(tri, sim.plate.plate.xr(:), sim.plate.plate.yr(:), displacement');
    trisurf(tr); xlim([0, 1]); ylim([0 1]); shading interp; lighting phong;
    % caxis([min(sim.signals.raw.displacement(:)), max(sim.signals.raw.displacement(:))]);
    fmt_string = sprintf('t=%2.2d [sec]', sim.t(k));
    title(fmt_string); 
end

