function fe = pdf_fe(e, et, n, sigma)
    mean_e = et + n * sigma.^2;
    vari_e = 4 * et * sigma.^2 + 2 * n * sigma.^4;
    % vpa([mean_e - 2 * sqrt(vari_e), et, mean_e + 2 * sqrt(vari_e)])
    fe = normpdf(e, mean_e, sqrt(vari_e));
end
