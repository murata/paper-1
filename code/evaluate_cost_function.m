clc; close all; clearvars; prepare_generic;
load("../data/goodwin_step_data.mat");
load("../data/automatic_detection_energy_subject_1.mat");

experiment = experiment2;

nx = 900; ny = 300;
space = generate_space(13, 31, 27, 33, nx, ny);
d_true = pdist2(experiment.sensor_locations', experiment.groundtruth');
xy = [space.xx(:), space.yy(:)];
options = optimset('Display', 'off');
fd_model = "uniform";

sensors = [1,2,6,11];
nsensors_used = length(sensors);
nsteps = 27;
sensor_pairs = nchoosek(sensors, 2);
ncases = size(sensor_pairs, 1);
nkappa = 51;
kappa_1 = linspace(0, 5, nkappa);
kappa_2 = linspace(0, 5, nkappa);
[params_1, params_2] = meshgrid(kappa_1, kappa_2);
kappa_1_vec = params_1(:);
kappa_2_vec = params_2(:);
nsims = numel(kappa_1_vec);
fit_curves;
duration = zeros(nsteps, 1);
code_start = time();
cost = zeros(nsteps, ncases, nsims);
for step_idx=1:nsteps
    step_start = time();
    for case_idx=1:ncases
        parfor kappa_idx=1:nsims
            warning('off','all');
            fxy = zeros(ny, nx, 2);
            for qq = 1:2
                sensor_idx = sensor_pairs(case_idx, qq);
                fun = @(x)problem(x, step_idx, sensor_idx, experiment, e, models{sensor_idx}, nx, ny, nsamples, xy, space, fd_model);
                if qq==1
                    [~, fx] = fun(kappa_1_vec(kappa_idx));
                else
                    [~, fx] = fun(kappa_2_vec(kappa_idx));
                end
                fxy(:, :, qq) = fx;
            end
            joint = prod(fxy, 3);
            division = trapz(space.x, trapz(space.y, joint));
            joint_pdf = joint / (division+eps);
            cost(step_idx, case_idx, kappa_idx) = min(-joint_pdf(:));
        end
    end

    duration(step_idx) = seconds(time()-step_start);
    mean_duration = sum(duration) / nnz(duration);
    ncompleted = nnz(duration) / numel(duration);
    fprintf("Step Idx: %3d Duration: %3.3f [secs] Completed: %2.2f ETA: %10s\n", step_idx, duration(step_idx), 100*ncompleted, code_start + seconds(nsteps * mean_duration));
end

for step_idx=1:nsteps
    parfor case_idx=1:ncases
        fname = sprintf("../figures/cost-wider/step-%03d-%d-%d.png", step_idx, sensor_pairs(case_idx, 1), sensor_pairs(case_idx, 2));
        h = figure('visible', 'off', 'renderer', 'painter','Position', [0, 0, 300, 300]);
        contourf(kappa_1, kappa_2, reshape(cost(step_idx, case_idx, :), [nkappa, nkappa]), 50, 'edgecolor', 'none'); 
        axis equal; axis tight; grid on; grid minor; colormap jet; colorbar;
        caxis([min(cost(step_idx, case_idx, :)); max(cost(step_idx, case_idx, :))]);
        title("Step ID: " + step_idx);
        xlabel("\kappa_{" + sensor_pairs(case_idx, 1) + "}");
        ylabel("\kappa_{" + sensor_pairs(case_idx, 2) + "}");
        drawnow;
        saveas(h, fname);
        close(h);
    end
end

parfor case_idx=1:ncases
    sensors = sensor_pairs(case_idx, :);
    video = VideoWriter(fullfile("../figures/cost-videos-wider/", sprintf('sensors-%d-%d.avi', sensors(1), sensors(2))));
    video.FrameRate = 1;
    open(video);
    for step_idx=1:nsteps
        fname = sprintf("../figures/cost-wider/step-%03d-%d-%d.png", step_idx, sensors(1), sensors(2));
        img = imread(fname);
        writeVideo(video, img);
    end
    close(video);
end


function [f, varargout] = problem(kappa, step_idx, sensor_idx, experiment, e, model, nx, ny, nsamples, xy, space, fd_model)
    xi = experiment.sensor_locations(:, sensor_idx)';
    delta = xi - experiment.groundtruth(:, step_idx)';
    [~, qp] = min(abs(space.x - experiment.groundtruth(1, step_idx)));
    [~, qq] = min(abs(space.y - experiment.groundtruth(2, step_idx)));

    beta = [model.a, model.b, atan2(delta(2), delta(1))];
    prob = pdf_fx(xy, xi, e(step_idx, sensor_idx)*kappa, nsamples(step_idx), experiment.sigma_zeta, beta, fd_model);
    qwert = reshape(prob, [ny, nx]);
    division = trapz(space.x, trapz(space.y, qwert));
    fxy = qwert / (division + eps);
    f = min(-fxy(:));
    if isinf(f)
        f = 999999999999;
    end
    nout = max(nargout,1) - 1;
    for k = 1:nout
        varargout{k} = fxy;
    end
end

function mySave(filenm, fxy)
    save(filenm, 'fxy');
end
