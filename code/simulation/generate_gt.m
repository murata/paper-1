%% Murat Ambarkutuk; murata@vt.edu -- VTSIL @ CPE, VT
%
start_time = time();
logging("Starting the process");

telapsed = time() - start_time;
logging("Simulation work done in %2.2f [seconds]", seconds(telapsed));
clearvars -except sim;
