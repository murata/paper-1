function t_impact = generate_step_timing(t_start, nstep, stance_time, ...
                                         mean_swing, std_swing, ...
                                         mean_shift, std_shift)
swing_time = mean_swing + randn(nstep, 1) * std_swing;
phase_shift = mean_shift + randn(1, 1) * std_shift;
t_impact_a = zeros(ceil(nstep/2), 1);
t_impact_b = zeros(ceil(nstep/2), 1);

t_impact_a(1) = t_start;
t_impact_b(1) = t_start + phase_shift;

for i=2:ceil(nstep/2)
    t_impact_a(i) = t_impact_a(i-1) + stance_time + swing_time(2*i-3);
    t_impact_b(i) = t_impact_b(i-1) + stance_time + swing_time(2*i-2);
end
t_impact = [t_impact_a, t_impact_b]';      
t_impact = t_impact(:);
t_impact = t_impact(1:nstep);
end

