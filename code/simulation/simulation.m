%% Murat Ambarkutuk; murata@vt.edu -- VTSIL @ CPE, VT
%
logging("Starting the simulation");
start_time = time();
for i=1:sim.nlocations
    sim_start_time = time();
    if sim.simulation_completed(i) && sim.continue_simulation
        logging("Case %s was already simulated, skipping.", i);
        continue
    end
    logging("Preparing: Case %s", i);
    sys = sim.state_space_discrete{i};
    logging("Ready to simulate: Case %s", i);
    [acc, ~, states] = lsim(sys, sim.forcing, sim.t, sim.initial_state);
    acc_sensor = acc(:, sim.sensor_idx*3)';
    % vel_sensor = states(:, sim.sensor_idx*6)';
    % disp_sensor = states(:, sim.sensor_idx*3)';
    sim.signals.raw.acceleration(i, :, :) = acc_sensor;
%     sim.signals.raw.velocity(i, :, :) = vel_sensor;
    % sim.signals.raw.displacement(i, :, :) = disp_sensor;
    sim.simulation_completed(i) = true;
    sim.sim_duration(i) = time() - sim_start_time;
    percent_done = mean(sim.simulation_completed);
    total_time = sum(sim.sim_duration)/percent_done;
    eta_wall_time = start_time + total_time;
    logging("Simulation done in %2.2f [secs] %2.2f%% done. ETC: %s", ...
            seconds(sim.sim_duration(i)), ...
            percent_done*100, ...
            eta_wall_time);
end
% save_sim(sim);
telapsed = time() - start_time;
logging("Simulation work done in %2.2f [seconds]", seconds(telapsed));
clearvars -except sim;
%%
start_time = time();
logging("Generating signals...");
sim.simulation_completed = zeros(sim.nrepeat);
sim.sim_duration = seconds(zeros(sim.nrepeat));

sim.signals.raw.energy = energy(sim.signals.raw.acceleration, 3);
noises = randn(sim.nrepeat, sim.nlocations, sim.nsensors, sim.nsamples);

logging("Starting to generate signal set...");
for repeat_idx = 1:sim.nrepeat
    sim_start_time = time();
    for location_idx = 1:sim.nlocations
        for sensor_idx = 1:sim.nsensors
            acc = sim.signals.raw.acceleration(location_idx, sensor_idx, :);
            try
                variance = sim.variances(sensor_idx);
            catch
                variance = sim.variances;                
            end
            signal_noisy = squeeze(acc) + squeeze(noises(repeat_idx, location_idx, sensor_idx, :)) * sqrt(variance);
            % sim.signals.sensor.acceleration(repeat_idx, location_idx, sensor_idx, :) = signal_noisy;
            sim.signals.sensor.energy(repeat_idx, location_idx, sensor_idx, :) = energy(signal_noisy, 1);
        end
    end
    sim.simulation_completed(repeat_idx) = true;
    sim.sim_duration(repeat_idx) = time() - sim_start_time;
    percent_done = mean(sim.simulation_completed(:));
    total_time = sum(sim.sim_duration(:))/percent_done;
    eta_wall_time = start_time + total_time;
    logging("Signal generation is done in %2.2f [secs] %2.2f%% done. ETC: %s", ...
            seconds(sim.sim_duration(repeat_idx)), ...
            percent_done*100, ...
            eta_wall_time);
end
save_sim(sim);
telapsed = time() - start_time;
logging("Simulation work done in %2.2f [seconds]", seconds(telapsed));
clearvars -except sim;
