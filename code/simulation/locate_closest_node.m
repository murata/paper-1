function [closest_node_id, closest_node] = locate_closest_node(x, y, xnode, ynode)
    closest_node_id = dsearchn([xnode,ynode],[x, y]);
    closest_node = [xnode(closest_node_id), ynode(closest_node_id)];
end

