clc; close all; clearvars -except models; prepare_generic;
load("../data/goodwin_step_data.mat");
load("../data/automatic_detection_energy_subject_1.mat");
load("../data/grad.mat");
load("../data/results-bookkeeping.mat");
experiment = experiment2;

nx = 900; ny = 300;
space = generate_space(13, 31, 27, 33, nx, ny);
d_true = pdist2(experiment.sensor_locations', experiment.groundtruth');
xy = [space.xx(:), space.yy(:)];
fd_model = "uniform";
options = optimoptions('fmincon','Display','none', "MaxFunctionEvaluations", 1000, "MaxIterations", 200, "OptimalityTolerance", 1e-3, "StepTolerance", 1e-3, "SpecifyObjectiveGradient", true);
if ~exist('models', 'var')
    fit_curves;
end
A = [];
b = [];

code_start = time();
ncases = size(t, 1);
% ncases = 20;
rng(3131);
% n_completed = zeros(ncases, 1);
% durations = nan(ncases, 1);
% code_start = time();

parfor idx = 1:ncases
    warning("off", "all");
    start_time = time();
    step_idx = t(idx, :).step_idx;
    nsensors_used = t(idx, :).nsensors;
    kappas = zeros(nsensors_used, 1);
    sensors = t(idx, :).sensor_idx{1};
    nsensors = t(idx, :).nsensors;
    for sensor_idx = 1:nsensors
        sensor_idx_now = sensors(sensor_idx);
        fun = @(x)sneaky(x, step_idx, sensor_idx_now, experiment, e, models{sensor_idx_now}, nx, ny, nsamples, xy, space, fd_model);
        kappas(sensor_idx) = fminbnd(fun, 0.25, 1);
    end
    fun = @(x)problem_kappa(x, experiment, e, models, ny, nx, nsensors_used, sensors, space, step_idx, nsamples, xy, fd_model, grad);
    kappa_joint = fmincon(fun, kappas, A, b, A, b, kappas - 0.05, kappas + 0.05, @unitdisk, options);
    t(idx, :).kappa = mat2cell(kappas, nsensors_used, 1);
    t(idx, :).kappa_joint = mat2cell(kappa_joint, nsensors_used, 1);

    [f, g, fused, entropy, consensus_set, cost_history, fxy] = fun(kappa_joint);
    t(idx, :).consensus_set = mat2cell(consensus_set, 1, length(consensus_set));
    [C,I] = max(fused(:));
    if C == 0
        % estimate = [mean(space.x); mean(space.y)];
        estimate = [nan; nan];
    else
        [Ji, Ii] = ind2sub(size(fused), I);
        estimate =  [space.x(Ii); space.y(Ji)];
    end

    error = estimate - experiment.groundtruth(:, step_idx);
    duration = seconds(time()-start_time);
    t(idx, :).estimate_x = estimate(1);
    t(idx, :).estimate_y = estimate(2);
    t(idx, :).groundtruth_x = experiment.groundtruth(1, step_idx);
    t(idx, :).groundtruth_y = experiment.groundtruth(2, step_idx);
    t(idx, :).error_x = error(1);
    t(idx, :).error_y = error(2);
    t(idx, :).normed_error = vecnorm(error, 2);
    t(idx, :).duration = duration;
    t(idx, :).entropy = entropy;

    % n_completed(idx) = true;
    % durations(idx) = duration;
    % progress = sum(n_completed) * 100 / length(n_completed);
    % eta = nanmean(durations) / progress;
    if mod(idx, 100) == 0
        fprintf("Step Idx: %06d Error: %3.3f [meters] Duration: %3.2f [secs]\n", idx, vecnorm(error, 2), duration);
    end
    % figure(idx);
    % subplot(4,1,1);
    % contour(space.x, space.y, sum(fxy, 3), 50); hold on;
    % scatter(experiment.sensor_locations(1, sensors), experiment.sensor_locations(2, sensors), 'bo');
    % scatter(experiment.sensor_locations(1, sensors(consensus_set)), experiment.sensor_locations(2, sensors(consensus_set)), 'b+');
    % scatter(experiment.groundtruth(1, step_idx), experiment.groundtruth(2, step_idx), 'ro');
    % scatter(estimate(1), estimate(2), 'rx');
    % axis equal;
    % grid on; grid minor;

    % subplot(4,1,2);
    % contour(space.x, space.y, sum(fxy(:, :, consensus_set), 3), 50); hold on;
    % scatter(experiment.sensor_locations(1, sensors), experiment.sensor_locations(2, sensors), 'bo');
    % scatter(experiment.sensor_locations(1, sensors(consensus_set)), experiment.sensor_locations(2, sensors(consensus_set)), 'b+');
    % scatter(experiment.groundtruth(1, step_idx), experiment.groundtruth(2, step_idx), 'ro');
    % scatter(estimate(1), estimate(2), 'rx');
    % axis equal;
    % grid on; grid minor;

    % subplot(4,1,3);
    % contour(space.x, space.y, prod(fxy(:, :, consensus_set), 3), 50); hold on;
    % scatter(experiment.sensor_locations(1, sensors), experiment.sensor_locations(2, sensors), 'bo');
    % scatter(experiment.sensor_locations(1, sensors(consensus_set)), experiment.sensor_locations(2, sensors(consensus_set)), 'b+');
    % scatter(experiment.groundtruth(1, step_idx), experiment.groundtruth(2, step_idx), 'ro');
    % scatter(estimate(1), estimate(2), 'rx');
    % axis equal;
    % grid on; grid minor;

    % subplot(4,1,4);
    % plot(1:length(cost_history), cost_history, 'k-o');
    % xlabel("iteration"); ylabel("partitioning metric");
    % grid on; grid minor;
    % drawnow;
end

save('../data/occupant1.mat', '-v7.3');

function [f, varargout] = problem_kappa(kappa, experiment, e, models, ny, nx, nsensors_used, sensors, space, step_idx, nsamples, xy, fd_model, grad)
    fxy = zeros(ny, nx, nsensors_used);
    gradient = zeros(nsensors_used, 1);

    for idx = 1:nsensors_used
        sensor_idx = sensors(idx);
        fun = @(x)inner_func(x, step_idx, sensor_idx, experiment, e, models{sensor_idx}, nx, ny, nsamples, xy, space, fd_model);
        [~, fx] = fun(kappa(idx));
        fxy(:, :, idx) = fx;
        qq = subs(grad, {'x', 'y'}, {0, 0});
        qq = subs(qq, {'a', 'b'}, {models{sensor_idx}.a, models{sensor_idx}.b});
        qq = subs(qq, {'e_i', 'k'}, {e(step_idx, sensor_idx), kappa(idx)});
        qq = subs(qq, {'n', 'sigma_i'}, {nsamples(step_idx), experiment.sigma_zeta});
        qq = subs(qq, {'x_i', 'y_i'}, {experiment.sensor_locations(1, sensor_idx), experiment.sensor_locations(2, sensor_idx)});
        gradient(idx) = double(qq);
    end
    [consensus_set, cost_history, fused, entropy] = bse(fxy, space);

    [C,I] = max(fused(:)); %% TODO: check if the max is unique

    [Ji, Ii] = ind2sub(size(fused), I);
    estimate =  [space.x(Ii); space.y(Ji)];

    for idx = 1:nsensors_used
        qq = subs(grad, {'x', 'y'}, {estimate(1), estimate(2)});
        qq = subs(qq, {'a', 'b'}, {models{sensor_idx}.a, models{sensor_idx}.b});
        qq = subs(qq, {'e_i', 'k'}, {e(step_idx, sensor_idx), kappa(idx)});
        qq = subs(qq, {'n', 'sigma_i'}, {nsamples(step_idx), experiment.sigma_zeta});
        qq = subs(qq, {'x_i', 'y_i'}, {experiment.sensor_locations(1, sensor_idx), experiment.sensor_locations(2, sensor_idx)});
        gradient(idx) = double(qq);
    end

    varargout{1} = gradient; 
    varargout{2} = fused;
    varargout{3} = entropy;
    varargout{4} = consensus_set;
    varargout{5} = cost_history;
    varargout{6} = fxy;
    f = min(-log(fused(:)));
    if isinf(f)
        f = 999999999999;
    end
end

function [f, fxy] = inner_func(kappa, step_idx, sensor_idx, experiment, e, model, nx, ny, nsamples, xy, space, fd_model)
    xi = experiment.sensor_locations(:, sensor_idx)';
    delta = xi - experiment.groundtruth(:, step_idx)';

    beta = [model.a, model.b, atan2(delta(2), delta(1))];
    prob = pdf_fx(xy, xi, e(step_idx, sensor_idx)*kappa, nsamples(step_idx), experiment.sigma_zeta, beta, fd_model);
    qwert = reshape(prob, [ny, nx]);
    division = trapz(space.x, trapz(space.y, qwert));
    fxy = qwert / (division + eps);
    f = min(-log(fxy(:)));
    if isinf(f)
        f = 999999999999;
    end
    nout = max(nargout,1) - 1;
    for k = 1:nout
        varargout{k} = fxy;
    end
end

function [f, varargout] = sneaky(kappa, step_idx, sensor_idx, experiment, e, model, nx, ny, nsamples, xy, space, fd_model)
    xi = experiment.sensor_locations(:, sensor_idx)';
    delta = xi - experiment.groundtruth(:, step_idx)';
    [~, qp] = min(abs(space.x - experiment.groundtruth(1, step_idx)));
    [~, qq] = min(abs(space.y - experiment.groundtruth(2, step_idx)));

    beta = [model.a, model.b, atan2(delta(2), delta(1))];
    prob = pdf_fx(xy, xi, e(step_idx, sensor_idx)*kappa, nsamples(step_idx), experiment.sigma_zeta, beta, fd_model);
    qwert = reshape(prob, [ny, nx]);
    division = trapz(space.x, trapz(space.y, qwert));
    fxy = qwert / (division + eps);
    f = min(-log(fxy(qq, qp)));
    if isinf(f)
        f = 999999999999;
    end
    nout = max(nargout,1) - 1;
    for k = 1:nout
        varargout{k} = fxy;
    end
end

function [c,ceq] = unitdisk(x)
    c = vecnorm(x, 2) - 1;
    ceq = [];
end
