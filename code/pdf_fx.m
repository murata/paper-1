function fx = pdf_fx(x, xi, et, n, sigma, beta, fd_model)
    di = vecnorm(x-xi,2,2);
    delta = xi-x;

    if fd_model == "uniform"
        ftheta = 0.5 * pi;  %% Short-hand for uniform distribution denoting lack of knowledge about the directionality
    else
        std = pi / 12;
        thetai = atan2(delta(:, 2), delta(:, 1));
        % ftheta = unifpdf(thetai, -0.5*pi+delta(3), 0.5*pi+delta(3));
        ftheta = (normpdf(thetai, beta(3), std) + normpdf(thetai+2*pi, beta(3), std) + normpdf(thetai-2*pi, beta(3), std)) / 3;
    end
    e_hat = localization_fun_inv(di, beta);
    fd = pdf_fe(e_hat, et, n, sigma) .* e_hat ./ abs(beta(2)) ./ di;
    fx = fd .* ftheta ./ di; % .* normpdf(di, 3, .4);
end
