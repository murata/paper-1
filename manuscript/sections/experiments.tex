The proposed technique's effectiveness and performance were assessed through a series of controlled experiments, the details of which are outlined in this section.

\subsection{Experimental Setup}
To evaluate our vibro-localization approach, we employed the empirical data from a set of controlled experiments in a corridor situated on the $4^{th}$ floor of Goodwin Hall, an operational building on Virginia Tech's campus.
In these experiments, two participants traversed a pre-defined 16-meter path.
\Cref{fig:layout-goodwin} represents the step locations constituting the traversed path---represented with green circles ({\color{green}$\circ$})---as well as the sensor locations---represented with black squares ($\blacksquare$)---overlayed.
We derived our reference points from these investigations and utilized identical experimental data as in \cite{alajlouni2019new} \hl{and }\cite{Alajlouni2022}.
The corridor's concrete floor housed the sensors, which were attached to uniform steel mounts welded to the flanges of the structural I-beams beneath.
In this study, the experimental testbed utilized is embedded within the structural framework of the building.
Due to this integration, a photograph of the testbed would not substantially add to the understanding of the setup, as it predominantly features standard structural components of the building.
The crucial aspects of our setup are its configuration and the placement of sensors and equipment, which are more effectively conveyed through the schematic representation provided in~\Cref{fig:layout-goodwin}.
Eleven PCB Piezotronics model 352B accelerometers, detecting dynamic out-of-plane acceleration within the frequency range of (2, 10000) $Hz$ and with an average sensitivity of 1000 millivolts per $g$ (where $g = 9.8 m/s^2$), recorded the structural vibrations.
These devices captured data from 162 steps taken by each participant, amounting to a total of 324 steps.
The data collection was facilitated by VTI Instruments EMX-4250 digital signal analyzer cards, connected to the accelerometers via coaxial cables and equipped with anti-aliasing filters and a high-precision 24-bit Analog-to-Digital-Converter.
The accelerometer data was sampled at a rate of 1024 $Hz$.
For a comprehensive insight into the experimental design, readers are directed to the foundational study \cite{alajlouni2019new}.

\input{figs/path.tikz}

\subparagraph{Data and Model Validity:} The preliminary study gathered vibration data during low-activity periods, ensuring minimal movement in the vicinity of the instrumented corridor.
The data revealed that the sensors' noise profiles were normally distributed with zero mean and consistent variance.
Thus, the signal model in \Cref{eq:zi} aptly represents the vibration measurements.
Given this observation, we confidently state that the energy-related random variables, represented as \( e_i \) for \( i \in \mathcal{M} \), align with the experimental findings.

\subparagraph{Signal Detection Problem:}
A primary distinction exists between our implementation of the baseline and the original work presented in their publication~\cite{alajlouni2019new}: the signal detection algorithm.
The baseline study, in its methodology, adopted a tight-window approach.
This approach was characterized by the identification of the first time instance that the signal breaks the \gls{snr} envelope and the peak of the signal.
In this study, on the other hand, we used a stochastic signal detection algorithm denote the time steps which the signal magnitude breaks the noise floor and eventually dissipates below the noise floor.
To contrast our method, the detection algorithm employed in this study took a more flexible stance: instead of strictly searching for the time instance we signal peaks, our algorithm was designed to be more lenient.
It permitted "silent" periods, which are intervals without significant signal activity, both before and after the vibration.
This choice of algorithm led to a notable difference in the signal energy values when compared to the baseline study.
\Cref{fig:detection-results} graphically demonstrates the difference between the signal detection algorithm employed by the baseline and the proposed technique with an impulse response curve of an underdamped second-order system.
The black line represents the elements of the ``noisy'' vibro-measurement vector.
The dashed red and green lines represent the results of the baseline and proposed detection algorithm, respectively.
This distinction in approach not only highlights the variability in signal processing techniques but also underscores the potential impact of these choices on the final results and interpretations.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=1]{figs/detection-results.eps}
    \caption{This figure demonstrates the differences between signal (step) detection algorithms employed by the baseline and proposed techniques. The black line (\textbf{---}) represents the noisy measurements of a second-order system. The green dashed line ({\color{green} \textbf{--- -}}) represents the proposed ``relaxed'' detection results employed in this study. On the other hand, the red dashed line ({\color{red} \textbf{--- -}}) represents the signal detection algorithm employed by the baseline study.}
    \label{fig:detection-results}
\end{figure}

% Another significant variation from the baseline study is the post-localization filtering, which utilized a Kalman filter.
% The Kalman filter is a mathematical technique designed to estimate the state of a system by incorporating the dynamics of the system with a series of measurements obtained over time.
% It excels in situations where the system is subject to uncertainties or noise.
% The filter operates by first predicting the system's future state and then updating this prediction when new measurements become available.
% This iterative method allows the Kalman filter to minimize the estimation error by balancing the predicted and measured values.
% As a result of employing the Kalman filter in the baseline study, which likely accounted for these linear paths that the occupants followed, it achieved smaller localization errors compared to our reported results of the baseline.

\subsection{Implementation}
In the course of our data processing, we discretized the localization space $\mathcal{S}$.
This discretization was achieved by segmenting it into a total of 270,000 grid cells, specifically arranged in a $300 \times 900$ configuration.
By evaluating the \glspl{pdf} only at the center of each grid cell, we avoided the intricate surface integrals and achieved greater computational efficiency.


\hl{The calibration vectors $\vect{\upbeta}_i$ are obtained in an offline processing step, where the signal energy and their known distance measurements are used to fit the parametric decay models denoted as }$g_{i}(e_i; \vect{\upbeta_i})$.
\hl{These measurements can be obtained by exciting the floor, for instance, by hitting it with a hammer, at known locations.
This process facilitates the accurate calibration of the system by correlating the known physical impacts at specific points with the resulting signal characteristics.
In this work, we only used the first 27 steps of a Occupant-1's data to obtain these calibration vectors for all sensors.}

\hl{In the online processing, when a footstep detection is made, the signal energy of the vibration measurements are used to minimize the }$\log$\hl{-transformed loss function~}\Cref{eq:loss}\hl{ by adjusting the value of vector }$\vect{\upkappa} = \left(\kappa_1, \ldots, \kappa_m\right)^\top$\hl{ in the direction of its gradient.
Consequently, the information-theoretic }\gls{bse}\hl{ algorithm was employed each iteration of gradient descent to discard Byzantine sensors.
The gradient descent steps are repeated until the algorithm converges to a solution and forms a consensus among the sensors.}

%  we found that they could be expressed more succinctly as summations.
% One might argue that such discretization could introduce precision issues, and indeed, there is some validity to this concern.
% However, the primary motivation behind this approach was to streamline and simplify our algorithm.
% This strategic simplification had a cascading effect on our calculations.
% Notably, many of the equations integral to our proposed vibro-localization technique were transformed.

To evaluate the proposed technique, we employed a combinatorial study to analyze the effect of the number of sensors used on the localization metrics.
Specifically, we employed all the possible combinations of $m=\{2, \ldots, 11\}$ with the given sensor configuration in the experimental data.
This yields $n_{cases} = \sum_{i=2}^{11} \binom{11}{i} = \num{2036}$ number of cases to evaluate for each step and occupant.
In other words, each step is reevaluated \num{2036} times, yielding \num{329832} ($\num{2036} \times 162$) data points for each occupant.
Therefore, in our analysis, we are able to provide different defining statistical characteristics of the localization error.
This approach also enables us to remedy various uncertainty sources such as the effects of sensor placement, and differences in propagation paths while enriching the results independent of the individual sensor performance.
\hl{The source code of this study is available on request.}
