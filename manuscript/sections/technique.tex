In this section, the manuscript provides its principal contribution: a new energy-based vibro-localization technique.
As an individual traverses the environment, the force exerted by their heel-strikes on the floor induces structural vibration waves within it.
These waves propagate through the floor and reach the sensors placed on the floor thereby generating vibro-measurements.
Along the propagation path, the vibration wave is deformed and disturbed by various factors that are internal and external to the floor.
The proposed technique employs these deformed vibro-measurements of the floor to determine the locations of occupants, despite challenges posed by measurement uncertainties and the potential presence of Byzantine sensors.
\input{figs/fig_interaction.tikz}

\subparagraph{Definitions and Notation}
Consider the problem of estimating a single footstep location $\vect{x}_i \in \mathcal{R}^2$ that belongs an occupant located at $\vect{x}_{true} \in \mathcal{R}^2$.
In this estimation problem, the vibro-measurements $z_i[k]$, that are obtained by $i^{th}$ of $m$ sensors placed under a floor between time steps $k \in \{1, \ldots, n\}$, are employed.
%  as depicted in \Cref{fig:overview}.
Let $\mathcal{M} \triangleq \{1, \ldots, m\}$ be the index set of all the sensors and sensor $i \in \mathcal{M}$ be located at $\vect{t}_i$ in a rectangular localization space $\mathcal{S}$.
The localization space $\mathcal{S}$ is an arbitrary closed shape within which all the sensors and the occupants are contained.
Assume, the vibro-measurements $z_i[k]$ are disrupted with measurement error $\upzeta_i[k]$ when the true vibro-measurement are $z_{true, i}[k]$.
% In this study, we define a measurement error due to characteristics of the sensor as $\upzeta_i[k]$.
We derive the relationship between the true vibro-measurements and the sensor's output as,

\begin{equation}
    z_i[k] = z_{true, i}[k] + \upzeta_i[k]~.
    \label{eq:zi}
\end{equation}

We often use a shorthand notation to represent these quantities in vector form associated with the time snapshot between $1 \leq k \leq n$.
Let us define these vectors for sake of clarity: $\vect{z}_i \triangleq \left(z_i[1], \ldots, z_i[n] \right)^\top \in \mathcal{R}^n$, $\vect{z}_{true, i} \triangleq \left(z_{true, i}[1], \ldots, z_{true, i}[n] \right)^\top \in \mathcal{R}^n$ and $\vect{\upzeta}_i \triangleq \left( \zeta_i[1], \ldots, \zeta_i[n] \right)^\top \in \mathcal{R}^n$.

Given these definitions, we derive a probabilistic localization framework that makes use of the signal energy of the vibro-measurement vector $\vect{z}_i$ and its \gls{pdf} with a novel \gls{bse} algorithm to benefit from as many sensors as possible in estimating the location vector $\vect{x}_{true}$.
The proposed localization technique employs a parametric energy decay model to estimate the distance between the sensor and the occupant.

The energy $e_i$ of the stochastic vibro-measurement vector $\vect{z}_i$ can be derived by employing Rayleigh's theorem as shown in \Cref{thm:rayleigh-deterministic}.
The signal energy of a random measurement vector $\vect{z}_i$ is
\begin{subequations}
    \begin{equation}
        e_i = \norm{\vect{z}_i}_2^2 = \vect{z}_i^\top \vect{z}_i \in \mathbb{R}_{+}~.
        \label{eq:energy}
    \end{equation}
    By plugging \Cref{eq:zi} in \Cref{eq:energy}, we derive the energy as:
    \begin{equation}
        e_i = \left( \vect{z}_{true, i} + \vect{\upzeta}_i \right)^\top \left( \vect{z}_{true, i} + \vect{\upzeta}_i \right)~.
        \label{eq:rayleigh}
    \end{equation}
    Notice that $e_{true, i} = \vect{z}_{true, i}^\top \vect{z}_{true, i}$; therefore, the relationship between the measured energy $e_i$ and the energy that was supposed to be registered $e_{true, i}$ is given by
    \begin{equation}
        e_i = e_{true,i} + \varepsilon_i
    \end{equation}
    where $\varepsilon_i = \vect{\upzeta}_i^\top \vect{\upzeta}_i + 2 \vect{\upzeta}_i^\top \vect{z}_i$ represents the error in signal energy.
\end{subequations}

We assume the disturbance vector $\vect{\upzeta}_i$ is an independently and identically distributed normal random vector, i.e., $\zeta_i[k] \sim \N{\mu_\zeta}{\sigma_\zeta}$.
Therefore, we can show $\vect{z}_i \sim \NSTD{\vect{z}_{true,i} + \mu_\zeta}{\sigma_\zeta^2 \vect{I}}$.
With this, we present our proposition showing that $\f{E_i}$ can be approximated with a normally distributed random variable when the number of samples $n$ is sufficiently large.

\begin{Proposition}
    The energy of a stochastic vibro-measurement vector can be approximated with a normally distributed random variable with a mean $\mu_E$ and variance $\sigma^2_E$.
    \label{thm:opus-theorem}
    \begin{equation}
        e \simeq \N{\mu_e}{\sigma_e}~.
    \end{equation}
\end{Proposition}
The details of this assertion can be seen below.
\begin{proof}
    \label{pro:energy-normal}
    If both sides of the \Cref{eq:rayleigh} is divided with the variance $\sigma_\zeta^2$, we have:
    \begin{equation*}
        \frac{e_i}{\sigma_\zeta^2} = \frac{1}{\sigma_\zeta^2} \vect{z}_i^\top \vect{z}_i~.
    \end{equation*}

    Recall $\zeta_i[k] \sim \N{\mu_\zeta}{\sigma_\zeta}$; therefore, $\frac{\zeta_i[k]}{\sigma_\zeta} \sim \NSTD{\frac{\mu_\zeta}{\sigma_\zeta}}{1}$.
    The left-hand side of the equation above is a random variable that is the squared sum of $n$ independently and identically distributed normal random variables that have unit variances.

    Thus, we can show that $\frac{e_i}{\sigma_\zeta^2} \sim \chi^{\prime 2}_n\left(\lambda\right)$ follows a noncentral chi-squared distribution with n degrees-of-freedom and noncentrality parameter $\lambda = \frac{1}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)$ (cf. \Cref{thm:noncentral-chi}).

    By invoking \Cref{thm:normal-approximation}, we derive an approximated \gls{pdf} of \( e_i / \sigma_\zeta^2 \) with a normal distribution when the number of time samples is sufficiently large (empirically $n>20$),
    \begin{equation*}
        \frac{e_i}{\sigma_\zeta^2} \approx \NSTD{n + \frac{1}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)}{2 n + \frac{4}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i} + \mu_\zeta\right)}~.
    \end{equation*}

    Finally, the scalability property of normal distribution can be employed to derive the distribution of the signal energy $e_i \simeq \N{\mu_{E_i}}{\sigma_{E_i}}$, where the mean and variance are characterized as shown below

    \begin{subequations}
        \begin{align}
            \mu_{E_i} &= \expt{e_i} = \sigma^2_\zeta n + \left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)~,
            \intertext{and}
            \sigma_{E_i}^2 &= \Var{e_i} = 2\sigma^4_\zeta n+4 \sigma^2_\zeta \left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)~.
        \end{align}
    \end{subequations}
\end{proof}

As a consequence of \Cref{thm:opus-theorem}, we present the next corollary:

\begin{Corollary}
    If the sensor is calibrated such that sensor bias is negligibly small, i.e., $\mu_\zeta  \approxeq 0$ and the variance of the measurement error is known $\sigma_\zeta^2$, then the energy distribution can be parameterized with number of samples $n$ and the unknown true energy $e_{true,i} = \vect{z}_{true, i}^\top \vect{z}_{true, i}$ that the sensor was supposed to register:
    \begin{equation*}
            e_i \sim f_{E_i}(e_i; e_{true, i}) \approx \NSTD{n \sigma_\zeta^2 + e_{true, i}}{2 n \sigma_\zeta^4 + 4 e_{true, i} \sigma_\zeta^2} = \dfrac{1}{\sigma_{E_i} \sqrt{2\pi}}\exp{\left[-\frac{\left(e_i-\mu_{E_i}\right)^2}{2\sigma^2_{E_i}} \right]}~,
    \end{equation*}
    where the mean $\mu_E$ and variance $\sigma_E^2$ of the energy distribution are given by
    \begin{subequations}
        \begin{align}
            \mu_{E_i} &= \expt{e_i} = \sigma_\zeta^2 n + e_{true, i}~,
            \intertext{and}
            \sigma_{E_i}^2 &= \Var{e_i} = 2 n \sigma_\zeta^4 + 4 e_{true, i} \sigma_\zeta^2~.
        \end{align}
    \end{subequations}
\end{Corollary}

\subsection{Parametric Energy Decay and Localization Framework}
In this work, we exploit the notion that the signal energy decreases as the structural vibration wavefronts propagate along a path.
Based on this concept, we propose a localization function $\vect{h}_i: (e_i, \theta_i)^\top \mapsto \vect{x}_i$ that maps the signal energy $e_i$ concerning the vibro-measurement vector $\vect{z}_i$ and the directionality of the occupant $\theta_i$ to a location vector $\vect{x}_i \in \mathcal{S}$.
The localization function $\vect{h}_i(e_i, \theta_i; \boldsymbol{\upbeta}_i)$ can be represented as,

\begin{equation}
    \vect{x}_i = \vect{h}_i\left(e_i, \theta_i; \boldsymbol{\upbeta}_i\right) = \vect{t}_i + g_i(e_i; \boldsymbol{\upbeta}_i)
    \begin{bmatrix}
        \cos{\theta_i} \\
        \sin{\theta_i}
    \end{bmatrix}~,
    \label{eq:xi}
\end{equation}
where $\vect{\upbeta}_i$ represents a known calibration vector of a parametric energy decay model $g\left(e_i; \vect{\upbeta}_i \right):e_i \mapsto d_i$ that maps energy $e_i$ to the distance $d_i$.
The calibration vectors $\vect{\upbeta}_i$, $\forall i \in \mathcal{M}$, are obtained in a pre-deployment calibration step.
A graphical representation of \Cref{eq:xi} is shown in \Cref{fig:equation-graphical}.
It should be noted that the parametric energy decay model $g(\cdot)$ is assumed to be a monotonically decreasing function for some positive energy measurement $e_i \in \mathbb{R}_+$; therefore, it is bijective and its inverse exists.
\hl{In this work, we employed a curve that represents the relationship between the energy of the vibro-measurement vectors and the propagation distance as a power curve.
Formally, this curve can be shown below,} $d_i = \beta_{0, i} \, e_i^{\beta_{1, i}}$\hl{, where calibration vector is given by }$\vect{\upbeta}_i = \left(\beta_{0, i}, \beta_{1, i} \right)^\top$.

\input{figs/layout.tikz}

In this framework, the occupant location with respect to the $i^{th}$ sensor is parameterized with its representation in a polar coordinate system centered at sensor location $\vect{t}_i$.
% In other words, the proposed framework has the flexibility to distinguish the distance $d_i$ and the directionality $\theta_i$ components separately.
As an accelerometer by itself provides only ranging information, i.e., the distance between the occupant and itself, we assume directionality component $\theta_i$ is completely unknown.
Therefore, we can simply model it with a random variable that follows a uniform distribution between the range $(0, 2\pi)$, which is denoted as $\theta_i \sim \mathcal{U}(0, 2\pi)$.

The defining properties of location estimate $\vect{x}_i$ can be obtained by studying its \gls{pdf} $\f{\vect{X}_i}$.
The derivation of the \gls{pdf} $\f{\vect{X}_i}$ given the \gls{pdf} of signal energy $\f{E_i}$ is a straight-forward application of the density transformation theorem shown in \Cref{thm:density}.
The following section provides the signal model with which the signal energy $e_i$ can be computed from the vibro-measurement vector $\vect{z}_i$.

\subsection{Derivation of the \gls{pdf} for Location Estimation}
Given the localization function $\vect{h}_i(e_i, \theta_i; \vect{\upbeta}_i)$ that maps $(e_i, \theta_i)^\top$ to $\vect{x}_i$, we can derive the \gls{pdf} of the location estimate $\vect{x}_i$ using the density transformation theorem, as presented in \Cref{thm:density},

\begin{subequations}
    \label{eq:fxi}
    \begin{align}
        f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right) &= f_{E_i, \theta_i}\left( g_{i}^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right), \theta_i; e_{true,i}, \vect{\upbeta}_i \right) \left\lvert \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)}\right\rvert~,
        \intertext{because $E_i$ and $\theta_i$ are independent from each other:}
        &= f_{E_i}(g_{i}^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right); e_{true,i}, \vect{\upbeta}_i) \f{\theta_i} \left\lvert \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)}\right\rvert~,
        \intertext{where $\f{\theta_i} = \frac{1}{2\pi}$ and $f_{E_i}(e; e_{true,i}, \vect{\upbeta}_i) = \frac{1}{\sigma_E \sqrt{2\pi}}\exp{\left[-\frac{\left(e-\mu_E\right)^2}{2\sigma^2_E} \right]}$.
        Therefore,}
        f_{\vect{X}_i} \left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right) &= \frac{1}{\sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g_i^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]} \left\lvert \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)}\right\rvert~,
    \end{align}
\end{subequations}
where $\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)$ denotes the Jacobian matrix of inverse localization function $\vect{h_i}^{-1}(\cdot)$ evaluated at $\vect{x_i}$.

The definition of the Jacobian matrix $\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i)$ is given as
\begin{equation}
    \label{eq:jacobian}
    \vect{J}^{-1}_{\vect{h}_i}(\vect{x}) = \begin{bmatrix}
        \frac{\partial g_{i}^{-1}}{\partial x}\left(\norm{\vect{x}-\vect{t}_i}_2; \vect{\upbeta}_i \right) & \frac{\partial g_{i}^{-1}}{\partial y}\left(\norm{\vect{x}-\vect{t}_i}_2; \vect{\upbeta}_i \right) \\
        \frac{\partial \angle{\left(\vect{x}-\vect{t}_i\right)}}{\partial x} & \frac{\partial \angle{\left(\vect{x}-\vect{t}_i\right)}}{\partial y}
    \end{bmatrix}~,
\end{equation}
where,
\begin{equation*}
    \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x})} = \frac{1}{\norm{\vect{x} - \vect{t}_i}}\frac{\partial g_{i}^{-1}}{\partial \vect{x}}\left( \norm{\vect{x} - \vect{t}_i}; \vect{\upbeta}_i\right)~.
\end{equation*}

By plugging \Cref{eq:jacobian} into \Cref{eq:fxi}, we derive the \gls{pdf} fully:
\begin{equation}
    \Rightarrow f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right) = \frac{\left\lvert \frac{\partial g_{i}^{-1}}{\partial \vect{x}_i}\left( \norm{\vect{x}_i - \vect{t}_i}; \vect{\upbeta}_i \right) \right\rvert}{ \norm{\vect{x}_i - \vect{t}_i} \sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g_{i}^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]}~.
    \label{eq:fxi_final}
\end{equation}

\Cref{eq:fxi_final} shows the \glspl{pdf} $f_{\vect{X}_i}(\cdot)$ assign a probability value for an occupant located at vector $\vect{x}_i$ given the inverse of the parametric decay function $g(\cdot)$ and sensor location $\vect{t}_i$.
This equation is one of the fundamental contributions of this study.
Notice the term in the denominator, i.e., $\norm{\vect{x}_i - \vect{t}_i}$, in \Cref{eq:fxi_final} resulting in an inverse relationship between the probability values and the distance between the sensor and the impact location.

\subsection{Sensor Fusion}
Given the \glspl{pdf} $f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i\right)$ for all sensors indexed by $i \in \mathcal{M}$, we aim to find a joint-\gls{pdf} that represents the collective localization outcome using the vibro-measurements of $m$ sensors.
As all the sensors are independent of each other, we can represent the joint-\gls{pdf} as given below:

\begin{equation}
    f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\vect{x}_1, \ldots, \vect{x}_m; e_{true, 1}, \ldots, e_{true, m}, \vect{\upbeta}_1, \ldots, \vect{\upbeta}_m \right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right)~.
\end{equation}
Let $\kappa_i = \dfrac{e_{true,i}}{e_i}$ be an independent variable denoting the ratio between the unknown true energy $e_{true, i}$ and measured signal energy $e_i$.
Notice that $e_i = e_{true, i} + \epsilon_i$; thus, $\kappa_i = 1 + \Delta$ for small $\Delta$.
Given this definition, we can reparameterize the joint-\gls{pdf} which forms the basis for the sensor fusion algorithm used in this paper,

\begin{equation}
    f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\vect{x}_1, \ldots, \vect{x}_m; \kappa_1, \ldots, \kappa_m, \vect{\upbeta}_1, \ldots, \vect{\upbeta}_m \right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; \kappa_i, \vect{\upbeta}_i \right)~.
    \label{eq:sf}
\end{equation}

\subsection{Byzantine Sensor Elimination}
\input{figs/sensor_fusion.tikz}

Byzantine sensors, as illustrated in \Cref{fig:sensor-fusion}, are those that provide misleading or incorrect data, often deviating from the true value or introducing conflicting information into a sensor network.
In the figure, sensors $A$ and $B$ are examples of informative sensors (shown in image (a) and (b)) while $C$ and $D$ are instances of Byzantine sensors (image (c) and (d)).
The \glspl{pdf} of the Byzantine sensors have a sharp peak indicating high precision, but they show offsets from the true value, revealing their low accuracy.
When such a sensor's data is fused with data from other sensors, it can significantly distort the resulting joint likelihood, leading to erroneous conclusions or alternative hypotheses about the occupant's location.

The second row of the figure provides insights into the effects of fusing data from a Byzantine sensor with informative ones.
For instance, image (f) showcases the fusion result of an informative Sensor $A$ with the Byzantine Sensor $C$.
The resulting uniform distribution across the localization space highlights the lack of consensus between the two, emphasizing the detrimental impact of the Byzantine sensor on the fusion process.
Similarly, image (g) depicts the fusion of Sensor $B$ with Sensor $C$, producing an offset peak that suggests an alternative hypothesis about the occupant's location.
To ensure the reliability of a sensor network, it's crucial to identify and eliminate such Byzantine influences.
By observing the fusion results and identifying distributions that deviate from expected patterns or the true value, one can iteratively pinpoint and remove Byzantine sensors, enhancing the overall accuracy and trustworthiness of the network.

In vibro-localization systems, there exist many factors that can easily render a sensor as a Byzantine.
For instance, the parametric energy decay model assumes the wavefronts detected by an accelerometer travel the direct path from the step location to the sensor observing the vibration phenomenon.
However, this assumption seldom holds for indoor environment as the wavefronts may be reflected by various objects or boundaries that exist in the environment.
To address and circumvent these undesired scenarios, we introduce an algorithm that identifies a consensus-forming subset of sensors within \( \mathcal{M} \) to counteract the influence of Byzantine sensors.

\glsreset{bse}

During the initialization phase of the information-theoretic \gls{bse} algorithm, a comprehensive computation is carried out to determine all conceivable pairwise joint-\glspl{pdf} and their corresponding entropies.
Subsequently, an initial consensus set is established using the sensor pair $(i, j)$ that produces the maximum entropy after fusion from a distinct pair of the index set $\mathcal{M}$.
Formally, we represent this initiation phase as given below.
\begin{equation}
    \mathcal{C} = \left\{i,j \mid \argmax_{i, j} \expt{-\log{f_{\vect{x}_i, \vect{x}_j}}\left( \vect{x} \right)} \right\}
\end{equation}

Furthermore, we represent the joint-\gls{pdf} encapsulating the existing consensus at any point in time, denoted as $f_{\mathcal{C}} \left(\vect{x}\right)$, as follows:
\begin{equation}
f_{\mathcal{C}} \left(\vect{x}\right) \triangleq  \prod_{\forall i \in \mathcal{C}} f_{\vect{X}_i}\left(\vect{x}; \kappa_i, \vect{\upbeta}_i \right)
\label{eq:loss}
\end{equation}

Throughout each iterative phase, a candidate sensor, denoted as $k$ (which is distinct from the pair $(i, j)$), is selected from the set $\mathcal{M}$.
This sensor undergoes an evaluation against the prevailing consensus set $\mathcal{C}$, achieved by fusing its \gls{pdf} with the consensus \gls{pdf} $f_{\mathcal{C}}(\vect{x})$.
A subsequent determination is predicated upon the entropy, or more precisely, the surprisal of the resulting hypothesis in with respect to the current consensus.
Should the integration of sensor $k$ not attenuate the joint-\gls{pdf} to a uniform distribution (as exemplified in case (f) in \Cref{fig:sensor-fusion}) or not decrease the average entropy, it is incorporated into the consensus set.
Otherwise, sensor $k$ is classified as Byzantine.
Finally, the above procedure undergoes an iterative repetition with varying vectors of $\vect{\upkappa} = \left(\kappa_1, \ldots, \kappa_m\right)^\top$.
This iteration progresses in the direction of the gradient of the $f_\mathcal{C}(\vect{x})$ calculated with respect to $\vect{\upkappa}$ vector.
The process persists until a local maxima in the entropy landscape, corresponding to a (possibly locally-)optimal consensus, is identified.
The proposed \gls{bse} algorithm, coupled with the vibro-localization technique, is outlined in \Cref{alg:cap}.

\begin{algorithm}[htb!]
    \caption{Vibro-localization using the Information-Theoretic \gls{bse} Algorithm\label{alg:cap}}
    \begin{algorithmic}[1]
        \Procedure{BSE}{$f, \mathcal{M}, \mathcal{S}$}
            \State $h^* \gets -\infty$
            \ForAll{$(i, j) \in \mathcal{M} \times \mathcal{M}$, $i \neq j$} \Comment{Initialization of the consensus set}
                \State $h_{i,j} \gets \textproc{entropy}(\textproc{fuse}(f_{\vect{x}_i}, f_{\vect{x}_j}), \mathcal{S})$
                \If{$h_{i,j} \leq h^*$} \State $h^*, \mathcal{C} \gets h_{i,j}, \{i, j\}$ \EndIf
            \EndFor
            \ForAll{$i \in \mathcal{M} \setminus \mathcal{C}$} \Comment{Attempt to expand the consensus set}
                \State $h_{i} \gets \textproc{entropy}(\textproc{fuse}(f_{\mathcal{C}}, f_{\vect{x}_i}), \mathcal{S})$
                \If{$h_{i} \geq h^*$} \State $h^*, \mathcal{C} \gets h_{i}, \mathcal{C} \cup \{i\}$ \EndIf
            \EndFor
            \State \textbf{return} $\mathcal{C}$
        \EndProcedure
        \State $\vect{\upkappa} \gets \vect{1}_{m \times 1}$
        \While{$\norm{\nabla \vect{\upkappa}} \leq thr$} \Comment{Gradient Descent}
            \State $\vect{\upkappa} += \nabla \vect{\upkappa}$ \Comment{Update $\vect{\upkappa}$ in the gradient direction}
            \State $f_{\vect{x}_1}, \ldots, f_{\vect{x}_m} \gets \textproc{density-projection}(e_1, \ldots, e_m, \vect{\upkappa})$
            \State $\mathcal{C} \gets \textproc{BSE}(f_{\vect{x}_1}, \ldots, f_{\vect{x}_m}, \mathcal{M}, \mathcal{S})$
            \State $f_{\mathcal{C}} \gets \textproc{FUSE}(f_{\vect{x}_i}~\forall i \in \mathcal{C})$
            \State $\vect{x}^* = \argmax_{\vect{x} \in \mathcal{S}} f_{\mathcal{C}}(\vect{x})$
        \EndWhile
    \end{algorithmic}
\end{algorithm}

At an initial glance, readers might find similarities between the algorithm above and the RANSAC algorithm~\cite{Fischler1981}, an acronym for Random Sample Consensus; however, they are fundamentally distinct.
RANSAC is an algorithm employed in fields such as computer vision and computational geometry to robustly estimate model parameters, even in the presence of outliers.
In contrast, the information-theoretic \gls{bse} algorithm is tailored for sensor networks to counteract Byzantine sensors using information-theoretic strategies.
While both algorithms seek to establish reliability (or robustness) and consensus (or agreement) within datasets, their approaches, and primary use cases are notably distinct.
A side-by-side comparison of their characteristics is presented in Table~\ref{tab:comparison}.

\begin{table}[htb]
    \begin{adjustwidth}{-\extralength}{}
        \centering
        \begin{tabularx}{\linewidth}{lXXX}
            \toprule
            & \textbf{RANSAC} & \textbf{\citet{mirshekari2018occupant}} & \textbf{Information-theoretic \gls{bse}} \\
            \midrule
            \textbf{Primary Use} & 
            Estimating parameters of mathematical models in the presence of outliers, predominantly in computer vision. & 
            \hl{Elimination of far-away sensors in an adaptive multilateration technique of a vibro-localization system.} &
            Elimination of Byzantine sensors in sensor networks of vibro-localization systems by using information theory. \\
            \midrule
            \textbf{Methodology} & 
            Works by randomly selecting subsets of data and identifying the model with the highest consensus. &
            \hl{Identifying distant sensors in }\gls{tdoa}\hl{ estimations to avoid bias in multilateration algorithm. }&
            \hl{Derives a consensus among sensors based on entropies of likelihoods to emphasizes robustness against malicious sensors.} \\
            \midrule
            \textbf{Input Data Type} & 
            Points & 
            \hl{Time-domain measurements}&
            \glspl{pdf} \\
            \bottomrule
        \end{tabularx}
        \caption{Comparison between RANSAC, \cite{mirshekari2018occupant}, and Information-theoretic \gls{bse} algorithm}
        \label{tab:comparison}
    \end{adjustwidth}
\end{table}

