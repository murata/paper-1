This section presents the empirical outcomes derived from our proposed vibro-localization technique.
These empirical results benchmark the efficacy of our approach in terms of accuracy, and precision.
By analyzing these findings in detail, we aim to provide a deeper look into the technique's performance under various conditions and scenarios.

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \includegraphics[scale=1]{figs/results-1.eps}
        \caption{An illustrative result of the $1^{st}$ occupant's data.}
        % \label{fig:result-pdf-occupant-1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[scale=1]{figs/results-2.eps}
        \caption{An illustrative result of the $2^{nd}$ occupant's data.}
        % \label{fig:result-pdf-occupant-2}
    \end{subfigure}
    \caption{Localization outcomes for two distinct occupants using varying sensor counts ($m=2, 6, 11$). The left column represents the first occupant's result set and the right, the second occupant's result set. Square markers indicate sensor locations, circles denote non-Byzantine sensors, while green pluses and red crosses symbolize the ground truth and estimated locations, respectively. Errors for configurations (a) to (e) show progressive refinement with increased sensors, highlighting the algorithm's adaptability and precision.}
    \label{fig:localization-results}
\end{figure}

In \Cref{fig:localization-results}, three representative sets (out of \num{659664} cases) of outcomes are depicted, each corresponding to a different number of sensors ($m=2, 6, 11$) employed for the localization of identical step data for two occupants.
The left column presents results for the first occupant, while the right column pertains to the second occupant.
Sensor locations within this figure are marked by square markers ($\blacksquare$), and sensors deemed non-Byzantine by the algorithm are highlighted with circular markers ($\circ$).
The green plus (\textcolor{green}{$+$}) and red cross (\textcolor{red}{$\times$}) markers respectively represent the ground truth $\vect{x}_{\text{true}}$ and the estimated location vector $\vect{x}^*$.
In the scenarios labeled (a) for the first occupant and (b) for the second, utilizing two sensors, the norm of localization errors are \num{1.39} and \num{1.1784} meters, respectively.
Both sensors are considered as the consensus set due to the lack of alternative sensor choices.
As the sensor count increases to six, as shown in labels (c) for the first occupant and (d) for the second, the observed errors were \num{1.39} for the first occupant and \num{1.172} meters for the second occupant.
However, the first occupant's results do not show significant improvement with the additional sensors.
In these cases, the initial sensors were adaptively substituted with new sensors for consensus.
With a further increase to eleven sensors, as indicated in labels (e) and (f), the localization errors reduce to \num{0.3} centimeters for the first occupant and \num{54.4} centimeters for the second, both accompanied by an updated consensus set.

To evaluate the influence of sensor count on localization outcomes, especially in terms of accuracy and precision, the quantile function, represented as $x = Q(p)$, of the localization error was plotted against the number of sensors, as illustrated in \Cref{fig:localization-error,fig:entropy}.
This function yields the error value $x$ for a given probability $p$, satisfying the condition $\prob{X \leq x} = p$.
In essence, it serves as the inverse of the \gls{cdf} for the random variable $x$.
For clarity, $Q(0.5)$ corresponds to the median of the localization error across varying sensor counts.

In \Cref{fig:localization-error}, the quartiles of sample localization errors---the first (25${}^{th}$ percentile), second (50${}^{th}$ percentile or median), and third (75${}^{th}$ percentile)---are plotted against the number of sensors available in the localization system.
In other words, the number of sensors listed in the figure represents the sensor count before the proposed \gls{bse} algorithm eliminates a subset from the sensor pool.
The data for the first and second occupants are differentiated by red and black colors, respectively.
For the first occupant, the solid ({\color{red} --}), dashed ({\color{red}-- -}), and dotted ({\color{red}$\cdot~\cdot$}) red lines represent the respective quartiles of the localization error.
For the second occupant, the solid ({\color{black} --}), dashed ({\color{black}-- -}), and dotted ({\color{black}$\cdot~\cdot$}) black lines serve the same purpose.
The figure indicates a reduction in localization error with an increasing number of sensors.
This trend is consistent across all quartiles.
Notably, as more sensors are introduced, the disparity between the first and third quartiles diminishes, highlighting enhanced accuracy in both optimal and suboptimal conditions.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=1]{figs/sensor-errors.eps}
    \caption{Quartile analysis of sample localization errors against the number of sensors before the proposed \gls{bse} algorithm was employed. The plot showcases a consistent reduction in errors across all quartiles with an increasing number of sensors, highlighting improved consistency in both best- and worst-case scenarios.}
    \label{fig:localization-error}
\end{figure}

As it is evident in~\Cref{tab:results}, a consistent trend across both occupants and all quartiles were seen: as the number of sensors increases, the localization error (measured in all metrics) decreases.
% This trend is evident in the measurements, where the mean and median errors reduce with an increasing number of sensors.
For instance, the median error for the first occupant decreases from \num{2.4481} with two sensors to \num{1.7931} meters with eleven sensors in the proposed technique (see \Cref{tab:results}).
Similarly, for the second occupant, it reduces from \num{2.3852} to \num{1.6147} meters for the same number of sensors.
The standard deviation, representing the error variability, also shows a steady decrease, an evidence of a growth in consistency, as more sensors are employed. This reduction in error and variability is a clear indication of enhanced accuracy and reliability in the localization process.

\Cref{fig:entropy} presents the precision of the entire localization system in terms of entropy, or the surprisal, encoded in the joint-\glspl{pdf}.
Akin to the previous figure, the data for the first and second occupants are differentiated by red and black colors, and the same styling is used to represent the same quartiles.
% For the first occupant, the solid ({\color{red} --}), dashed ({\color{red}-- -}), and dotted ({\color{red}$\cdot~\cdot$}) red lines represent the respective quartiles of the entropy in the joint-\glspl{pdf}.
% For the second occupant, the solid ({\color{black} --}), dashed ({\color{black}-- -}), and dotted ({\color{black}$\cdot~\cdot$}) black lines serve the same purpose.
The figure distinctly shows a decrease in uncertainty as the number of sensors grows.
This trend is consistent across all quartiles.
Significantly, with the addition of more sensors, the gap between the first and third quartiles narrows, indicating enhanced precision in both best- and worst-case scenarios.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=1]{figs/sensor-entropies.eps}
    % \caption{Precision of the localization system represented in terms of entropy encoded in the joint-\glspl{pdf} for varying sensor counts. The data for the first and second occupants are differentiated by red and black lines, respectively. The solid, dashed, and dotted lines correspond to the first, second, and third quartiles of the entropy. The figure highlights a consistent reduction in uncertainty across all quartiles with an increasing number of sensors, emphasizing enhanced precision in both optimal and suboptimal scenarios.}
    \caption{Entropy-based precision of the localization system for varying sensor counts. Red and black lines differentiate data for the first and second occupants. The figure underscores reduced uncertainty with more sensors, highlighting enhanced precision across all quartiles.}
    \label{fig:entropy}
\end{figure}

\Cref{fig:entropy-error} synthesizes the insights derived from \Cref{fig:localization-error} and \Cref{fig:entropy}, demonstrating a discernible correlation between accuracy and precision metrics obtained with the proposed localization technique.
The figure demonstrates that enhancements in precision are parallel with improvements in accuracy.
This trend can be observed for both of the occupants even with different numbers of sensors used in the localization system.
This trend, as can be seen in the figure, can be described as quasi-linear curves where the range of the lines differs with the number of sensors used in the technique.
Also, it can be seen in the figure that sub-meter localization accuracy is viable even with two sensors if the sensors yield a certain level of measurement precision.
On the other hand, for the higher number of sensors, this goal is more attainable as their curves span more in the sub-meter region.
Furthermore, note that the magnitude of the improvement in accuracy with the improved precision differs for different numbers of sensors employed in the system.
In other words, more desirable results become prominent when more sensors are used.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=1]{figs/entropy-vs-error.eps}
    \caption{This figure shows a Quantile-Quantile plot between the precision and accuracy metrics observed in the experimental data. The figure provides evidence for the correlation between precision and accuracy for varying numbers of sensors.}
    \label{fig:entropy-error}
\end{figure}

The empirical-\glspl{pdf} and empirical-\glspl{cdf} are non-parametric tools employed to analyze the distribution of data points in a sample without assuming an inherent distribution.
The empirical-\glspl{pdf} provide a histogram-like representation, highlighting the relative frequencies of various data values, while the empirical-\glspl{cdf} capture the cumulative probability for each value.
% Together, they are pivotal in exploratory data analysis, enhancing the visualization and comprehension of data distribution.
\Cref{fig:result-pdf} depicts the empirical-\gls{pdf}, represented with solid lines, and \gls{cdf}, represented with dashed lines, of the normed localization error derived from location estimates for both occupants' data.
The plots on the left and right represent these curves of the first and second occupants' data, respectively.
The solid blue and brown curves represent the empirical-\gls{pdf} of the proposed and baseline techniques while the dashed curves represent the empirical-\gls{cdf}.
As can be seen in the figures the proposed technique shows relatively higher accumulations in lower regions in the error axis than the baseline suggesting the error characteristics of the proposed technique will more likely fall on the smaller regions than the baseline.
Another way to present this observation is through the empirical-\glspl{cdf}.
For instance, a major takeaway from the empirical-\gls{cdf} curves is that 80\% of the errors of the proposed and baseline techniques are equal or less than \num{2.286} and \num{3.102} meters for both occupants, respectively.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=1]{figs/error-pdf.eps}
    \caption{Empirical-\glspl{pdf} and \glspl{cdf} of normed localization errors derived from location estimates for both occupants. Solid lines represent the empirical-\glspl{pdf}, with blue and brown indicating the proposed and baseline techniques, respectively. Dashed lines depict the empirical-\glspl{cdf}. The plots demonstrate that the proposed technique generally results in lower localization errors compared to the baseline.}
    \label{fig:result-pdf}
\end{figure}

\Cref{tab:results} tabulates the overall landscape of resulting error characteristics of the proposed technique and the baseline by providing a   statistical analysis comparing the performance of two localization methods.
Various descriptive statistical metrics such as mean, standard deviation, median, root mean square (RMS), minimum, and maximum values, all expressed in meters, are presented. The results span varying numbers of sensors, from 2 to 11, and are differentiated for two distinct occupants.

For the first occupant, the proposed method consistently outperforms the baseline across all metrics.
The weighted average mean localization error for the proposed method is \num{1.58} meters, a notable improvement from the baseline's \num{2.3056} meters.
Similarly, for the second occupant, the proposed method achieves a weighted average mean error of \num{1.4843} meters, significantly lower than the baseline's \num{2.2771} meters.
Also, one interesting finding from this result set is that the proposed localization technique can achieve sub-meter localization accuracy and precision when enough sensors are employed (cf. Std. Dev. of Occupant-1 and 2 with 10 and 11 sensors; cf. median localization error of Occupant-2 with 11 sensors). 
This table underlines the enhanced accuracy and precision of the proposed vibro-localization technique over the baseline for various sensor configurations and both occupants.

\hl{In }\Cref{fig:hello-mirshekari-all,fig:hello-mirshekari-bse}\hl{, we analyze the relationship between average sensor distance and localization error for two scenarios: considering all sensors and after }\gls{bse}\hl{ algorithm is applied.
We used regression analysis to understand error behavior, with the slope indicating error increase as occupants move farther from sensors.
The correlation between the localization error and the sensor distance assesses the technique's effectiveness in mitigating systemic errors.
An ideal localization technique should minimize these slopes and correlations.}
\Cref{tab:mirshekari}\hl{ compares our results with}~\cite{mirshekari2018occupant}.

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=1]{figs/avg-distance-error_all.eps}
    \caption{The error characteristics of the proposed method as a function of the average sensor distance when all sensors were considered.}
    \label{fig:hello-mirshekari-all}
\end{figure}

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=1]{figs/avg-distance-error_bse.eps}
    \caption{The error characteristics of the proposed method as a function of the average sensor distance when a subset of the sensors were considered.}
    \label{fig:hello-mirshekari-bse}
\end{figure}

\begin{table}[htbp]
    \setlength{\tabcolsep}{6pt} % Default value: 6pt
    \setlength{\aboverulesep}{0pt}
    \setlength{\belowrulesep}{0pt}
    \renewcommand{\arraystretch}{1} % Default value: 1
    % \begin{tabularx}{\linewidth}{l XX XX}
    \begin{tabularx}{\linewidth}{@{} l >{\columncolor{table_color1}}C >{\columncolor{table_color2}}C >{\columncolor{table_color1}}C >{\columncolor{table_color2}}C}
        \toprule
        & \multicolumn{2}{c}{Work presented in~\cite{mirshekari2018occupant}} & \multicolumn{2}{c}{Proposed} \\
        \midrule
        & Without SE & With SE & Without SE & With SE \\
        Slope & 1.44 & 0.56 & \textbf{0.63} & \textbf{0.14} \\
        Intercept & \textbf{-2.3} & \textbf{-0.56} & 1.75  & 1.12 \\
        Correlation Coefficient & 0.82 & \textbf{0.24} & \textbf{0.37} & \textbf{0.24}\\
        \bottomrule
    \end{tabularx}
    \caption{A systemic comparison between the results of work presented in~\cite{mirshekari2018occupant} and the proposed localization technique. SE stands for Sensor Elimination.}
    \label{tab:mirshekari}
\end{table}

\startlandscape
\begin{table}[htbp]
    \caption{Comparison of baseline and proposed methods for different numbers of sensors and cases for two occupants. The table presents statistical measures such as mean, standard deviation, median, root mean square (RMS), minimum, and maximum values in meters. Baseline algorithm is adapted from \cite{alajlouni2019new}.}
    \label{tab:results}
    \centering
    \setlength{\tabcolsep}{6pt} % Default value: 6pt
    \setlength{\aboverulesep}{0pt}
    \setlength{\belowrulesep}{0pt}
    \renewcommand{\arraystretch}{1.5} % Default value: 1
        \begin{tabularx}{\linewidth}{@{} C C C >{\columncolor{table_color1}}C >{\columncolor{table_color1}}C >{\columncolor{table_color2}}C >{\columncolor{table_color2}}C >{\columncolor{table_color1}}C >{\columncolor{table_color1}}C >{\columncolor{table_color2}}C >{\columncolor{table_color2}}C >{\columncolor{table_color1}}C >{\columncolor{table_color1}}C >{\columncolor{table_color2}}C >{\columncolor{table_color2}}C @{}}

        \toprule
        & & & \multicolumn{2}{c}{\textbf{Mean} [m]} & \multicolumn{2}{c}{\textbf{Std. Dev.} [m]} & \multicolumn{2}{c}{\textbf{Median} [m]} & \multicolumn{2}{c}{\textbf{RMS} [m]} & \multicolumn{2}{c}{\textbf{Min} [m]} & \multicolumn{2}{c}{\textbf{Max} [m]} \\
        \cmidrule(lr){4-5} \cmidrule(lr){6-7} \cmidrule(lr){8-9} \cmidrule(lr){10-11} \cmidrule(lr){12-13} \cmidrule(lr){14-15}
        & \# Sensors & \# Cases & Baseline & Proposed & Baseline & Proposed & Baseline & Proposed & Baseline & Proposed & Baseline & Proposed & Baseline & Proposed \\
        \multirow{10}{*}{\rotatebox[origin=c]{90}{\textbf{Occupant - 1}}} & 2 & 55 & 3.6888 & 2.3173 & 3.3372 & 2.3253 & 2.4481 & 1.7542 & 4.9742 & 3.2827  & 0.0107 & 0.0011 & 16.0242 & 17.5661 \\
        & 3 & 165 & 2.8971 & 1.8467 & 2.6042 & 1.7687 & 1.9609 & 1.5082 & 3.8955 & 2.5570 & 0.0092 & 0.0011 & 16.0090 & 17.5661 \\
        & 4 & 330 & 2.5066 & 1.6502 & 2.1398 & 1.4102 & 1.9153 & 1.4338 & 3.2957 & 2.1707 & 0.0103 & 0.0011 & 14.9141 & 17.4761 \\
        & 5 & 462 & 2.2905 & 1.5606 & 1.8090 & 1.2039 & 1.8043 & 1.4203 & 2.9187 & 1.9710 & 0.0039 & 0.0011 & 14.1410 & 17.4761 \\
        & 6 & 462 & 2.1500 & 1.5195 & 1.5744 & 1.1127 & 1.7895 & 1.4097 & 2.6648 & 1.8833 & 0.0026 & 0.0011 & 13.2300 & 11.8762 \\
        & 7 & 330 & 2.0547 & 1.4868 & 1.4026 & 1.0717 & 1.7807 & 1.4001 & 2.4877 & 1.8328 & 0.0129 & 0.0011 & 12.4402 & 11.8762 \\
        & 8 & 165 & 1.9854 & 1.4516 & 1.2778 & 1.0440 & 1.7613 & 1.3780 & 2.3611 & 1.7880 & 0.0090 & 0.0011 & 11.7862 & 8.3292 \\
        & 9 & 55 & 1.9322 & 1.4110 & 1.1878 & 1.0173 & 1.7596 & 1.3571 & 2.2681 & 1.7395 & 0.0249 & 0.0011 & 10.5963 & 5.0333 \\
        & 10 & 11 & 1.8894 & 1.3714 & 1.1227 & 0.9975 & 1.7415 & 1.3262 & 2.1977 & 1.6956 & 0.0151 & 0.0034 & 9.3303 & 4.8963 \\
        & 11 & 1 & 1.8545 & 1.3066 & 1.0758 & 0.9818 & 1.7931 & 1.2675 & 2.1423 & 1.6320 & 0.1764 & 0.0034 & 7.4640 & 4.6415 \\
        \midrule
        & \multicolumn{2}{c}{\textbf{Weighted Average}} & 2.3056 & \textbf{1.58} & 1.7853 & \textbf{1.2521} & 1.8401 & \textbf{1.4272} & 2.9200 & \textbf{2.0212} & 0.0078 & \textbf{0.0011} & \textbf{13.6703} & 14.1558 \\
        \midrule
        \multirow{10}{*}{\rotatebox[origin=c]{90}{\textbf{Occupant - 2}}} & 2 & 55 & 3.7099 & 2.1653 & 3.3677 & 2.0995 & 2.3852 & 1.6825 & 5.0103 & 3.0159 & 0.0177 & 0.0010 & 16.0251 & 17.6579 \\
        & 3 & 165 & 2.9058 & 1.7971 & 2.6500 & 1.7054 & 1.9605 & 1.4196 & 3.9327 & 2.4775 & 0.0017 & 0.0010 & 16.0101 & 16.9322 \\
        & 4 & 330 & 2.4965 & 1.5820 & 2.1846 & 1.4151 & 1.8451 & 1.3217 & 3.3174 & 2.1225 & 0.0055 & 0.0010 & 14.0716 & 16.7308 \\
        & 5 & 462 & 2.2650 & 1.4633 & 1.8475 & 1.2276 & 1.7665 & 1.2689 & 2.9229 & 1.9101 & 0.0062 & 0.0010 & 13.0168 & 16.7308 \\
        & 6 & 462 & 2.1130 & 1.4073 & 1.6066 & 1.1290 & 1.7101 & 1.2475 & 2.6544 & 1.8041 & 0.0026 & 0.0010 & 12.3208 & 16.7308 \\
        & 7 & 330 & 2.0078 & 1.3721 & 1.4331 & 1.0874 & 1.7021 & 1.2302 & 2.4667 & 1.7507 & 0.0093 & 0.0011 & 11.3596 & 9.0267 \\
        & 8 & 165 & 1.9322 & 1.3314 & 1.3078 & 1.0590 & 1.6537 & 1.1898 & 2.3332 & 1.7012 & 0.0106 & 0.0011 & 10.8745 & 5.8133 \\
        & 9 & 55 & 1.8731 & 1.2880 & 1.2215 & 1.0230 & 1.6466 & 1.1549 & 2.2362 & 1.6448 & 0.0074 & 0.0011 & 8.7195 & 5.0686 \\
        & 10 & 11 & 1.8261 & 1.2356 & 1.1608 & 0.9749 & 1.6713 & 1.0666 & 2.1636 & 1.5736 & 0.0324 & 0.0011 & 6.9466 & 4.9669 \\
        & 11 & 1 & 1.7871 & 1.1716 & 1.1195 & 0.9224 & 1.6147 & 0.9852 & 2.1069 & 1.4884 & 0.1071 & 0.0011 & 5.8988 & 3.8890 \\
        \midrule
        & \multicolumn{2}{c}{\textbf{Weighted Average}} & 2.2771 & \textbf{1.4843} & 1.8217 & \textbf{1.2545} & 1.7755 & \textbf{1.2790} & 2.9194 & \textbf{1.9444} & 0.0063 & \textbf{0.0010} & \textbf{12.7591} & 14.2538 \\
        \bottomrule
	\end{tabularx}
\end{table}
\finishlandscape
