Structural vibration-based occupant localization is a perception methodology where occupants' locations in an indoor environment are determined by analyzing the floor vibrations due to their footfall patterns.
Specifically, these methods employ the measurements of accelerometers that are fixed to the floor.
Henceforth, the terms vibro-localization and vibro-measurements will refer to such localization techniques and the measurements used in these techniques, respectively.
Vibro-localization techniques facilitate a myriad of applications ranging from smart home monitoring and event classification~\cite{Woolard2017, Li2020, Clemente2020, Shi2019, Shi2020} to human gait assessment~\cite{Fagert2017, Kessler2019, Fagert2021, Davis2022, Dong2022, Dong2023, Dong2024} and occupant identification and tracking~\cite{Pan2015, Poston2017, Hu2021, Dong2021, Drira2022,Fagert2022,Dong2022Reidentification,Dong2023Characterizing}.
% Therefore, vibro-localization is a part of a recently established research field \gls{hbi}~\cite{obrien2020, becerik2022}.

Energy-based vibro-localization techniques utilize the energy that is inherent in vibro-measurements as a localization feature since the signal energy serves as a consistent metric for gauging the magnitude of the vibro-measurements~\cite{Pan2014, Valero2021, Alam2021, MejiaCruz2021, davisdissertation}.
Specifically, higher signal amplitudes result in larger energy values registered by the sensors, and vice versa.
By employing this notion, energy-based vibro-localization techniques characterize the relationship between the signal energy and the length of the propagation path.
Therefore, these techniques offer a simplified approach to occupant localization, thereby reducing the need for exhaustive signal analysis.
In this study, the terms signal energy, power, intensity, and strength are used interchangeably despite their nuanced differences.

Dispersion is a natural phenomenon where different frequency components of structural waves propagate at different velocities in the medium, i.e., the floor.
This phenomenon has been seen as one of the major contributors to localization errors; hence, substantial scholarly endeavors have been directed toward examining the floor's dispersive attributes on localization outcomes.
These researchers have mitigated the dispersive effects inherent in the floor by isolating narrow frequency bands.
These bands, derived via Continuous Wavelet Transformation, remain unaffected by dispersion~\cite{Kwon2008, Racic2009, Ciampa2010, Mirshekari2016}.
\citet{Kwon2008}, for instance, presented a successful human activity recognition system utilizing floor vibro-measurements.
Their technique employed a feature extraction step based on wavelet packet decomposition coupled with statistical measures to capture the unique characteristics of different activities.
The empirical findings emphasized the efficacy of the proposed technique in the precise identification of diverse human activities.
%  highlighting its prospective utility in intelligent environments and surveillance infrastructures.
\citet{Racic2009} presented a technique for the detection and classification of human activities via floor vibrations.
They employed an approach involving a combination of wavelet-based feature extraction and a support vector machine classifier to accurately identify different activities, demonstrating the potential of floor vibro-measurements for activity monitoring and recognition in smart environments.
Such narrowband filtering essentially compromises the spatial resolution of the localization technique in hand; this challenge, especially in the context of radio-localization, has been discussed in detail (for instance, \cite{Ghany2020}).

\hl{In their study, }\citet{mirshekari2018occupant}\hl{ employed the wavelet transformation to isolate less dispersion-affected narrowband frequency spectra, crucial for precise }\gls{tdoa} \hl{ estimation.
This innovative multilateration algorithm, unlike conventional methods, does not rely on prior wave velocity knowledge and adapts effectively to different surfaces.
It also integrates a sensor elimination approach to enhance vibro-localization accuracy.
Selecting the nearest sensors minimizes wave propagation through the floor, mitigating signal attenuation.
This improves the }\gls{snr}\hl{, reduces the distortion, and enhances }\gls{tdoa}\hl{ and the location estimations.
Localization with four sensors is favored over more, as additional, distant sensors introduce attenuation and lower accuracy.
Sensor elimination employs TDoA estimations to exclude distant sensors.}

The Warped Frequency Transformation technique has been utilized to discern the dispersion curve and to mitigate perturbations attributed to dispersion~\cite{demarchi2011, woolard2018}.
There exist system-theoretic techniques that characterize the dynamic behavior of the floor via transfer function estimation~\cite{Mohammed2021, Davis2021, MejiaCruz2022}.
Additionally, the Green's function has been employed to account for wave reflections and its dispersion~\cite{Nowakowski2015}.
Despite their precision in empirical analyses, these techniques exhibit limitations in their capacity to generalize the complex material properties and boundary conditions inherent in floors.

On the other hand, \citet{bahroun2014} presented their work formalizing the group velocities, i.e., a major component of signal energy, as a function of propagation path distance.
These promising results paved the way for the model-based techniques which tend to explain the wave phenomena from the data.
\citet{alajlouni2018}, for instance, showed their hypothesis of an energy-decay model (energy logarithmically decays with propagation distance) as a localization model.
In their work, \citet{Pai2019} analyzed whether a relationship exists between the occupant's footfall patterns and the measured signal characteristics in an empirical case study.
In light of their work, the authors assert that there is no evidence of a monotonic relationship between the amplitude or kurtosis of the measured signal and the propagation distance.
Parametric energy-decay models highlight the potential for further improvement~\cite{Alajlouni2022, Wu2023, Tarrio2012}.

Along with parametric decay models, \citet{poston2016}, in an alternative attempt, moved the localization frameworks to a probabilistic framework by modeling the probability of detection and false alarm.
\citet{Alajlouni2022} took a similar approach to localize the occupant by maximizing the sensor likelihood functions given the hypothesis of the sensor's time domain measurements and the energy-decay model proposed in their earlier work.
\citet{Wu2023} propose G-Fall, a device-free fall detection system based on floor vibrations collected by geophone sensors.
Their system utilizes \glspl{hmm} and an energy-based vibro-localization technique, achieving precise and user-independent fall detection with a significant reduction in false alarm rates.

\subsection{Limitations of the Existing Approaches}
% With the advent of methodologies presented earlier, energy-based vibro-localization techniques have proven to be a feasible solution for the indoor occupant localization problem particularly when the privacy of the occupants is at stake.
One of the inherent challenges limiting the ability and success of vibro-localization techniques is that these techniques are single-shot estimators: each heel-strike and its corresponding vibro-measurement vectors are unique; hence, repeated measurements for a single step are not easily attainable.
Therefore, most of the common estimation frameworks cannot be directly employed in such localization systems.
This challenge brings about the following limitations in the landscape of vibro-localization techniques.
%  vector from each sensor deployed within the premises.
% In other words, each sensor is capable of generating a single observation for every distinct step an occupant takes.
% Therefore, multi-sensor perception schemes are especially popular among vibro-localization techniques to increase spatial coverage, probability of detection, and localization accuracy.
% In light of these observations, some limitations in the landscape of vibro-localization techniques are listed below.

\begin{enumerate}
    \item \textbf{Limitations of Ideal Sensor Models}:
    % Because vibro-localization techniques can be seen as single-shot estimators, the traditional averaging-based methods cannot be employed to eliminate the disturbances contained in the vibro-measurement vectors.
    % It is not trivial to characterize the disturbances that each sensor adds to the vibro-measurement vectors.
    Accelerometers are not ideal because they tend to introduce random and systematic errors in the vibro-measurement vector during signal acquisition time.
    %  the the traditional estimation methods, including but not limited to averaging-based methods, cannot be employed in vibro-localization techniques.
    While not trivial, the characterization of errors stemming from signal acquisition was not a central aspect of the existing literature.
    A complete understanding about the localization errors could not be achieved unless such sensor imperfections are categorically identified and incorporated into vibro-localization frameworks.

    \item \textbf{Limitations in Uncertainty Quantification}: The extent to which measurement errors contribute to localization errors is still unknown.
    In other words, the sensing errors in vibro-measurements are yet to be tied to the localization errors.
    It is imperative to account for errors in vibro-measurement vector for a successful localization technique.

    \item \textbf{Limitations in Information Reliability Assessment}: Along with the measurement imperfections, a myriad of uncertainty sources drive the success and failure cases of the energy-based vibro-localization techniques such as reflections, dispersion, etc.
    %  of the other disturbances, for instance, dispersion phenomenon, that can skew some sensors' measurements.
    To remedy the adverse effects of any unreliable sensor information at a given time, a metric needs to be proposed to measure the reliability of the information that each vibro-measurement vector carries.
\end{enumerate}

\subsection{Baseline Study and Overview of the Fundamental Differences}

In this subsection, we present a comprehensive comparison between the established baseline in vibro-localization, as outlined by~\citet{alajlouni2019new}, and the proposed technique.
The fundamental differences between these two methodologies are summarized in~\Cref{tab:comparison_techniques}.
The comparison encompasses the key elements of vibro-localization, including the type of localization features measured, the known parameters assumed in both techniques, the calibrated parameters used during offline processing, and the final output produced during online processing.
This comparative analysis aims to highlight the enhancements and novel contributions of the proposed technique.

\begin{table}[htb]
    \centering
    \begin{tabularx}{\linewidth}{lcc}
        \toprule
        & \textbf{Baseline Technique}~\cite{alajlouni2019new} & \textbf{Proposed Technique} \\
        \midrule
        \shortstack{\textbf{Localization Feature} \\ (Measured)} &
        \shortstack{Energy Measurements \\ \( e_1, \ldots, e_m \)} &
        \shortstack{Energy Measurements \\ \( e_1, \ldots, e_m \)} \\
        \midrule
        \shortstack{\textbf{Known Parameters} \\ (\emph{A priori})} &
        \shortstack{Sensor locations \\ \( \vect{t}_1, \ldots, \vect{t}_m \)} &
        \shortstack{Sensor locations \\ \( \vect{t}_1, \ldots, \vect{t}_m \)} \\
        \midrule
        \shortstack{\textbf{Calibrated Parameters} \\ (Offline Processing)} &
        \shortstack{None \\ \phantom{Asdas}} &
        \shortstack{Sensor noise profile: $\mu_\zeta, \sigma_\zeta$ \\ Calibration vectors $\vect{\upbeta}_1, \ldots, \vect{\upbeta}_m$} \\ % Corrected this line
        \midrule
        \shortstack{\textbf{Output} \\ (Online Processing)} &
        \shortstack{Location estimate: $\hat{\vect{x}}$ \\ \phantom{SDa}} &
        \shortstack{Location estimate: $\vect{x}^*$ \\ Consensus set: $\mathcal{C}$ and its distribution: $f_{\mathcal{C}} \left(\vect{x}\right)$} \\
        \bottomrule
    \end{tabularx}
    \caption{Comparative Overview of Baseline and Proposed Vibro-localization Techniques. This table illustrates the key differences in localization features, known and calibrated parameters, and output between the Baseline Technique as per~\citet{alajlouni2019new} and the proposed technique}
    \label{tab:comparison_techniques}
\end{table}

In contrast, our study builds upon and extends this model by considering non-linear factors that may affect the energy-distance relationship.
These factors could include environmental characteristics and multi-path effects, which cannot be accounted for in a purely linear model.

To emphasize the contributions of this study, it is essential to contrast our approach with the closest related work, as encapsulated in \cite{Alajlouni2022}. While \cite{Alajlouni2022} initiates the exploration of the problem space by deriving \glspl{pdf} of the energy of acquired vibro-measurement vectors, it does so with the simplifying assumption of neglecting the cross-terms as they are zero mean random variables.
Our work diverges fundamentally at this juncture, where we incorporate all terms in our derivation. This inclusion introduces a more comprehensive model that acknowledges the potential influence of cross-terms.

Further diverging from \cite{Alajlouni2022}, we abandon the assumption that energy measurements \( e_i \) are independent and identically distributed.
This work demonstrates, through Proof 1 and Corollary 1, that the \( e_i \)'s are, in fact, sampled from distinct distributions for each sensor.
We substantiate this claim by providing the \glspl{pdf} and the first two statistical moments for these distributions.
This nuanced understanding of the energy measurements’ distribution is pivotal to enhancing the accuracy of vibro-localization techniques, thereby marking a significant stride forward from the state-of-the-art.

\subsection{Summary of the Contributions}
This paper presents an energy-based vibro-localization technique that addresses the sensor imperfections and their effects on the localization results.
The proposed technique employs a family of accelerometers placed on a floor to generate multiple vibro-measurement vectors about the same step.
Furthermore, the proposed technique employs two corrective steps in the localization time: (i) a comprehensive uncertainty quantification to minimize the effect of the internal errors occurring during the signal acquisition time present in the vibro-measurement vectors; and, (ii) an information-theoretic \gls{bse} algorithm to address the external sources of uncertainty such as reflections and dispersion.
The following points summarize the proposed technique's contributions.

\begin{itemize}
    \item \textbf{Vibro-localization Technique with Comprehensive Uncertainty Quantification} (Addressing Limitations 1 and 2): The proposed vibro-localization technique employs an explicit error model for each sensor.
    Therefore, a complete uncertainty quantification on the localization errors due to the measurement errors can be minimized with our technique.
    % In other words, our technique minimizes the disturbances due to measurement errors.

    \item \textbf{Information-Theoretic \gls{bse} Algorithm} (Addressing Limitation 3): The paper introduces a \gls{bse} algorithm.
    The proposed \gls{bse} algorithm divides the sensors into two distinct subsets: the ones that show some consistency amongst them, and the ones which are divergent in nature.
    By leveraging a greedy information-theoretic approach, it decides whether a sensor should be placed in the former set, or vice versa.
    This algorithm guarantees a locally-optimal subset of the sensors in minimizing the localization errors.

    \item \textbf{Empirical Validation} (Addressing Limitations 1--3): Data from a previously conducted controlled experiments were employed to validate and benchmark the proposed technique.
    The results demonstrated significant improvements over the baseline~\cite{alajlouni2019new} approach in terms of both accuracy and precision.

    \item \textbf{Quantification of the Empirical Precision and Accuracy} (Addressing Limitation 3): This paper employs the results of the empirical validation study to quantify an empirical correlation between precision and accuracy achieved with the proposed vibro-localization technique.
    By employing such correlation metrics, we gain better insights about the technique's performance and failures.
\end{itemize}

\subsection{Organization of the Paper}
This paper is structured into six distinct sections to provide a comprehensive overview of the research topic.
\begin{itemize}
    \item The first section, referenced as \Cref{sec:intro}, serves as the introductory portion of the article.
    In this section, readers are introduced to the primary problem that the research addresses.
    Additionally, it offers a review of the existing literature on the subject, ensuring that readers have a foundational understanding of the context and the significance of the problem at hand.

    \item Following the introduction, \Cref{sec:technique} delves into the specifics of the proposed technique.
    This section provides the details of the uncertainty quantification of the localization outcomes due to the errors in the vibro-measurement vectors.
    A probabilistic technique is employed to quantify the localization uncertainties.
    Here, we also provide details of \gls{bse} algorithm used in the elimination of Byzantine sensors.

    \item In the subsequent section, \Cref{sec:experiments}, the focus is on the controlled experiments that were carried out.
    This section provides a detailed account of the experimental setup and procedure, laying the groundwork for the results that follow.

    \item \Cref{sec:results} presents the results obtained from the experiments.
    It offers insights into the technique's performance and reliability, discussing the outcomes in the context of the proposed method's effectiveness.

    \item \Cref{sec:conclusion} encapsulates the primary findings of the research.
    It presents the conclusions drawn from the study and sheds light on potential avenues for future work.
    This section serves to summarize the research's contributions and provide future research directions.
\end{itemize}
