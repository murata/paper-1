%  LaTeX support: latex@mdpi.com
%  For support, please attach all files needed for compiling as well as the log file, and specify your operating system, LaTeX version, and LaTeX editor.

%=================================================================
\documentclass[sensors,article,submit,moreauthors]{Definitions/mdpi}
\usepackage{ambarkutuk-paper}
\renewcommand{\glossarysection}[2][]{}

\input{etc/definitions}
\input{etc/glossaries}
\makeglossaries
%--------------------
% Class Options:
%--------------------
%---------
% article
%---------
% The default type of manuscript is "article", but can be replaced by:
% abstract, addendum, article, book, bookreview, briefreport, casereport, comment, commentary, communication, conferenceproceedings, correction, conferencereport, entry, expressionofconcern, extendedabstract, datadescriptor, editorial, essay, erratum, hypothesis, interestingimage, obituary, opinion, projectreport, reply, retraction, review, perspective, protocol, shortnote, studyprotocol, systematicreview, supfile, technicalnote, viewpoint, guidelines, registeredreport, tutorial
% supfile = supplementary materials

%----------
% submit
%----------
% The class option "submit" will be changed to "accept" by the Editorial Office when the paper is accepted. This will only make changes to the frontpage (e.g., the logo of the journal will get visible), the headings, and the copyright information. Also, line numbering will be removed. Journal info and pagination for accepted papers will also be assigned by the Editorial Office.

%------------------
% moreauthors
%------------------
% If there is only one author the class option oneauthor should be used. Otherwise use the class option moreauthors.

%---------
% pdftex
%---------
% The option pdftex is for use with pdfLaTeX. Remove "pdftex" for (1) compiling with LaTeX & dvi2pdf (if eps figures are used) or for (2) compiling with XeLaTeX.

%=================================================================
% MDPI internal commands - do not modify
\firstpage{1}
\makeatletter
\setcounter{page}{\@firstpage}
\makeatother
\pubvolume{1}
\issuenum{1}
\articlenumber{0}
\pubyear{2023}
\copyrightyear{2023}
%\externaleditor{Academic Editor: Firstname Lastname}
\datereceived{ }
\daterevised{ } % Comment out if no revised date
\dateaccepted{ }
\datepublished{ }
%\datecorrected{} % For corrected papers: "Corrected: XXX" date in the original paper.
%\dateretracted{} % For corrected papers: "Retracted: XXX" date in the original paper.
\hreflink{https://doi.org/} % If needed use \linebreak
%\doinum{}
%\pdfoutput=1 % Uncommented for upload to arXiv.org

%=================================================================
% Add packages and commands here. The following packages are loaded in our class file: fontenc, inputenc, calc, indentfirst, fancyhdr, graphicx, epstopdf, lastpage, ifthen, float, amsmath, amssymb, lineno, setspace, enumitem, mathpazo, booktabs, titlesec, etoolbox, tabto, xcolor, colortbl, soul, multirow, microtype, tikz, totcount, changepage, attrib, upgreek, array, tabularx, pbox, ragged2e, tocloft, marginnote, marginfix, enotez, amsthm, natbib, hyperref, cleveref, scrextend, url, geometry, newfloat, caption, draftwatermark, seqsplit
% cleveref: load \crefname definitions after \begin{document}

%=================================================================
% Please use the following mathematics environments: Theorem, Lemma, Corollary, Proposition, Characterization, Property, Problem, Example, ExamplesandDefinitions, Hypothesis, Remark, Definition, Notation, Assumption
%% For proofs, please use the proof environment (the amsthm package is loaded by the MDPI class).

%=================================================================
% Full title of the paper (Capitalized)
\Title{A Multi-Sensor Stochastic Energy-based Vibro-localization Technique with Byzantine Sensor Elimination}

% MDPI internal command: Title for citation in the left column
\TitleCitation{A Multi-Sensor Stochastic Energy-based Vibro-localization Technique}

% Author Orchid ID: enter ID or remove command
\newcommand{\orcidauthorA}{0000-0001-8287-1069} % Add \orcidA{} behind the author's name
\newcommand{\orcidauthorB}{0000-0001-7864-551X} % Add \orcidB{} behind the author's name
\newcommand{\orcidauthorC}{0000-0002-5443-8484} % Add \orcidC{} behind the author's name
\newcommand{\orcidauthorD}{0000-0001-5413-474X} % Add \orcidD{} behind the author's name

% Authors, for the paper (add full first names)
\Author{Murat Ambarkutuk $^{1}$\orcidA{}, Sa'ed Alajlouni $^{2}\orcidB{}$, Pablo Tarazaga $^{2}$\orcidC{}, and Paul Plassmann $^{1, *}$\orcidD{}}

% \longauthorlist{yes}

% MDPI internal command: Authors, for metadata in PDF
\AuthorNames{Murat Ambarkutuk, Sa'ed Alajlouni, Pablo Tarazaga, and Paul Plassmann}

% MDPI internal command: Authors, for citation in the left column
\AuthorCitation{Ambarkutuk, M.; Alajlouni, S.; Tarazaga, P.; Plassmann P.}
% If this is a Chicago style journal: Lastname, Firstname, Firstname Lastname, and Firstname Lastname.

% Affiliations / Addresses (Add [1] after \address if there is only one affiliation.)
\address{%
	$^{1}$ \quad The Bradley Department of Electrical and Computer Engineering, Virginia Tech; murata@vt.edu (M.A.); plassmann@vt.edu (P.P.)\\
	$^{2}$ \quad J. Mike Walker \textquotesingle66 Department of Mechanical Engineering, Texas A\&M University; saed@tamu.edu (S.A.); ptarazaga@tamu.edu (P.T.)
}

% Contact information of the corresponding author
\corres{Correspondence: plassmann@vt.edu}
\abstract{
	This paper presents an occupant localization technique that determines the location of individuals in indoor environments by analyzing the structural vibrations of the floor caused by their footsteps.
	Structural vibration waves are difficult to measure as they are influenced by various factors, including the complex nature of wave propagation in heterogeneous and dispersive media (such as the floor) as well as the inherent noise characteristics of sensors observing the vibration wavefronts.
	The proposed vibration-based occupant localization technique minimizes the errors that occur during the signal acquisition time.
	In this process, the likelihood function of each sensor---representing where the occupant likely resides in the environment---is fused to obtain a consensual localization result in a collective manner.
	In this work, it becomes evident that the above sources of uncertainties can render certain sensors as deceptive, commonly referred to as ``Byzantines.''
	Because the ratio of Byzantines among the set sensors defines the success of the collectivistic localization results, this paper introduces a \gls{bse} algorithm to prevent unreliable information of Byzantine sensors from affecting the location estimations.
	This algorithm identifies and eliminates sensors that generate erroneous estimates, preventing the influence of these sensors on the overall consensus.
	% Furtermore, this paper establishes a metric that quantifies the correlation between precision and accuracy in the proposed vibro-localization technique.
	To validate and benchmark the proposed technique, a set of previously-conducted controlled experiments was employed.
	The empirical results demonstrates the proposed technique's significant improvement (\texttildelow 30\%) over the baseline approach in terms of both accuracy and precision.
	% The results indicate that, even in the presence of Byzantine sensors and measurement uncertainty, the localization outcomes improve.
	% The findings also highlight the potential of the technique for effectively detecting occupants in indoor environments through the analysis of structural vibrations, paving the way for enhanced localization capabilities in various applications.
}

% Keywords
\keyword{occupant localization; structural vibrations; sensor fusion; byzantine sensor elimination}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Only for the journal Data
%\dataset{DOI number or link to the deposited data set if the data set is published separately. If the data set shall be published as a supplement to this paper, this field will be filled by the journal editors. In this case, please submit the data set as a supplement.}
%\datasetlicense{License under which the data set is made available (CC0, CC-BY, CC-BY-SA, CC-BY-NC, etc.)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\glsresetall

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction\label{sec:intro}}
\input{sections/intro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Method\label{sec:technique}}
\input{sections/technique}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experiments \label{sec:experiments}}
\input{sections/experiments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results \label{sec:results}}
\input{sections/results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions \& Future Work\label{sec:conclusion}}
\input{sections/conclusion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{6pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \authorcontributions{For research articles with several authors, a short paragraph specifying their individual contributions must be provided. The following statements should be used ``Conceptualization, X.X. and Y.Y.; methodology, X.X.; software, X.X.; validation, X.X., Y.Y. and Z.Z.; formal analysis, X.X.; investigation, X.X.; resources, X.X.; data curation, X.X.; writing---original draft preparation, X.X.; writing---review and editing, X.X.; visualization, X.X.; supervision, X.X.; project administration, X.X.; funding acquisition, Y.Y. All authors have read and agreed to the published version of the manuscript.'', please turn to the  \href{http://img.mdpi.org/data/contributor-role-instruction.pdf}{CRediT taxonomy} for the term explanation. Authorship must be limited to those who have contributed substantially to the work~reported.}
\authorcontributions{
	Conceptualization, M.A. and S.A.;
	data curation, S.A. and P.T.;
	formal analysis, M.A. and P.P.;
	methodology, M.A. and P.P.;
	software, M.A.;
	validation, S.A. and P.P.;
	investigation, M.A. and S.A.;
	resources, P.T.;
	writing---original draft preparation, M.A.;
	writing---review and editing, S.A. and P.P.;
	visualization, M.A.;
	supervision, P.P. and P. T.
	% project administration, X.X.;
	% funding acquisition, Y.Y.
	All authors have read and agreed to the published version of the manuscript.
}

\funding{Not applicable.}
\institutionalreview{Not applicable.}
\informedconsent{Not applicable.}
\dataavailability{Not applicable.}
\acknowledgments{The authors wish to acknowledge the support as well as the collaborative efforts provided by our sponsors, VTI Instruments, PCB Piezotronics, Inc.; Dytran Instruments, Inc.; and Oregano Systems. The authors are particularly appreciative for the support provided by the College of Engineering at Virginia Tech through Dean Richard Benson and Associate Dean Ed Nelson as well as VT Capital Project Manager, Todd Shelton, and VT University Building Official, William Hinson. The authors would also like to acknowledge Gilbane, Inc. and in particular,
David Childress and Eric Hotek. M. Ambarkutuk would like to acknowledge the insights and expertise of Professor Creed F. Jones; the author is truly grateful for his guidance. The authors also thank the editor and anonymous reviewers for providing helpful suggestions for improving the quality of this manuscript.}

\conflictsofinterest{The authors declare no conflict of interest.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\abbreviations{Abbreviations}{
	The following abbreviations are used in this manuscript:\\
	\noindent
	\printglossary[type=\acronymtype]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Optional
\appendixtitles{no} % Leave argument "no" if all appendix headings stay EMPTY (then no dot is printed after "Appendix A"). If the appendix sections contain a heading then change the argument to "yes".
\appendixstart
\appendix
\input{sections/appendix}
% The appendix is an optional section that can contain details and data supplemental to the main text---for example, explanations of experimental details that would disrupt the flow of the main text but nonetheless remain crucial to understanding and reproducing the research shown; figures of replicates for experiments of which representative data are shown in the main text can be added here if brief, or as Supplementary Data. Mathematical proofs of results not central to the paper can be added as an appendix.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{adjustwidth}{-\extralength}{0cm}
%\printendnotes[custom] % Un-comment to print a list of endnotes

\reftitle{References}

% Please provide either the correct journal abbreviation (e.g. according to the “List of Title Word Abbreviations” http://www.issn.org/services/online-services/access-to-the-ltwa/) or the full name of the journal.
% Citations and References in Supplementary files are permitted provided that they also appear in the reference list here.

%=====================================
% References, variant A: external bibliography
%=====================================
%\bibliography{your_external_BibTeX_file}
\bibliography{etc/paper-1,etc/added}
% If authors have biography, please use the format below
%\section*{Short Biography of Authors}
%\bio
%{\raisebox{-0.35cm}{\includegraphics[width=3.5cm,height=5.3cm,clip,keepaspectratio]{Definitions/author1.pdf}}}
%{\textbf{Firstname Lastname} Biography of first author}
%
%\bio
%{\raisebox{-0.35cm}{\includegraphics[width=3.5cm,height=5.3cm,clip,keepaspectratio]{Definitions/author2.jpg}}}
%{\textbf{Firstname Lastname} Biography of second author}

% For the MDPI journals use author-date citation, please follow the formatting guidelines on http://www.mdpi.com/authors/references
% To cite two works by the same author: \citeauthor{ref-journal-1a} (\citeyear{ref-journal-1a}, \citeyear{ref-journal-1b}). This produces: Whittaker (1967, 1975)
% To cite two works by the same author with specific pages: \citeauthor{ref-journal-3a} (\citeyear{ref-journal-3a}, p. 328; \citeyear{ref-journal-3b}, p.475). This produces: Wong (1999, p. 328; 2000, p. 475)

\PublishersNote{}
\end{adjustwidth}
\end{document}
